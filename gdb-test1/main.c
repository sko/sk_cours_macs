#include<stdlib.h>
#include<stdio.h>

#include "myHeader.h"

int CountLine(FILE* fd);

int main(void) {
    char fileName[MAXLEN] = "data/input.dat";
    FILE *fd = NULL;
    int nbLines;
    int i = 0;
    char* lines[MAXLINE];

    fd = fopen(fileName, "r");
    nbLines = CountLine(fd);
    printf("%d lines\n", nbLines);
    fclose(fd);

    for (i = 0; i < nbLines; i++) {
        lines[i] = malloc(sizeof (char) * MAXLEN);
    }

    fd = fopen(fileName, "r");
    for (i = 1; i <= nbLines; i++) {
        char buf[MAXLEN] = "";
        if (fgets(lines[i], MAXLEN - 1, fd) != lines[i]) {
            fprintf(stderr, "error: exit.\n");
            exit(EXIT_FAILURE);
        }
    }

    fclose(fd);


    return EXIT_SUCCESS;
}
