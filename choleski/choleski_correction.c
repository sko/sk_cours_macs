#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define nbLi 3

void ComputeCholeski (double A[nbLi][nbLi], double B[nbLi][nbLi]);

void ComputeCholeski (double A[nbLi][nbLi], double B[nbLi][nbLi]){
int j = 0;
int i = 0;
int k = 0;
double tmpSum1 = 0.0;
double tmpSum2 = 0.0;
    for(j = 0; j< nbLi; j++){
        tmpSum1 = 0.0;
        for(k=0;k<j; k++){
            tmpSum1 += B[j][k]*B[j][k];
        }

        B[j][j] = sqrt(A[j][j] - tmpSum1);

        for(i=0; i< j; i++ ){
            B[i][j] = 0 ;
        }

        for(i=j+1; i< nbLi; i++ ){
            tmpSum2 = 0.0;
            for(k=0;k<j;k++){
                tmpSum2 += B[i][k]*B[j][k];
            }
            B[i][j] = (A[i][j] - tmpSum2) / B[j][j];
        }

    }
}

void printMat(double M[nbLi][nbLi]){
    int i = 0;
    int j = 0;
    for(i=0;i<nbLi; i++){
        for(j=0;j<nbLi; j++){
        printf("[%d][%d] = %lf\n",i,j,M[i][j]);
        }
    }
}


int main(void){
    double A[nbLi][nbLi] = {{1,1,1}, {1,2,2},{1,2,3}};
    double B[nbLi][nbLi] = {{1,1,1}, {1,2,2},{1,2,3}};

    ComputeCholeski (A, B);
    printMat(B);

    return EXIT_SUCCESS;
}
