// Options:   --nomain --quiet --no-checksum --no-bitfields --no-comma-operators --concise --no-jumps -o fileB.c
#include "csmith.h"

volatile uint64_t csmith_sink_ = 0;

static long __undefined;


struct S0 {
   uint32_t  f0;
   volatile uint32_t  f1;
   int16_t  f2;
};

union U1 {
   int32_t  f0;
};


static int32_t g_4 = 0x90A624CDL;
static int32_t * volatile g_3[3][8] = {{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4},{&g_4,&g_4,&g_4,(void*)0,&g_4,&g_4,&g_4,&g_4}};
static int32_t * volatile g_5 = &g_4;
static const int32_t g_21 = 8L;
static const int32_t *g_22[8][2] = {{&g_21,(void*)0},{(void*)0,&g_21},{(void*)0,(void*)0},{&g_21,(void*)0},{(void*)0,&g_21},{(void*)0,(void*)0},{&g_21,(void*)0},{(void*)0,&g_21}};
static union U1 g_44 = {-1L};
static uint32_t g_49 = 0UL;
static uint32_t g_70[5][3][3] = {{{0x47C1471CL,0x47C1471CL,4294967295UL},{4294967295UL,4294967295UL,1UL},{4294967295UL,4294967295UL,0x47C1471CL}},{{0x237FF9BDL,0x237FF9BDL,4294967295UL},{4294967295UL,4294967295UL,0x47C1471CL},{0x237FF9BDL,0x237FF9BDL,4294967295UL}},{{4294967295UL,4294967295UL,0x47C1471CL},{0x237FF9BDL,0x237FF9BDL,4294967295UL},{4294967295UL,4294967295UL,0x47C1471CL}},{{0x237FF9BDL,0x237FF9BDL,4294967295UL},{4294967295UL,4294967295UL,0x47C1471CL},{0x237FF9BDL,0x237FF9BDL,4294967295UL}},{{4294967295UL,4294967295UL,0x47C1471CL},{0x237FF9BDL,0x237FF9BDL,4294967295UL},{4294967295UL,4294967295UL,0x47C1471CL}}};
static int8_t g_74 = 0x37L;
static uint32_t g_100 = 4294967291UL;
static const int32_t *g_104[6] = {&g_4,&g_4,&g_4,&g_4,&g_4,&g_4};
static const int32_t ** volatile g_103[1][1] = {{&g_104[4]}};
static const int32_t ** volatile g_105 = &g_104[4];
static int8_t g_126 = (-1L);
static uint16_t g_140 = 0x1F22L;
static const uint16_t g_150 = 0x4BD6L;
static uint16_t g_153 = 8UL;
static uint16_t *g_152[6] = {&g_153,&g_153,&g_153,&g_153,&g_153,&g_153};
static uint64_t g_154 = 18446744073709551609UL;
static volatile struct S0 g_158 = {0x5199DCEDL,1UL,-2L};
static volatile struct S0 * volatile g_159[7] = {&g_158,&g_158,&g_158,&g_158,&g_158,&g_158,&g_158};
static volatile struct S0 g_160 = {0UL,18446744073709551615UL,0xD3EBL};
static int32_t *g_169 = &g_4;
static int32_t **g_168 = &g_169;
static union U1 *g_180[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static int16_t g_217 = (-5L);
static int16_t *g_218[10] = {&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217};
static uint8_t g_225 = 0xC9L;
static struct S0 g_258 = {0x5B112714L,0UL,0L};
static uint8_t *g_297 = &g_225;
static const int32_t ** volatile g_315 = &g_104[4];
static struct S0 g_387 = {1UL,18446744073709551609UL,0x1237L};
static struct S0 * volatile g_388 = &g_258;
static uint64_t *g_428 = (void*)0;
static uint64_t **g_427 = &g_428;
static uint64_t *** volatile g_426[10][7] = {{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{(void*)0,&g_427,&g_427,(void*)0,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,(void*)0,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,(void*)0,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,(void*)0,&g_427},{(void*)0,&g_427,&g_427,(void*)0,&g_427,&g_427,&g_427},{&g_427,&g_427,(void*)0,(void*)0,&g_427,&g_427,&g_427}};
static uint64_t *** volatile g_429 = &g_427;
static uint8_t g_441 = 1UL;
static volatile struct S0 g_462 = {7UL,0x8DA63879L,0xC594L};
static volatile struct S0 g_464[2][8][5] = {{{{0xE044D6DAL,0x84B80001L,0x53E0L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0xDA1600E4L,0UL,-5L},{0x1C0306E1L,0x25E86082L,-6L},{0x0F0DB80EL,0x361B9269L,0xCA42L}},{{0x0ECFB9FDL,18446744073709551611UL,0xCB88L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x4201F775L,0xE872F021L,-1L},{0UL,0UL,0x054EL},{0xDA1600E4L,0UL,-5L}},{{5UL,0xA30C04CFL,0x2CA4L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x0F0DB80EL,0x361B9269L,0xCA42L},{6UL,1UL,2L},{1UL,0UL,0L}},{{1UL,18446744073709551611UL,1L},{0xE044D6DAL,0x84B80001L,0x53E0L},{1UL,0UL,0L},{6UL,18446744073709551607UL,0xC85AL},{0x4201F775L,0xE872F021L,-1L}},{{1UL,0xC98856A3L,8L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x53EB6BF7L,0x9E39E357L,0x298CL},{0xABB5D249L,1UL,-6L},{0x53EB6BF7L,0x9E39E357L,0x298CL}},{{0xE044D6DAL,0x84B80001L,0x53E0L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0xDA1600E4L,0UL,-5L},{0x1C0306E1L,0x25E86082L,-6L},{0x0F0DB80EL,0x361B9269L,0xCA42L}},{{0x0ECFB9FDL,18446744073709551611UL,0xCB88L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x4201F775L,0xE872F021L,-1L},{0UL,0UL,0x054EL},{0xDA1600E4L,0UL,-5L}},{{5UL,0xA30C04CFL,0x2CA4L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x0F0DB80EL,0x361B9269L,0xCA42L},{6UL,1UL,2L},{1UL,0UL,0L}}},{{{1UL,18446744073709551611UL,1L},{0xE044D6DAL,0x84B80001L,0x53E0L},{1UL,0UL,0L},{6UL,18446744073709551607UL,0xC85AL},{0x4201F775L,0xE872F021L,-1L}},{{1UL,0xC98856A3L,8L},{0xE044D6DAL,0x84B80001L,0x53E0L},{0x53EB6BF7L,0x9E39E357L,0x298CL},{0xF23521C5L,0xCD7DFA84L,0x016EL},{0xAFB6D8F9L,0xD7691855L,0x61EBL}},{{0xDA1600E4L,0UL,-5L},{0xDA1600E4L,0UL,-5L},{0x47F7494FL,0x84B74016L,-3L},{0x22541475L,4UL,0x7216L},{0x9841DA2FL,0x9FA05B87L,0xE9BDL}},{{0x53EB6BF7L,0x9E39E357L,0x298CL},{0xDA1600E4L,0UL,-5L},{0x923634ABL,6UL,0L},{0x9D772FBAL,18446744073709551614UL,-1L},{0x47F7494FL,0x84B74016L,-3L}},{{1UL,0UL,0L},{0xDA1600E4L,0UL,-5L},{0x9841DA2FL,0x9FA05B87L,0xE9BDL},{0x089C80D7L,0UL,0x488FL},{0x56B59CE9L,0x69BF6F83L,0L}},{{0x0F0DB80EL,0x361B9269L,0xCA42L},{0xDA1600E4L,0UL,-5L},{0x56B59CE9L,0x69BF6F83L,0L},{6UL,0x6C1D64AAL,0xF1E0L},{0x923634ABL,6UL,0L}},{{0x4201F775L,0xE872F021L,-1L},{0xDA1600E4L,0UL,-5L},{0xAFB6D8F9L,0xD7691855L,0x61EBL},{0xF23521C5L,0xCD7DFA84L,0x016EL},{0xAFB6D8F9L,0xD7691855L,0x61EBL}},{{0xDA1600E4L,0UL,-5L},{0xDA1600E4L,0UL,-5L},{0x47F7494FL,0x84B74016L,-3L},{0x22541475L,4UL,0x7216L},{0x9841DA2FL,0x9FA05B87L,0xE9BDL}}}};
static const uint64_t g_484 = 0x37954E6C6213D6B7LL;
static struct S0 g_504 = {0x486B0507L,0x098387DDL,3L};
static const struct S0 g_506 = {4294967295UL,0xE6B12779L,0x9194L};
static volatile struct S0 g_526 = {0xCB0138F2L,0xE5FB28A6L,-2L};
static volatile struct S0 *g_525 = &g_526;
static int8_t g_551 = 0x38L;
static int16_t g_600 = (-1L);
static volatile int8_t g_622 = 0x6FL;
static uint32_t **g_640 = (void*)0;
static volatile uint16_t *g_647 = (void*)0;
static volatile uint16_t **g_646 = &g_647;
static volatile uint16_t ** const  volatile *g_645 = &g_646;
static uint16_t g_669 = 65526UL;
static struct S0 g_795[6] = {{0x74040EE1L,1UL,-9L},{4294967295UL,0xCED7B2CEL,0xD1E7L},{4294967295UL,0xCED7B2CEL,0xD1E7L},{0x74040EE1L,1UL,-9L},{4294967295UL,0xCED7B2CEL,0xD1E7L},{4294967295UL,0xCED7B2CEL,0xD1E7L}};
static volatile struct S0 g_796 = {0xCAEAAD01L,0xE90E6CFBL,0xBBB1L};
static union U1 ** volatile * volatile g_813 = (void*)0;
static int16_t **g_897 = &g_218[7];
static int16_t *** volatile g_896 = &g_897;
static const int32_t ** volatile * volatile g_942 = &g_315;
static const int32_t ** volatile * volatile * volatile g_941 = &g_942;
static volatile uint64_t g_984 = 0xCE24A907B4BCBE04LL;
static uint64_t g_1046 = 0x3BD91C522CAD129FLL;
static const int32_t ** volatile g_1074 = (void*)0;
static volatile struct S0 g_1090 = {0x9B8E5278L,0x5F5132E5L,0xB19BL};
static struct S0 g_1217 = {0x61B5D02BL,0xAD159CA2L,-4L};
static const int32_t ** volatile g_1266 = (void*)0;
static uint32_t *g_1365 = &g_258.f0;
static uint32_t ** const g_1364 = &g_1365;
static uint32_t ** const *g_1363 = &g_1364;
static int16_t g_1375 = 2L;
static int8_t g_1382 = 0x2DL;
static struct S0 *g_1438 = &g_258;
static struct S0 ** const  volatile g_1437 = &g_1438;
static int32_t ** volatile g_1440 = (void*)0;
static int32_t ** volatile g_1442 = &g_169;
static int64_t * volatile g_1453 = (void*)0;
static int64_t * volatile * const  volatile g_1452[3][3][5] = {{{(void*)0,&g_1453,&g_1453,&g_1453,&g_1453},{&g_1453,&g_1453,&g_1453,&g_1453,&g_1453},{(void*)0,&g_1453,&g_1453,&g_1453,&g_1453}},{{&g_1453,&g_1453,&g_1453,&g_1453,&g_1453},{(void*)0,&g_1453,&g_1453,&g_1453,&g_1453},{&g_1453,&g_1453,&g_1453,&g_1453,&g_1453}},{{(void*)0,&g_1453,&g_1453,&g_1453,&g_1453},{&g_1453,&g_1453,&g_1453,&g_1453,&g_1453},{(void*)0,&g_1453,&g_1453,&g_1453,&g_1453}}};
static int64_t * volatile * const  volatile *g_1451 = &g_1452[2][0][4];
static struct S0 ** volatile g_1461 = &g_1438;
static struct S0 g_1462 = {0x6F121A9DL,0x6D42934DL,-5L};
static uint32_t g_1519 = 8UL;
static const int64_t * const ** volatile g_1559 = (void*)0;
static const int64_t * const ** volatile g_1560 = (void*)0;
static volatile uint16_t * volatile g_1618 = (void*)0;
static int16_t **g_1632 = &g_218[7];
static uint32_t g_1642 = 18446744073709551614UL;
static union U1 ** volatile g_1647 = &g_180[0];
static int32_t * volatile g_1649[1] = {&g_44.f0};
static int64_t g_1667 = 0x800A963ED1B91415LL;
static volatile uint8_t g_1670 = 0x99L;
static const int64_t g_1688 = 0x6653F5BFDBB653F3LL;
static int32_t * volatile g_1714 = &g_44.f0;
static uint16_t **g_1769 = (void*)0;
static uint16_t ***g_1768 = &g_1769;
static volatile uint64_t g_1835 = 1UL;
static uint64_t g_1864 = 0x8ACF46AD09628219LL;
static int32_t ** volatile g_1890 = &g_169;
static volatile uint8_t *** const  volatile g_1962 = (void*)0;
static uint8_t * volatile * volatile *g_1966 = (void*)0;
static const uint64_t g_1979 = 0xA8F5F95749A217C8LL;
static volatile struct S0 g_1993[6][8] = {{{0UL,0x35EB196AL,8L},{0xE05090C0L,18446744073709551615UL,0x1F9DL},{0x11A8D790L,2UL,1L},{0xE05090C0L,18446744073709551615UL,0x1F9DL},{0UL,0x35EB196AL,8L},{0x729CE6DEL,7UL,-1L},{0x45BAEBBBL,5UL,7L},{0x3B1D3031L,0x6BB43815L,0xAB1DL}},{{0xE05090C0L,18446744073709551615UL,0x1F9DL},{0x81621125L,0x10CA9066L,0x7471L},{4294967290UL,0UL,1L},{0x11A8D790L,2UL,1L},{0x11A8D790L,2UL,1L},{4294967290UL,0UL,1L},{0x81621125L,0x10CA9066L,0x7471L},{0xE05090C0L,18446744073709551615UL,0x1F9DL}},{{0x7F3C681BL,18446744073709551615UL,0x497EL},{0x729CE6DEL,7UL,-1L},{4294967290UL,0UL,1L},{1UL,0xF36AC67FL,1L},{0x45BAEBBBL,5UL,7L},{0UL,0x35EB196AL,8L},{0x45BAEBBBL,5UL,7L},{1UL,0xF36AC67FL,1L}},{{0x11A8D790L,2UL,1L},{0x29F32B50L,0xA283B707L,-1L},{0x11A8D790L,2UL,1L},{0x3B1D3031L,0x6BB43815L,0xAB1DL},{1UL,0xF36AC67FL,1L},{0UL,0x35EB196AL,8L},{4294967290UL,0UL,1L},{4294967290UL,0UL,1L}},{{4294967290UL,0UL,1L},{0x729CE6DEL,7UL,-1L},{0x7F3C681BL,18446744073709551615UL,0x497EL},{0x7F3C681BL,18446744073709551615UL,0x497EL},{0x729CE6DEL,7UL,-1L},{4294967290UL,0UL,1L},{1UL,0xF36AC67FL,1L},{0x45BAEBBBL,5UL,7L}},{{4294967290UL,0UL,1L},{0x81621125L,0x10CA9066L,0x7471L},{0xE05090C0L,18446744073709551615UL,0x1F9DL},{0x729CE6DEL,7UL,-1L},{1UL,0xF36AC67FL,1L},{0x729CE6DEL,7UL,-1L},{0xE05090C0L,18446744073709551615UL,0x1F9DL},{0x81621125L,0x10CA9066L,0x7471L}}};
static volatile struct S0 g_1994 = {1UL,0xD167C617L,0L};
static struct S0 ** volatile g_2083[7] = {&g_1438,(void*)0,&g_1438,&g_1438,(void*)0,&g_1438,&g_1438};
static struct S0 ** const  volatile g_2084 = &g_1438;
static int16_t ***g_2115 = (void*)0;
static int16_t ****g_2114 = &g_2115;
static volatile uint16_t ** volatile * volatile g_2123[6] = {&g_646,&g_646,&g_646,&g_646,&g_646,&g_646};
static volatile uint16_t ** volatile * volatile g_2124 = (void*)0;
static int64_t *g_2153 = &g_1667;
static int64_t **g_2152 = &g_2153;
static int64_t ***g_2151 = &g_2152;
static int32_t g_2159[2][2][4] = {{{0xA0DE902BL,0xA0DE902BL,(-1L),0xA0DE902BL},{0xA0DE902BL,(-9L),(-9L),0xA0DE902BL}},{{(-9L),0xA0DE902BL,(-9L),(-9L)},{0xA0DE902BL,0xA0DE902BL,(-1L),0xA0DE902BL}}};
static volatile int16_t g_2177[7] = {0x449FL,(-1L),(-1L),0x449FL,(-1L),(-1L),0x449FL};



static uint16_t  func_1(void);
static int32_t  func_6(const int8_t  p_7, union U1  p_8, uint32_t  p_9);
static uint32_t  func_12(int64_t  p_13, int32_t * p_14, int32_t * p_15);
static uint32_t * func_29(int32_t * p_30, uint64_t  p_31);
static int32_t * func_32(uint8_t  p_33, uint8_t  p_34, union U1  p_35, int8_t  p_36);
static int32_t  func_40(union U1 * p_41, const uint16_t  p_42);
static int16_t  func_58(union U1 * p_59, uint32_t  p_60, int32_t ** p_61, uint32_t * p_62);
static int32_t ** func_64(int64_t  p_65, union U1  p_66, uint32_t  p_67);
static uint8_t  func_75(uint32_t * p_76, uint32_t * p_77, uint32_t  p_78);
static uint32_t * func_79(int64_t  p_80, int32_t ** p_81, union U1 * p_82, uint64_t  p_83);




static uint16_t  func_1(void)
{ 
    uint32_t l_2 = 0x47DC4B4EL;
    const int32_t **l_18 = (void*)0;
    const int32_t *l_20[10] = {&g_21,(void*)0,&g_21,&g_21,(void*)0,&g_21,&g_21,(void*)0,&g_21,&g_21};
    const int32_t **l_19[2][7] = {{&l_20[5],&l_20[5],&l_20[9],&l_20[4],(void*)0,&l_20[4],&l_20[9]},{&l_20[5],&l_20[5],&l_20[9],&l_20[4],(void*)0,&l_20[4],&l_20[5]}};
    uint16_t l_23 = 0x0412L;
    uint32_t *l_24 = &l_2;
    union U1 l_37 = {0xBDB62B2FL};
    union U1 *l_38 = &l_37;
    int32_t l_1809 = 0xC5B21DE3L;
    union U1 l_1842 = {0xCF04FB04L};
    int32_t *l_2158[3];
    int32_t l_2160 = 0xA66B3E62L;
    uint64_t *l_2163 = &g_154;
    const uint8_t l_2166 = 0x27L;
    uint32_t l_2178 = 0x53C27CC0L;
    uint64_t l_2179 = 18446744073709551615UL;
    int i, j;
    for (i = 0; i < 3; i++)
        l_2158[i] = &g_2159[0][0][3];
    (*g_5) |= l_2;
    l_2160 ^= func_6(((safe_div_func_uint32_t_u_u(func_12(((safe_add_func_uint64_t_u_u(((g_22[1][0] = &g_4) == &g_21), (((*l_24) = (l_23 && 0x4FF5452FL)) > (safe_lshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s(g_21, (-1L))), ((((**g_1363) = func_29(func_32(g_4, g_21, ((*l_38) = l_37), g_4), g_153)) != (void*)0) > 0xA3F3B6AE2F4F86C5LL)))))) >= l_1809), &l_1809, &l_1809), g_1217.f2)) != 0x782D1367L), l_1842, g_1642);
    g_2159[1][1][0] ^= (safe_div_func_int64_t_s_s(((***g_2151) = ((void*)0 != l_2163)), (safe_mul_func_uint16_t_u_u(l_2166, (safe_mul_func_uint16_t_u_u(((safe_mod_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((safe_div_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(0x70C7L, 0x72BAL)), 4UL)), g_44.f0)) > g_2177[5]), g_153)) ^ 0xF3F00C75B897E7B3LL), l_2178))))));
    return l_2179;
}



static int32_t  func_6(const int8_t  p_7, union U1  p_8, uint32_t  p_9)
{ 
    int32_t *l_1852[3][3][3];
    uint8_t l_1855 = 1UL;
    int64_t l_1870[10][5][3] = {{{0L,0L,0xDFF74AB362191487LL},{0xB58B636080EF93C9LL,0xC387C2990DDC4514LL,0xC387C2990DDC4514LL},{0xDFF74AB362191487LL,0x1F0EDB99D884E9E3LL,1L},{0xB58B636080EF93C9LL,1L,0xB58B636080EF93C9LL},{0L,0xDFF74AB362191487LL,1L}},{{0x9CA836C71256D33BLL,0x9CA836C71256D33BLL,0xC387C2990DDC4514LL},{0x45DEA4B9512FCCE5LL,0xDFF74AB362191487LL,0xDFF74AB362191487LL},{0xC387C2990DDC4514LL,1L,1L},{0x45DEA4B9512FCCE5LL,0x1F0EDB99D884E9E3LL,0x45DEA4B9512FCCE5LL},{0x9CA836C71256D33BLL,0xC387C2990DDC4514LL,1L}},{{0L,0L,0xDFF74AB362191487LL},{0xB58B636080EF93C9LL,0xC387C2990DDC4514LL,0xC387C2990DDC4514LL},{0xDFF74AB362191487LL,0x1F0EDB99D884E9E3LL,1L},{0xB58B636080EF93C9LL,1L,0xB58B636080EF93C9LL},{0L,0xDFF74AB362191487LL,1L}},{{0x9CA836C71256D33BLL,0x9CA836C71256D33BLL,0xC387C2990DDC4514LL},{0x45DEA4B9512FCCE5LL,0xDFF74AB362191487LL,0xDFF74AB362191487LL},{0xC387C2990DDC4514LL,1L,1L},{0x45DEA4B9512FCCE5LL,0x1F0EDB99D884E9E3LL,0x45DEA4B9512FCCE5LL},{0x9CA836C71256D33BLL,0xC387C2990DDC4514LL,1L}},{{0L,0L,0xDFF74AB362191487LL},{0xB58B636080EF93C9LL,0xC387C2990DDC4514LL,0xC387C2990DDC4514LL},{0xDFF74AB362191487LL,0x1F0EDB99D884E9E3LL,1L},{0xB58B636080EF93C9LL,1L,0xB58B636080EF93C9LL},{0L,0xDFF74AB362191487LL,1L}},{{0x9CA836C71256D33BLL,0x9CA836C71256D33BLL,0xC387C2990DDC4514LL},{0x45DEA4B9512FCCE5LL,0xDFF74AB362191487LL,0xDFF74AB362191487LL},{0xC387C2990DDC4514LL,1L,1L},{0x45DEA4B9512FCCE5LL,0x1F0EDB99D884E9E3LL,0x45DEA4B9512FCCE5LL},{0x9CA836C71256D33BLL,0xC387C2990DDC4514LL,1L}},{{0L,0L,0xDFF74AB362191487LL},{0xB58B636080EF93C9LL,0xC387C2990DDC4514LL,0xC387C2990DDC4514LL},{0xDFF74AB362191487LL,0x1F0EDB99D884E9E3LL,1L},{0xB58B636080EF93C9LL,1L,0xB58B636080EF93C9LL},{0L,0xDFF74AB362191487LL,1L}},{{0x9CA836C71256D33BLL,0x9CA836C71256D33BLL,0xC387C2990DDC4514LL},{0x45DEA4B9512FCCE5LL,0xDFF74AB362191487LL,0xDFF74AB362191487LL},{0xC387C2990DDC4514LL,1L,1L},{0x45DEA4B9512FCCE5LL,0x1F0EDB99D884E9E3LL,0x45DEA4B9512FCCE5LL},{0x9CA836C71256D33BLL,0xC387C2990DDC4514LL,1L}},{{0L,0L,0xDFF74AB362191487LL},{0xB58B636080EF93C9LL,0xC387C2990DDC4514LL,0xC387C2990DDC4514LL},{0xDFF74AB362191487LL,0x1F0EDB99D884E9E3LL,1L},{0xB58B636080EF93C9LL,1L,0xB58B636080EF93C9LL},{0L,0xDFF74AB362191487LL,1L}},{{0x9CA836C71256D33BLL,0x9CA836C71256D33BLL,0xC387C2990DDC4514LL},{0x45DEA4B9512FCCE5LL,0xDFF74AB362191487LL,0xDFF74AB362191487LL},{0xC387C2990DDC4514LL,1L,1L},{0x45DEA4B9512FCCE5LL,0x1F0EDB99D884E9E3LL,0x45DEA4B9512FCCE5LL},{0x9CA836C71256D33BLL,0xC387C2990DDC4514LL,1L}}};
    int8_t l_1929 = 0L;
    int32_t ***l_2034 = &g_168;
    uint64_t l_2037 = 0xD3BD0F320004F897LL;
    uint16_t *l_2044 = &g_140;
    struct S0 *l_2082 = &g_1217;
    int16_t l_2091 = 0xE1BDL;
    int32_t l_2138 = 6L;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 3; k++)
                l_1852[i][j][k] = &g_44.f0;
        }
    }
    for (g_258.f2 = 0; (g_258.f2 < (-19)); --g_258.f2)
    { 
        uint64_t l_1847 = 0x6E684FC504AC3704LL;
        int32_t l_1865[3][5] = {{0xDD898862L,0xDD898862L,0xDD898862L,0xDD898862L,0xDD898862L},{0x653B954EL,0x653B954EL,0x653B954EL,0x653B954EL,0x653B954EL},{0xDD898862L,0xDD898862L,0xDD898862L,0xDD898862L,0xDD898862L}};
        const union U1 * const l_1888[1] = {&g_44};
        uint64_t *l_1941 = &g_1864;
        uint64_t ** const l_1940 = &l_1941;
        uint64_t ** const *l_1939[3];
        uint8_t **l_1968 = &g_297;
        uint8_t ***l_1967 = &l_1968;
        uint8_t l_1990[1];
        int16_t *l_2006 = &g_1375;
        uint32_t ** const l_2017 = &g_1365;
        int32_t ***l_2036[8] = {&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168};
        uint16_t l_2058 = 0x572BL;
        uint16_t **l_2068 = (void*)0;
        uint16_t **l_2071 = (void*)0;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1939[i] = &l_1940;
        for (i = 0; i < 1; i++)
            l_1990[i] = 7UL;
        for (g_4 = (-19); (g_4 != (-29)); g_4--)
        { 
            int8_t *l_1876 = &g_1382;
            int32_t l_1877 = (-3L);
            uint16_t *l_1878 = (void*)0;
            uint16_t *l_1879 = &g_669;
            int8_t *l_1880[4] = {&g_126,&g_126,&g_126,&g_126};
            int32_t l_1881 = 0x0964EB7AL;
            int32_t l_1882 = (-9L);
            int i;
            if (l_1847)
                break;
            for (g_387.f0 = 29; (g_387.f0 != 31); ++g_387.f0)
            { 
                for (g_225 = 0; (g_225 > 19); g_225 = safe_add_func_uint16_t_u_u(g_225, 8))
                { 
                    int16_t l_1853 = (-1L);
                    int32_t l_1854 = (-1L);
                    l_1852[1][0][1] = (void*)0;
                    l_1854 ^= l_1853;
                    return l_1855;
                }
                if (l_1847)
                    break;
                if (p_7)
                    continue;
            }
            l_1881 |= (((p_7 || ((safe_mul_func_int8_t_s_s((g_126 ^= ((p_7 | (safe_mod_func_uint32_t_u_u(3UL, (((*l_1879) |= (((safe_lshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s((l_1865[0][1] = g_1864), ((safe_mod_func_uint64_t_u_u(((safe_mod_func_uint8_t_u_u(p_7, l_1870[6][0][2])) < ((safe_lshift_func_uint8_t_u_u((safe_div_func_uint8_t_u_u(0UL, 0xCFL)), (safe_unary_minus_func_int8_t_s(((*l_1876) = 0x9FL))))) || l_1877)), p_7)) ^ 4294967292UL))), 1)) && p_8.f0) > l_1847)) | p_9)))) < l_1877)), l_1847)) > l_1877)) && 9UL) >= 0UL);
            p_8.f0 = l_1882;
        }
        for (g_1667 = (-9); (g_1667 == (-26)); g_1667 = safe_sub_func_int8_t_s_s(g_1667, 1))
        { 
            int32_t *l_1885 = &l_1865[0][4];
            (*g_168) = func_29(l_1885, p_7);
        }
        for (g_49 = (-11); (g_49 != 48); g_49 = safe_add_func_int64_t_s_s(g_49, 3))
        { 
            uint16_t l_1904 = 0x4774L;
            int32_t l_1913[2];
            uint16_t l_1930[4];
            uint16_t l_2076 = 0xA471L;
            int i;
            for (i = 0; i < 2; i++)
                l_1913[i] = (-5L);
            for (i = 0; i < 4; i++)
                l_1930[i] = 65527UL;
            if ((l_1888[0] != (void*)0))
            { 
                int32_t * const l_1889 = &l_1865[0][1];
                int16_t *l_1905 = &g_795[0].f2;
                int32_t l_1920[8];
                uint64_t l_1950 = 4UL;
                int64_t l_1995 = 0x4F31DD7A1DE9F738LL;
                int64_t l_2002[6] = {0x85BD3568D6A8BD8ALL,0x85BD3568D6A8BD8ALL,0x85BD3568D6A8BD8ALL,0x85BD3568D6A8BD8ALL,0x85BD3568D6A8BD8ALL,0x85BD3568D6A8BD8ALL};
                int64_t *l_2007[10][8] = {{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]},{&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2],&l_2002[2]}};
                int16_t l_2008 = 0x5498L;
                union U1 *l_2009 = (void*)0;
                int i, j;
                for (i = 0; i < 8; i++)
                    l_1920[i] = (-2L);
                (*g_1890) = l_1889;
                if (((safe_lshift_func_int16_t_s_u((65527UL || 0UL), 14)) | (safe_add_func_int64_t_s_s((((**g_1632) &= (p_7 > (safe_unary_minus_func_int32_t_s((*g_1714))))) | l_1865[1][2]), ((safe_sub_func_int16_t_s_s((safe_add_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u(((*l_1905) = (p_7 > (safe_div_func_int32_t_s_s(((0xF3L | (65532UL && l_1904)) >= 1L), 0x7E136747L)))), l_1847)), 0xCD74L)), 0x7D4AL)) != g_1864)))))
                { 
                    uint16_t *l_1912[10][8] = {{&g_140,(void*)0,&g_140,(void*)0,(void*)0,&l_1904,&l_1904,(void*)0},{(void*)0,&l_1904,&l_1904,(void*)0,(void*)0,&g_140,(void*)0,&g_140},{(void*)0,&g_153,&l_1904,&g_153,(void*)0,&l_1904,&g_153,&g_153},{&g_140,&g_153,(void*)0,(void*)0,&g_153,&g_140,&l_1904,&g_153},{&g_153,&l_1904,(void*)0,&g_153,(void*)0,&l_1904,&g_153,&g_140},{&g_153,(void*)0,&l_1904,&g_153,&g_153,&l_1904,(void*)0,&g_153},{&g_140,&g_153,&l_1904,(void*)0,&g_153,(void*)0,&l_1904,&g_153},{&g_153,&l_1904,&g_140,&g_153,(void*)0,(void*)0,&g_153,&g_140},{&g_153,&g_153,&l_1904,(void*)0,&g_153,&l_1904,&g_153,(void*)0},{&g_140,(void*)0,&g_140,(void*)0,(void*)0,&l_1904,&l_1904,(void*)0}};
                    int32_t l_1914 = 0x7276B1A4L;
                    int32_t l_1915 = 0x2FC2AD2DL;
                    int32_t l_1916 = 0x51674CECL;
                    int32_t l_1917 = 2L;
                    int32_t l_1918 = (-10L);
                    int32_t l_1919 = 0x997E02D4L;
                    int32_t l_1921 = 0L;
                    int32_t l_1922 = 0xD6F69680L;
                    int32_t l_1923 = 0x17A41D8FL;
                    int32_t l_1924 = 0x65003071L;
                    int32_t l_1925 = 1L;
                    int32_t l_1926 = 1L;
                    int32_t l_1927 = 7L;
                    int32_t l_1928[9][5][5] = {{{0x04F2F411L,(-1L),6L,1L,8L},{0x04F2F411L,5L,0xCCBDD592L,0xB013B419L,1L},{0x6D667504L,(-1L),(-1L),7L,(-2L)},{0xB013B419L,(-1L),(-2L),1L,(-2L)},{6L,6L,(-1L),5L,6L}},{{0xEC54F10AL,0xCAB0BCE1L,0x1CEE0A39L,5L,(-2L)},{0x686FFE55L,5L,0x5B1453C6L,1L,3L},{1L,5L,1L,0xEC54F10AL,5L},{0x61816378L,1L,0x0E253DB9L,1L,(-2L)},{0x5D56C6B5L,0xCCBDD592L,(-4L),3L,0xB0A7051DL}},{{7L,0xA7510DC2L,0x0650E67DL,0x2540423DL,6L},{(-1L),7L,(-1L),1L,0x5D56C6B5L},{(-1L),(-1L),(-1L),0x6D667504L,(-7L)},{3L,0x36E21788L,0x0650E67DL,0x5B1453C6L,0x6C1677D7L},{(-10L),0x5D56C6B5L,(-4L),(-1L),1L}},{{6L,0x0E253DB9L,0x0E253DB9L,6L,(-1L)},{1L,0x5B1453C6L,1L,(-2L),6L},{(-1L),0x5D56C6B5L,(-7L),0x37872DCCL,0x2540423DL},{0xEC54F10AL,0x20C22579L,(-9L),(-2L),0xCCBDD592L},{0x36E21788L,0x1CEE0A39L,(-2L),6L,5L}},{{0x20C22579L,7L,0x37872DCCL,(-1L),0x6D667504L},{0xEC54F10AL,(-9L),(-1L),0x5B1453C6L,0x61816378L},{0x1CEE0A39L,(-2L),0xEFAC7CEDL,0x6D667504L,(-1L)},{(-7L),1L,0x20C22579L,1L,(-1L)},{6L,(-8L),(-9L),0x2540423DL,0x61816378L}},{{0x6C1677D7L,0x2540423DL,0x686FFE55L,3L,0x6D667504L},{0x104BA7B9L,5L,1L,1L,5L},{(-1L),(-7L),0x0E253DB9L,0xEC54F10AL,0xCCBDD592L},{0x5D56C6B5L,(-2L),0xB013B419L,3L,0x2540423DL},{1L,0xA7510DC2L,(-3L),0xB0A7051DL,6L}},{{0x5D56C6B5L,1L,(-1L),7L,(-1L)},{(-1L),0x1CEE0A39L,0x04F2F411L,0x6D667504L,1L},{0x104BA7B9L,0x36E21788L,(-3L),0x0E253DB9L,0x6C1677D7L},{0x6C1677D7L,(-1L),(-4L),0x1CEE0A39L,(-7L)},{6L,0x5B1453C6L,6L,6L,0x5D56C6B5L}},{{(-7L),0x5B1453C6L,1L,0xCCBDD592L,6L},{0x1CEE0A39L,(-1L),(-7L),(-1L),0xB0A7051DL},{0xEC54F10AL,0x36E21788L,1L,(-2L),(-2L)},{0x20C22579L,0x1CEE0A39L,0x20C22579L,(-1L),5L},{0x36E21788L,1L,0x37872DCCL,0x1CEE0A39L,(-3L)}},{{0xEC54F10AL,0xA7510DC2L,5L,0x5B1453C6L,(-1L)},{(-1L),(-2L),0x37872DCCL,(-3L),(-1L)},{1L,(-7L),0x20C22579L,7L,0x37872DCCL},{6L,5L,1L,0x2540423DL,(-1L)},{(-10L),0x2540423DL,(-7L),0x104BA7B9L,0x6D667504L}}};
                    uint8_t *l_1948 = (void*)0;
                    uint8_t *l_1949 = &l_1855;
                    uint32_t l_1957 = 4294967295UL;
                    int32_t **l_1969 = (void*)0;
                    uint8_t l_1986 = 2UL;
                    int i, j, k;
                    if ((safe_lshift_func_uint8_t_u_u(((*l_1949) = ((safe_rshift_func_int8_t_s_u((-7L), ((safe_sub_func_uint16_t_u_u((--l_1930[1]), (safe_unary_minus_func_int32_t_s((safe_mul_func_int8_t_s_s((safe_add_func_int16_t_s_s((safe_unary_minus_func_int32_t_s((((l_1939[1] = (void*)0) != &g_427) < 7UL))), ((l_1847 > (safe_div_func_uint16_t_u_u(p_9, l_1913[0]))) == (((safe_rshift_func_uint16_t_u_u((safe_add_func_int64_t_s_s(((l_1913[0] < (*l_1889)) ^ p_8.f0), l_1847)), p_7)) || p_8.f0) >= p_9)))), 0x73L)))))) || p_9))) && (**g_897))), (*l_1889))))
                    { 
                        int32_t l_1970[7];
                        int8_t l_1980 = 3L;
                        int8_t *l_1981 = &l_1929;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_1970[i] = 0x153E7DD5L;
                        g_4 ^= (l_1950 > (safe_add_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_s(((((safe_lshift_func_int16_t_s_s(l_1957, (0UL == ((safe_add_func_int32_t_s_s((l_1970[5] |= ((g_1962 != (void*)0) & (!(safe_rshift_func_int8_t_s_s((((*g_168) = func_79((g_1966 != l_1967), l_1969, &g_44, (*l_1889))) == &l_1920[2]), 3))))), p_7)) == p_9)))) >= 0xCA0EA93BL) && l_1970[5]) ^ 0x0AL), 2)), p_7)));
                        p_8.f0 = (((*l_1981) = (((((l_1913[0] <= (safe_mod_func_int64_t_s_s(((safe_mod_func_uint32_t_u_u(((**g_1364) = 0xC6DEC3B4L), (safe_add_func_int32_t_s_s((safe_div_func_int8_t_s_s(p_8.f0, ((l_1970[5] >= (((void*)0 == &g_525) >= g_1979)) & (((*l_1889) = 0L) >= (**g_1890))))), p_9)))) & l_1980), 3UL))) > p_7) != l_1930[1]) == 0x85L) != p_7)) | g_150);
                    }
                    else
                    { 
                        (*g_525) = (*g_525);
                        if ((**g_315))
                            continue;
                    }
                    if (p_8.f0)
                    { 
                        uint8_t l_1982 = 0UL;
                        int32_t l_1985 = 0x35EE7E10L;
                        ++l_1982;
                        l_1986++;
                    }
                    else
                    { 
                        int8_t l_1989 = (-6L);
                        l_1990[0]++;
                        (*g_1714) = ((*g_169) = (****g_941));
                        g_1994 = g_1993[1][4];
                    }
                }
                else
                { 
                    int32_t l_1996 = 0x50A04E65L;
                    int32_t l_1997 = (-1L);
                    int32_t l_1998 = 8L;
                    int32_t l_1999 = 0L;
                    int32_t l_2000 = 0xA15DB3C8L;
                    int32_t l_2001[7][3][4] = {{{0x2F287F49L,0L,0x653BA288L,0xA18DE912L},{1L,0L,2L,2L},{(-6L),(-6L),2L,(-10L)}},{{1L,0x2F287F49L,0x653BA288L,0L},{4L,0x653BA288L,(-10L),0x653BA288L},{(-10L),0x653BA288L,4L,0L}},{{0x653BA288L,0x2F287F49L,1L,(-10L)},{2L,(-6L),(-6L),2L},{2L,0L,1L,0xA18DE912L}},{{0x653BA288L,2L,4L,0x15F1A3D5L},{(-10L),1L,(-10L),0x15F1A3D5L},{4L,2L,0x653BA288L,0xA18DE912L}},{{1L,0L,2L,2L},{(-6L),(-6L),2L,(-10L)},{1L,0x2F287F49L,0x653BA288L,0L}},{{4L,0x653BA288L,(-10L),0x653BA288L},{(-10L),0x653BA288L,4L,0L},{0x653BA288L,0x2F287F49L,1L,(-10L)}},{{2L,(-6L),(-6L),2L},{2L,0L,1L,0xA18DE912L},{0x653BA288L,2L,4L,0x15F1A3D5L}}};
                    uint16_t l_2003 = 0x2347L;
                    int i, j, k;
                    l_2003--;
                }
                (*g_1714) = p_8.f0;
                (*g_168) = func_79((l_2008 &= (l_2006 != (void*)0)), &g_169, l_2009, ((safe_add_func_int64_t_s_s(p_9, (~(((safe_sub_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(p_7, ((((-9L) && ((p_8.f0 || p_8.f0) == 0x28L)) | (-1L)) != (-9L)))), l_1930[3])) != (***g_896)) == 0x1FC9L)))) >= g_44.f0));
            }
            else
            { 
                const uint32_t *l_2019 = &g_387.f0;
                const uint32_t ** const l_2018 = &l_2019;
                int32_t ****l_2035 = &l_2034;
                uint16_t *l_2043[10][5] = {{(void*)0,(void*)0,(void*)0,&l_1930[1],&l_1904},{&g_153,&l_1904,(void*)0,&g_153,&l_1904},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_140,(void*)0,(void*)0,&l_1904,&l_1904},{&g_140,(void*)0,(void*)0,(void*)0,&g_140},{(void*)0,&g_153,&g_153,&l_1904,(void*)0},{&g_153,&g_153,(void*)0,(void*)0,&g_153},{(void*)0,(void*)0,&g_140,&g_153,(void*)0},{(void*)0,(void*)0,&g_140,&l_1930[1],&g_140},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1904}};
                int i, j;
                if (((l_2017 != l_2018) && (safe_mul_func_int16_t_s_s((&g_297 != (void*)0), ((safe_mul_func_uint16_t_u_u(p_9, ((safe_lshift_func_uint8_t_u_s((safe_add_func_int64_t_s_s((safe_sub_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u((safe_unary_minus_func_uint16_t_u((safe_sub_func_uint32_t_u_u(p_7, (((*l_2035) = l_2034) != l_2036[6]))))))), (&g_126 == (void*)0))), 0UL)), 7)) && 0xCC101B6AB41D1420LL))) != p_8.f0)))))
                { 
                    l_2037--;
                }
                else
                { 
                    uint16_t l_2040 = 0xCCC7L;
                    uint16_t **l_2045 = &l_2043[9][3];
                    int32_t *l_2064 = &l_1865[0][1];
                    l_2040 = p_8.f0;
                    (*g_168) = func_29(((***l_2035) = (void*)0), ((safe_lshift_func_int8_t_s_u((((*l_2045) = (l_2044 = l_2043[0][1])) == (void*)0), 1)) <= ((**g_1632) = l_2040)));
                    if (((****l_2035) > l_2040))
                    { 
                        return p_9;
                    }
                    else
                    { 
                        uint16_t l_2057 = 0x98A4L;
                        l_1913[1] = (safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(p_8.f0, 4)), ((+(((safe_sub_func_int64_t_s_s((((***l_2034) = (((safe_rshift_func_uint16_t_u_u(p_8.f0, ((safe_add_func_int64_t_s_s(l_2057, l_2058)) || p_7))) == (0x54L >= ((safe_mul_func_int8_t_s_s(p_9, (safe_add_func_int8_t_s_s(g_796.f2, (!(0x49L >= p_9)))))) == l_1930[1]))) == l_1904)) ^ 1L), 0x6D89906932A0E510LL)) || 0xD1C41DA9L) && (-8L))) >= p_9)));
                        l_2064 = ((*g_168) = func_29(&l_1913[0], (****l_2035)));
                    }
                    (***l_2035) = (void*)0;
                }
            }
            if (l_1913[0])
            { 
                int16_t l_2065 = 0xDB8EL;
                if (l_2065)
                    break;
                for (g_504.f0 = 0; (g_504.f0 != 39); g_504.f0 = safe_add_func_uint16_t_u_u(g_504.f0, 2))
                { 
                    if (p_9)
                        break;
                }
                p_8.f0 = 0L;
            }
            else
            { 
                uint16_t ***l_2069 = &l_2068;
                uint16_t ***l_2070 = &g_1769;
                int32_t l_2072 = 0x2EFC3E16L;
                struct S0 **l_2073 = &g_1438;
                l_2072 |= (0x5D43L > (0x2461L != (&l_2044 != (l_2071 = ((*l_2070) = ((*l_2069) = l_2068))))));
                (*l_2073) = (*g_1461);
                for (g_44.f0 = (-6); (g_44.f0 >= 18); ++g_44.f0)
                { 
                    uint8_t l_2077 = 255UL;
                    l_2076 |= ((void*)0 == &g_297);
                    return l_2077;
                }
            }
            for (g_1667 = 8; (g_1667 >= (-22)); g_1667 = safe_sub_func_int8_t_s_s(g_1667, 1))
            { 
                union U1 *l_2080[7][2] = {{&g_44,&g_44},{&g_44,&g_44},{&g_44,&g_44},{&g_44,&g_44},{&g_44,&g_44},{&g_44,&g_44},{&g_44,&g_44}};
                int i, j;
                for (g_1462.f2 = 0; (g_1462.f2 >= 0); g_1462.f2 -= 1)
                { 
                    union U1 **l_2081 = &g_180[3];
                    (*l_2081) = l_2080[5][0];
                }
            }
        }
        (*g_2084) = l_2082;
    }
    for (p_9 = 0; (p_9 == 52); ++p_9)
    { 
        int16_t l_2090 = 0xB968L;
        int32_t l_2095 = 1L;
        uint64_t **l_2099 = &g_428;
        int32_t l_2119 = 0xF144FC36L;
        if ((*g_5))
        { 
            const int32_t **l_2087 = &g_22[0][1];
            (*l_2087) = (*g_315);
        }
        else
        { 
            uint32_t l_2092 = 0UL;
            const uint64_t l_2096 = 1UL;
            int32_t l_2097 = 0x099D12E8L;
            for (g_1375 = 0; (g_1375 == (-10)); --g_1375)
            { 
                uint64_t * const *l_2098 = &g_428;
                ++l_2092;
                if (p_9)
                    continue;
                if (p_7)
                    continue;
                for (g_4 = 0; (g_4 <= 9); g_4 += 1)
                { 
                    uint64_t ***l_2100 = (void*)0;
                    uint64_t ***l_2101 = &g_427;
                    l_2095 = p_9;
                    l_2097 = l_2096;
                    l_2097 |= (l_2098 != ((*l_2101) = l_2099));
                }
            }
            return l_2090;
        }
        for (g_44.f0 = 1; (g_44.f0 <= 6); g_44.f0 += 1)
        { 
            uint32_t l_2108 = 1UL;
            int32_t l_2120 = 0xA786DC87L;
            uint32_t l_2122 = 0xA9576C3AL;
            int64_t ***l_2154 = &g_2152;
            for (g_669 = 0; (g_669 <= 9); g_669 += 1)
            { 
                int64_t l_2117[6][8][5] = {{{0x00DCA90BF8270FB3LL,(-7L),0xCC549E4636797D26LL,1L,0x0487D462B8F62AE9LL},{(-3L),(-7L),0x835B10D7C925351ALL,1L,1L},{1L,4L,1L,0xBFCC372E95A33777LL,6L},{6L,0xE3750E14271B1EB3LL,(-1L),0x4A305CB5CB31AE36LL,4L},{0x277CE7B3845F4D81LL,0L,0x13E7186E901F015BLL,1L,0xD53833360B9C9CF4LL},{0xDC8EE395747D4601LL,0xCC549E4636797D26LL,(-1L),4L,0xB8A6A6F13939222CLL},{0x584B6E33A9F7E9AALL,0x8BED324FBFEF1AE0LL,1L,0x5E20D4B99E5D4495LL,(-7L)},{0x9550E6C80B80320BLL,(-1L),0x835B10D7C925351ALL,1L,(-5L)}},{{0x8BED324FBFEF1AE0LL,6L,0xCC549E4636797D26LL,1L,(-7L)},{0x277CE7B3845F4D81LL,(-3L),1L,(-3L),0x277CE7B3845F4D81LL},{0xCC549E4636797D26LL,0x277CE7B3845F4D81LL,6L,0x0EBB336336C5FA4ALL,0x00DCA90BF8270FB3LL},{0xDC8EE395747D4601LL,1L,0x277CE7B3845F4D81LL,0xD53833360B9C9CF4LL,(-5L)},{4L,(-7L),0xDC8EE395747D4601LL,0x277CE7B3845F4D81LL,0x00DCA90BF8270FB3LL},{1L,0xD53833360B9C9CF4LL,0x584B6E33A9F7E9AALL,(-1L),0x277CE7B3845F4D81LL},{0x00DCA90BF8270FB3LL,0x835B10D7C925351ALL,0x9550E6C80B80320BLL,0x995452CD7513CC3BLL,(-7L)},{0x584B6E33A9F7E9AALL,0x13E7186E901F015BLL,0x8BED324FBFEF1AE0LL,0x173D64C54516A6F0LL,(-3L)}},{{0x584B6E33A9F7E9AALL,0xCF5F2FBD5A26F0CALL,0x4A305CB5CB31AE36LL,0xDC8EE395747D4601LL,0x4A305CB5CB31AE36LL},{0x00DCA90BF8270FB3LL,0x00DCA90BF8270FB3LL,0x13E7186E901F015BLL,(-5L),0x584B6E33A9F7E9AALL},{1L,0xBFCC372E95A33777LL,0x173D64C54516A6F0LL,0xB8A6A6F13939222CLL,(-5L)},{4L,(-1L),(-1L),1L,0x0EBB336336C5FA4ALL},{0xDC8EE395747D4601LL,0xBFCC372E95A33777LL,0x995452CD7513CC3BLL,0xCC549E4636797D26LL,0x5E20D4B99E5D4495LL},{0xCC549E4636797D26LL,0x00DCA90BF8270FB3LL,0x835B10D7C925351ALL,0x9550E6C80B80320BLL,0x995452CD7513CC3BLL},{0x277CE7B3845F4D81LL,0xCF5F2FBD5A26F0CALL,0xB8A6A6F13939222CLL,(-7L),1L},{0x9550E6C80B80320BLL,0x13E7186E901F015BLL,0xB8A6A6F13939222CLL,0x8BED324FBFEF1AE0LL,0xD53833360B9C9CF4LL}},{{(-1L),0x835B10D7C925351ALL,0x835B10D7C925351ALL,(-1L),0L},{0xE3750E14271B1EB3LL,0xD53833360B9C9CF4LL,0x995452CD7513CC3BLL,0x0487D462B8F62AE9LL,4L},{0xBFCC372E95A33777LL,(-7L),(-1L),0x3B3FF79C7E20A813LL,0x173D64C54516A6F0LL},{0xB8A6A6F13939222CLL,1L,0x173D64C54516A6F0LL,0x0487D462B8F62AE9LL,(-1L)},{0x5E20D4B99E5D4495LL,0x277CE7B3845F4D81LL,0x13E7186E901F015BLL,(-1L),0x3B3FF79C7E20A813LL},{0xD53833360B9C9CF4LL,(-3L),0x4A305CB5CB31AE36LL,0x8BED324FBFEF1AE0LL,6L},{1L,0x5E20D4B99E5D4495LL,0x8BED324FBFEF1AE0LL,(-7L),6L},{1L,0x3B3FF79C7E20A813LL,0x9550E6C80B80320BLL,0x9550E6C80B80320BLL,0x3B3FF79C7E20A813LL}},{{(-5L),0x9550E6C80B80320BLL,0x584B6E33A9F7E9AALL,0xCC549E4636797D26LL,(-1L)},{(-7L),(-3L),0xDC8EE395747D4601LL,1L,0x173D64C54516A6F0LL},{(-3L),(-5L),0x277CE7B3845F4D81LL,0xB8A6A6F13939222CLL,4L},{(-7L),0x0487D462B8F62AE9LL,6L,(-5L),0L},{(-5L),0x0EBB336336C5FA4ALL,1L,0xDC8EE395747D4601LL,0xD53833360B9C9CF4LL},{1L,0x4A305CB5CB31AE36LL,(-3L),0x173D64C54516A6F0LL,1L},{1L,0x4A305CB5CB31AE36LL,0x00DCA90BF8270FB3LL,0x995452CD7513CC3BLL,0x995452CD7513CC3BLL},{0xD53833360B9C9CF4LL,0x0EBB336336C5FA4ALL,0xD53833360B9C9CF4LL,(-1L),0x5E20D4B99E5D4495LL}},{{0x5E20D4B99E5D4495LL,0x0487D462B8F62AE9LL,0x3B3FF79C7E20A813LL,0x277CE7B3845F4D81LL,0x0EBB336336C5FA4ALL},{0xB8A6A6F13939222CLL,(-5L),0xCC549E4636797D26LL,0xD53833360B9C9CF4LL,(-5L)},{0xBFCC372E95A33777LL,(-3L),0x3B3FF79C7E20A813LL,0x0EBB336336C5FA4ALL,0x584B6E33A9F7E9AALL},{0xE3750E14271B1EB3LL,0x9550E6C80B80320BLL,0xD53833360B9C9CF4LL,(-3L),0x4A305CB5CB31AE36LL},{(-1L),0x3B3FF79C7E20A813LL,0x00DCA90BF8270FB3LL,0L,(-3L)},{0x9550E6C80B80320BLL,0x5E20D4B99E5D4495LL,(-3L),0L,(-7L)},{0x277CE7B3845F4D81LL,(-3L),1L,(-3L),0x277CE7B3845F4D81LL},{(-3L),0xB8A6A6F13939222CLL,0x5E20D4B99E5D4495LL,0xCF5F2FBD5A26F0CALL,1L}}};
                uint8_t l_2118 = 246UL;
                int32_t l_2128 = 0x4515FDC8L;
                int i, j, k;
                if (l_2095)
                { 
                    int16_t *****l_2116 = &g_2114;
                    int32_t l_2121[10][8][3] = {{{0x23E5F124L,0xCCA93AC1L,2L},{6L,(-1L),0x06CBCD58L},{1L,9L,2L},{1L,8L,(-3L)},{0xE7F1C2C1L,1L,1L},{0x8A848A5DL,(-6L),0x8A848A5DL},{0xCCA93AC1L,(-1L),0x5897C50DL},{(-3L),8L,1L}},{{0xB8E1E51CL,0x23E5F124L,(-1L)},{0x06CBCD58L,(-1L),6L},{0xB8E1E51CL,1L,9L},{(-3L),(-6L),0x813820F5L},{0xCCA93AC1L,0x9F43467EL,0x85020AECL},{0x8A848A5DL,(-1L),7L},{0xE7F1C2C1L,0x85379C62L,9L},{1L,0x8090DB50L,(-1L)}},{{1L,0x9AD59E57L,(-1L)},{6L,0x8090DB50L,0xE2DB58E4L},{0x23E5F124L,0x85379C62L,0x5897C50DL},{0x813820F5L,(-1L),(-5L)},{0x9F43467EL,0x9F43467EL,1L},{7L,(-6L),1L},{0x23E5F124L,1L,2L},{(-1L),(-1L),(-1L)}},{{1L,0x23E5F124L,2L},{0xE2DB58E4L,8L,1L},{0xE7F1C2C1L,(-1L),1L},{(-5L),(-6L),(-5L)},{0xCCA93AC1L,1L,0x5897C50DL},{1L,8L,0xE2DB58E4L},{0xB8E1E51CL,9L,(-1L)},{(-1L),(-1L),(-1L)}},{{0xB8E1E51CL,0xCCA93AC1L,9L},{1L,(-6L),7L},{0xCCA93AC1L,0x85020AECL,0x85020AECL},{(-5L),(-1L),0x813820F5L},{0xE7F1C2C1L,0xE5D2413BL,9L},{0xE2DB58E4L,0x8090DB50L,6L},{1L,0L,(-1L)},{(-1L),0x8090DB50L,1L}},{{0x23E5F124L,0xE5D2413BL,0x5897C50DL},{7L,(-1L),0x8A848A5DL},{0x9F43467EL,0x85020AECL,1L},{0x813820F5L,(-6L),(-3L)},{0x23E5F124L,0xCCA93AC1L,2L},{6L,(-1L),0x06CBCD58L},{1L,9L,2L},{1L,8L,(-3L)}},{{0xE7F1C2C1L,1L,1L},{0x8A848A5DL,(-6L),0x8A848A5DL},{0xCCA93AC1L,(-1L),0x5897C50DL},{(-3L),8L,1L},{0xB8E1E51CL,0x23E5F124L,(-1L)},{0x06CBCD58L,(-1L),6L},{0xB8E1E51CL,1L,9L},{(-3L),(-6L),0x813820F5L}},{{0xCCA93AC1L,0x9F43467EL,0x85020AECL},{0x8A848A5DL,(-1L),7L},{0xE7F1C2C1L,0x85379C62L,9L},{1L,0x8090DB50L,(-1L)},{1L,0x9AD59E57L,(-1L)},{6L,0x8090DB50L,0xE2DB58E4L},{0x9AD59E57L,2L,(-1L)},{1L,(-6L),0L}},{{1L,1L,0x85020AECL},{0xE2DB58E4L,(-1L),0x80ED38A1L},{0x9AD59E57L,0x85020AECL,0x5897C50DL},{(-1L),(-7L),(-1L)},{1L,0x9AD59E57L,0x5897C50DL},{0x8A848A5DL,0x83EAC76CL,0x80ED38A1L},{1L,1L,0x85020AECL},{0L,0x598CDD17L,0L}},{{0x9F43467EL,0xCCA93AC1L,(-1L)},{0x80ED38A1L,0x83EAC76CL,0x8A848A5DL},{0xE7F1C2C1L,0L,(-1L)},{(-1L),(-7L),(-1L)},{0xE7F1C2C1L,0x9F43467EL,0L},{0x80ED38A1L,(-1L),0xE2DB58E4L},{0x9F43467EL,0x9A0B6C03L,0x9A0B6C03L},{0L,(-6L),1L}}};
                    volatile uint16_t ** volatile *l_2125 = &g_646;
                    int i, j, k;
                    l_2120 |= ((safe_add_func_int16_t_s_s(((**g_1632) |= (safe_div_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_u(l_2108, ((&p_8 == &p_8) <= (~((*g_1365) &= ((&g_218[g_44.f0] != ((*g_896) = &g_218[g_44.f0])) < (l_2108 | (6L | ((safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s((((((*l_2116) = g_2114) != (void*)0) & p_7) != p_8.f0), p_8.f0)), 0xFCL)) >= 9L))))))))) > 0x58L), l_2117[1][1][4]))), l_2118)) == l_2119);
                    l_2122 &= l_2121[7][1][2];
                    (*l_2125) = (*g_645);
                }
                else
                { 
                    uint8_t *l_2129 = &l_2118;
                    int32_t l_2144[7][2][6] = {{{0x84B6D700L,1L,1L,0L,0x221494FBL,0x84B6D700L},{(-9L),1L,0L,(-9L),1L,(-9L)}},{{(-9L),1L,(-9L),0L,1L,(-9L)},{0x84B6D700L,0x221494FBL,0L,1L,1L,0x84B6D700L}},{{0L,1L,1L,1L,1L,0L},{0x84B6D700L,1L,1L,0L,0x221494FBL,0x84B6D700L}},{{(-9L),1L,0L,(-9L),1L,(-9L)},{(-9L),1L,(-9L),0L,1L,(-9L)}},{{0x84B6D700L,0x221494FBL,0L,1L,1L,0x84B6D700L},{0L,1L,1L,1L,1L,0L}},{{0x84B6D700L,1L,1L,0L,0x221494FBL,0x84B6D700L},{(-9L),1L,0L,(-9L),1L,(-9L)}},{{(-9L),1L,(-9L),0L,1L,(-9L)},{0x84B6D700L,0x221494FBL,0L,1L,1L,0x84B6D700L}}};
                    int8_t *l_2145[3];
                    uint8_t l_2146 = 8UL;
                    int32_t l_2149 = 0xC76E8D2CL;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_2145[i] = &g_74;
                    l_2128 |= (safe_div_func_int64_t_s_s((p_8.f0 != ((*l_2129)--)), p_7));
                    l_2128 &= (((safe_rshift_func_uint16_t_u_u(0x9D66L, 14)) < (p_7 == ((safe_rshift_func_int8_t_s_s((p_8.f0 > (((safe_sub_func_int16_t_s_s(l_2138, ((((l_2149 = (((((g_126 = (g_74 |= (p_8.f0 <= (safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint8_t_u_s(((l_2144[1][0][5] = l_2144[4][1][2]) & l_2117[5][1][2]), 2))))))) > (l_2146 < (safe_lshift_func_int8_t_s_u((l_2149 & 0x4FA755584E2BBA6DLL), 5)))) < l_2146) == p_8.f0) == 0UL)) < l_2108) == l_2119) <= p_8.f0))) == p_8.f0) && p_9)), p_8.f0)) | 0xA0B5BCDCEC458AF3LL))) >= g_1375);
                    for (l_2120 = 6; (l_2120 >= 0); l_2120 -= 1)
                    { 
                        p_8.f0 |= ((!5L) & (p_7 >= (((l_2154 = g_2151) == (void*)0) == (safe_div_func_uint64_t_u_u(l_2122, 0xABE926F5D9AEB8EFLL)))));
                    }
                }
                (*g_168) = (void*)0;
                (**l_2034) = &l_2120;
                p_8.f0 |= 0L;
            }
        }
    }
    p_8.f0 = ((p_8.f0 && (~0x91750551B70F82EDLL)) >= p_9);
    return p_8.f0;
}



static uint32_t  func_12(int64_t  p_13, int32_t * p_14, int32_t * p_15)
{ 
    uint32_t ***l_1811 = &g_640;
    uint32_t ****l_1810 = &l_1811;
    int8_t l_1812 = 9L;
    uint8_t *l_1813 = &g_225;
    union U1 l_1814 = {0x7D387BE9L};
    int32_t l_1815 = 0L;
    uint16_t *l_1818 = &g_153;
    const union U1 *l_1832 = (void*)0;
    const union U1 **l_1831 = &l_1832;
    const union U1 ***l_1830 = &l_1831;
    int32_t l_1833[5][6] = {{0L,0xCB2E3FC5L,0L,0L,0xCB2E3FC5L,0L},{0L,0xCB2E3FC5L,0L,0L,0xCB2E3FC5L,0L},{0L,0xCB2E3FC5L,0L,0L,0xCB2E3FC5L,0L},{0L,0xCB2E3FC5L,0L,0L,0xCB2E3FC5L,0L},{0L,0xCB2E3FC5L,0L,0L,0xCB2E3FC5L,0L}};
    int32_t l_1834 = 1L;
    const int64_t l_1836 = (-1L);
    int32_t l_1837[8][1][10] = {{{8L,(-1L),0x46DADD1CL,0x6DC1706FL,(-7L),1L,8L,(-9L),1L,0x46DADD1CL}},{{(-1L),(-9L),0x6257C3EAL,(-1L),(-7L),5L,(-7L),(-1L),0x6257C3EAL,(-9L)}},{{(-7L),8L,(-5L),0x46DADD1CL,0xD90BDF4DL,(-5L),0x6DC1706FL,7L,5L,(-3L)}},{{0xB828FF71L,8L,0xD90BDF4DL,0x6257C3EAL,0x475455EFL,(-5L),(-5L),0x475455EFL,0x6257C3EAL,0xD90BDF4DL}},{{0x394D8C31L,0x394D8C31L,(-7L),0x475455EFL,1L,0x1EE803CAL,0x46DADD1CL,0x394D8C31L,0x48CA462DL,0x46DADD1CL}},{{8L,0xB828FF71L,(-5L),(-3L),0xB828FF71L,0x6257C3EAL,0x46DADD1CL,0x6257C3EAL,0xB828FF71L,(-3L)}},{{5L,0x394D8C31L,5L,(-5L),0x46DADD1CL,0xD90BDF4DL,(-5L),0x6DC1706FL,7L,5L}},{{0x6DC1706FL,8L,0x48CA462DL,0x475455EFL,0x394D8C31L,(-9L),0x6DC1706FL,0x6DC1706FL,(-9L),0x394D8C31L}}};
    int32_t *l_1838 = (void*)0;
    uint8_t *l_1839 = &g_441;
    int8_t l_1840 = 0xD4L;
    int32_t **l_1841[8][2][6] = {{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}},{{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838},{&l_1838,&l_1838,&l_1838,&l_1838,&l_1838,&l_1838}}};
    int i, j, k;
    (*g_168) = l_1838;
    l_1840 = ((*p_14) = (((*l_1839) = ((*l_1813) = g_158.f2)) && 0xFEL));
    p_15 = ((*g_168) = (*g_1442));
    return (*g_1365);
}



static uint32_t * func_29(int32_t * p_30, uint64_t  p_31)
{ 
    uint32_t *l_1808 = &g_387.f0;
    return l_1808;
}



static int32_t * func_32(uint8_t  p_33, uint8_t  p_34, union U1  p_35, int8_t  p_36)
{ 
    int32_t *l_39 = &g_4;
    int64_t *l_1666[8] = {&g_1667,&g_1667,&g_1667,&g_1667,&g_1667,&g_1667,&g_1667,&g_1667};
    int64_t **l_1665 = &l_1666[0];
    int64_t ***l_1664 = &l_1665;
    const int64_t ***l_1683 = (void*)0;
    const int64_t *l_1691 = &g_1688;
    const int64_t **l_1690[4][9] = {{&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691},{&l_1691,(void*)0,&l_1691,(void*)0,&l_1691,&l_1691,(void*)0,(void*)0,&l_1691},{&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691,&l_1691},{&l_1691,(void*)0,(void*)0,&l_1691,&l_1691,(void*)0,&l_1691,&l_1691,(void*)0}};
    const int64_t ***l_1689 = &l_1690[3][6];
    int32_t l_1716 = 0x194CFBFEL;
    int32_t l_1717 = 0x618B002FL;
    int32_t l_1718 = 0xF41FA6C4L;
    int32_t l_1720[9][5][1] = {{{9L},{0xD934379EL},{(-1L)},{0x8759AA8BL},{0x75A73053L}},{{1L},{0x3B67F04BL},{1L},{0x75A73053L},{0x8759AA8BL}},{{(-1L)},{0xD934379EL},{9L},{9L},{0xD934379EL}},{{(-1L)},{0x8759AA8BL},{0x75A73053L},{1L},{0x3B67F04BL}},{{1L},{0x75A73053L},{0x8759AA8BL},{(-1L)},{0xD934379EL}},{{9L},{9L},{0xD934379EL},{(-1L)},{0x8759AA8BL}},{{0x75A73053L},{1L},{0x3B67F04BL},{1L},{0x75A73053L}},{{0x8759AA8BL},{(-1L)},{0xD934379EL},{9L},{9L}},{{0xD934379EL},{(-1L)},{0x8759AA8BL},{0x75A73053L},{1L}}};
    int16_t **l_1732 = &g_218[7];
    int32_t *l_1747[9] = {&l_1718,&l_1718,(void*)0,&l_1718,&l_1718,(void*)0,&l_1718,&l_1718,(void*)0};
    uint16_t l_1764[3][5] = {{0x0AE3L,0x0AE3L,0x0AE3L,0x0AE3L,0x0AE3L},{0xC73EL,65528UL,0xC73EL,65528UL,0xC73EL},{0x0AE3L,0x0AE3L,0x0AE3L,0x0AE3L,0x0AE3L}};
    uint32_t l_1785 = 0xCE6A0773L;
    uint32_t l_1791 = 1UL;
    const int32_t **l_1807 = (void*)0;
    int i, j, k;
    for (p_34 = 0; (p_34 <= 2); p_34 += 1)
    { 
        uint32_t l_1655 = 0UL;
        int32_t l_1671 = 0xFD1136F1L;
        const int64_t *l_1687 = &g_1688;
        const int64_t **l_1686 = &l_1687;
        const int64_t ***l_1685 = &l_1686;
        const int64_t ***l_1692 = &l_1690[3][6];
        int32_t l_1719 = 0xF03CCB32L;
        int32_t l_1721 = 0x0ACD3119L;
        uint32_t l_1722 = 1UL;
        struct S0 *l_1725 = &g_387;
        int32_t l_1770 = 0x72E6B14BL;
        int32_t l_1783 = 7L;
        uint32_t **l_1801 = &g_1365;
        for (p_35.f0 = 2; (p_35.f0 >= 0); p_35.f0 -= 1)
        { 
            union U1 *l_43[2][8][10] = {{{(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,(void*)0,&g_44},{&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0},{&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44},{(void*)0,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,(void*)0},{&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44,(void*)0,(void*)0,&g_44,&g_44,&g_44,&g_44},{&g_44,(void*)0,(void*)0,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,(void*)0},{(void*)0,(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44,&g_44,(void*)0,&g_44,&g_44,&g_44,(void*)0},{&g_44,&g_44,(void*)0,(void*)0,&g_44,&g_44,&g_44,&g_44,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_44,&g_44,&g_44,&g_44,&g_44,&g_44}}};
            int32_t l_1651[8];
            int32_t l_1652[10][1][10] = {{{(-7L),(-7L),7L,0xCFDB868AL,0xB2DD3B2EL,0x26E67594L,0x1347E998L,0xEFFB2C3AL,0x1347E998L,0x26E67594L}},{{0x691DEFC5L,7L,0L,7L,0x691DEFC5L,(-1L),0x1347E998L,(-7L),0x71ADE004L,0x71ADE004L}},{{0xCFDB868AL,(-7L),0xEA11FBB3L,0x26E67594L,0x26E67594L,0xEA11FBB3L,(-7L),0xCFDB868AL,0L,0x71ADE004L}},{{0xEFFB2C3AL,0x26E67594L,(-7L),8L,0x691DEFC5L,0x1347E998L,0x691DEFC5L,8L,(-7L),0x26E67594L}},{{0xEA11FBB3L,(-1L),(-7L),0x691DEFC5L,0xB2DD3B2EL,8L,0xCFDB868AL,0xCFDB868AL,8L,0xB2DD3B2EL}},{{0x71ADE004L,0xEA11FBB3L,0xEA11FBB3L,0x71ADE004L,7L,8L,0xEFFB2C3AL,(-7L),0xB2DD3B2EL,(-7L)}},{{0xEA11FBB3L,0x1347E998L,0L,(-7L),0L,0x1347E998L,0xEA11FBB3L,0xEFFB2C3AL,0xB2DD3B2EL,(-1L)}},{{0xEFFB2C3AL,8L,7L,0x71ADE004L,0xEA11FBB3L,0xEA11FBB3L,0x71ADE004L,7L,8L,0xEFFB2C3AL}},{{0xCFDB868AL,8L,0xB2DD3B2EL,0x691DEFC5L,(-7L),(-1L),0xEA11FBB3L,(-1L),(-7L),0x691DEFC5L}},{{0x691DEFC5L,0x1347E998L,0x691DEFC5L,8L,(-7L),0x26E67594L,0xEFFB2C3AL,0L,0L,0xEFFB2C3AL}}};
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_1651[i] = 1L;
            for (g_4 = 0; (g_4 <= 2); g_4 += 1)
            { 
                return l_39;
            }
            for (p_36 = 0; (p_36 <= 2); p_36 += 1)
            { 
                int i, j;
                if (p_34)
                    break;
                if (p_36)
                    break;
            }
        }
        if (((((((((l_1655 < l_1655) <= (safe_mul_func_int16_t_s_s(l_1655, (l_1671 ^= ((safe_add_func_int64_t_s_s(((***l_1664) = ((safe_mul_func_int16_t_s_s((safe_div_func_uint16_t_u_u(0xD502L, (0x85739D71L && (((void*)0 == l_1664) | ((((~(((~0x3408D5FB71D204D9LL) ^ (*l_39)) || p_36)) || (*l_39)) >= g_1670) <= (**g_1632)))))), 0xBCABL)) | 3L)), (*l_39))) < 8L))))) < p_36) != 0L) == p_36) >= 1UL) <= 0x58E3DE49L) > l_1655))
        { 
            uint64_t *l_1678 = &g_154;
            uint32_t **l_1681[3];
            uint32_t ***l_1682 = &g_640;
            const int64_t ****l_1684[6][4][4] = {{{&l_1683,&l_1683,&l_1683,(void*)0},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,(void*)0,(void*)0}},{{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,(void*)0,&l_1683},{&l_1683,&l_1683,&l_1683,(void*)0},{&l_1683,&l_1683,&l_1683,&l_1683}},{{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,(void*)0},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,(void*)0}},{{&l_1683,(void*)0,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683}},{{(void*)0,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,(void*)0,&l_1683,(void*)0}},{{(void*)0,&l_1683,&l_1683,(void*)0},{&l_1683,(void*)0,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683},{&l_1683,&l_1683,&l_1683,&l_1683}}};
            uint64_t l_1695 = 18446744073709551606UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1681[i] = &g_1365;
            if ((((6UL == 0UL) == (((safe_add_func_int64_t_s_s(p_34, ((safe_mod_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s((1L != (((*l_1678)++) < (((l_1681[2] == ((*l_1682) = &g_1365)) == ((((l_1685 = l_1683) != (l_1692 = l_1689)) ^ ((((safe_mul_func_int8_t_s_s((p_33 < (*l_39)), 255UL)) | 0x56L) & g_49) < 0xFC3A41F3L)) >= 1UL)) != 65535UL))), l_1695)), p_33)) & 0x4AL))) <= p_34) | (*l_39))) | p_35.f0))
            { 
                int8_t *l_1697 = &g_126;
                int32_t ***l_1713 = &g_168;
                (*g_1714) ^= ((0L == ((((*l_1697) = (!l_1671)) && (safe_sub_func_uint32_t_u_u(((*g_1365) = (safe_sub_func_uint64_t_u_u(((*l_1678) = l_1671), (safe_sub_func_int64_t_s_s((safe_mod_func_int64_t_s_s(p_33, (safe_rshift_func_int16_t_s_u((safe_unary_minus_func_uint8_t_u((((((safe_lshift_func_int16_t_s_u((safe_mod_func_int16_t_s_s(p_33, 1L)), 12)) < (**g_1364)) < ((l_1713 != (void*)0) == (*g_169))) == 7UL) || p_36))), 15)))), 0x889F8DEB7B8E447BLL))))), (-2L)))) | (*l_39))) != 0x2C0EL);
                for (g_387.f2 = 2; (g_387.f2 >= 0); g_387.f2 -= 1)
                { 
                    int i, j;
                    if (p_35.f0)
                        break;
                    (**g_168) |= p_34;
                }
            }
            else
            { 
                return l_39;
            }
        }
        else
        { 
            int32_t *l_1715[10][1][1] = {{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}},{{&l_1671}}};
            struct S0 **l_1726[1][4] = {{&l_1725,&l_1725,&l_1725,&l_1725}};
            int i, j, k;
            ++l_1722;
            (*g_1461) = l_1725;
        }
        for (l_1718 = 2; (l_1718 >= 0); l_1718 -= 1)
        { 
            uint64_t *l_1729 = &g_154;
            int32_t l_1741 = (-8L);
            uint16_t **l_1766 = &g_152[0];
            uint16_t ***l_1765 = &l_1766;
            const int64_t l_1772[2] = {1L,1L};
            int16_t *l_1781 = &g_1462.f2;
            uint8_t * const *l_1806 = &g_297;
            int i;
            if ((((0x72L || (0x47L == ((p_33 != (((*l_1729)--) > ((**l_1665) = ((void*)0 == l_1732)))) > (safe_mul_func_uint16_t_u_u((((safe_div_func_int8_t_s_s(0x97L, (-10L))) != ((safe_mul_func_uint16_t_u_u(l_1741, (*l_39))) < 0xF7B608F60CF2612ALL)) || l_1741), 0x991DL))))) < 255UL) ^ (*l_39)))
            { 
                uint32_t l_1744[10];
                int i;
                for (i = 0; i < 10; i++)
                    l_1744[i] = 4UL;
                for (g_225 = 0; (g_225 <= 2); g_225 += 1)
                { 
                    int32_t *l_1742 = &l_1719;
                    int32_t *l_1743[8];
                    uint16_t *l_1750 = &g_140;
                    uint16_t ****l_1767[4] = {&l_1765,&l_1765,&l_1765,&l_1765};
                    uint8_t *l_1771 = &g_441;
                    int16_t l_1773 = 0x3E2BL;
                    int16_t *l_1774 = (void*)0;
                    int16_t *l_1775 = (void*)0;
                    int16_t *l_1776 = &g_1375;
                    int i, j;
                    for (i = 0; i < 8; i++)
                        l_1743[i] = &g_4;
                    l_1744[9]--;
                    (*g_169) |= (((l_1671 |= (p_33 < ((**g_1632) = (l_1747[1] != (*g_315))))) & (safe_add_func_uint16_t_u_u(p_33, ((*l_1750) |= l_1741)))) && (0xA4303A5FL & (safe_sub_func_int32_t_s_s(p_36, ((((l_1741 ^ (safe_mod_func_uint8_t_u_u((safe_unary_minus_func_uint64_t_u(0x212A9B9C762CB7F0LL)), l_1741))) ^ 18446744073709551606UL) | g_21) > 1UL)))));
                    (*g_169) = ((safe_rshift_func_int8_t_s_s((g_126 = (-1L)), 6)) != (safe_lshift_func_int8_t_s_s(((safe_add_func_uint64_t_u_u((((safe_sub_func_uint8_t_u_u((*g_297), (l_1764[0][1] < ((((*g_1365) = ((g_1768 = l_1765) != &g_1769)) ^ l_1721) && ((*l_1776) = ((**g_897) |= ((((**l_1665) = (*l_39)) > (l_1721 || (((*l_1771) = l_1770) <= l_1772[1]))) | l_1773))))))) > l_1719) & (*g_297)), 1L)) & p_36), g_504.f1)));
                }
                (*l_39) = (((*l_39) < (safe_rshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s(p_35.f0, p_34)), ((**g_1632) = (-10L))))) | (l_1781 == (**g_896)));
            }
            else
            { 
                int8_t l_1782[7][5][4] = {{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x26L,2L,0x1AL,2L},{0x69L,0x96L,0x26L,2L},{0x66L,2L,0x66L,0L},{0x66L,0L,0x26L,(-1L)},{0x69L,0L,0x1AL,0L}},{{0x66L,0x96L,0x0EL,2L},{0x26L,0L,0x66L,2L},{0x1AL,0x96L,0x1AL,(-1L)},{0x1AL,(-1L),0x66L,2L},{0x26L,(-1L),0x0EL,(-1L)}}};
                int32_t l_1784[4][2] = {{0xAF0D736DL,1L},{0xAF0D736DL,1L},{0xAF0D736DL,1L},{0xAF0D736DL,1L}};
                uint8_t **l_1804 = &g_297;
                uint8_t *l_1805 = &g_441;
                int i, j, k;
                l_1785++;
                for (g_600 = 0; (g_600 <= 6); g_600 += 1)
                { 
                    int32_t l_1790[2];
                    int8_t *l_1798 = &g_1382;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1790[i] = 1L;
                    (*l_39) = (safe_sub_func_uint32_t_u_u(g_70[(l_1718 + 1)][p_34][l_1718], g_70[(p_34 + 1)][l_1718][p_34]));
                    l_1791++;
                    l_1790[0] &= ((safe_mod_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((*l_1798) &= p_35.f0), 0x4CL)), p_36)) ^ 0x714BCC68L);
                }
                (*l_39) ^= (safe_mul_func_int8_t_s_s(g_100, ((((*g_168) = func_79((l_1801 != (void*)0), &g_169, (*g_1647), (safe_mul_func_int8_t_s_s((((((*l_1804) = &p_33) != (l_1805 = &g_441)) == g_600) > p_35.f0), 0x4EL)))) == &l_1719) < g_1642)));
            }
            l_1806 = l_1806;
        }
    }
    (**g_942) = (*g_315);
    return (*g_168);
}



static int32_t  func_40(union U1 * p_41, const uint16_t  p_42)
{ 
    uint32_t *l_47 = (void*)0;
    uint32_t *l_48 = &g_49;
    int32_t l_63 = 1L;
    union U1 l_68[1][6] = {{{0xDD215CD4L},{0xDD215CD4L},{0xDD215CD4L},{0xDD215CD4L},{0xDD215CD4L},{0xDD215CD4L}}};
    uint32_t l_1648 = 1UL;
    int32_t *l_1650 = &g_4;
    int i, j;
    (*l_1650) = (safe_add_func_uint32_t_u_u(((*l_48) |= p_42), (safe_rshift_func_uint16_t_u_s((safe_mod_func_int32_t_s_s((safe_add_func_uint8_t_u_u((safe_add_func_int8_t_s_s(((func_58(p_41, l_63, func_64(g_4, l_68[0][0], g_44.f0), (*g_1364)) & g_1519) < l_63), g_1462.f2)), p_42)), 5UL)), l_1648))));
    return (*l_1650);
}



static int16_t  func_58(union U1 * p_59, uint32_t  p_60, int32_t ** p_61, uint32_t * p_62)
{ 
    int32_t * const l_1439 = &g_44.f0;
    int32_t **l_1441[6] = {&g_169,&g_169,&g_169,&g_169,&g_169,&g_169};
    uint8_t * const *l_1447 = &g_297;
    uint8_t * const **l_1446 = &l_1447;
    uint8_t * const ***l_1445 = &l_1446;
    union U1 *l_1524[9][7][4] = {{{&g_44,(void*)0,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44}},{{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44},{&g_44,(void*)0,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,(void*)0},{&g_44,(void*)0,&g_44,(void*)0},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,(void*)0,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,(void*)0,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,&g_44}},{{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,&g_44,(void*)0},{&g_44,(void*)0,&g_44,(void*)0},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,(void*)0,&g_44},{&g_44,&g_44,&g_44,&g_44},{&g_44,&g_44,(void*)0,&g_44}}};
    int64_t l_1534[8][5][6] = {{{(-1L),0x4D7CA4919DA52285LL,0xC5982BB1978B48C5LL,(-9L),0xC5982BB1978B48C5LL,0x4D7CA4919DA52285LL},{3L,0x5F6D5084C96E8907LL,0xC1C1A23960C41B8ALL,1L,0xEC9DC4C740A73C93LL,0xB21DBB4F504AF395LL},{7L,0x00E2F117F70BB6B7LL,8L,0xC5724FBD11622AC3LL,0x9B38004C26DD6DB2LL,0xB5BFF6D6A90C7A5BLL},{6L,0x00E2F117F70BB6B7LL,7L,0xE74B629B5ECAEBEALL,0xEC9DC4C740A73C93LL,(-1L)},{0xFF82F06ABA52B1CELL,0x5F6D5084C96E8907LL,0x903E6D9854CF9278LL,0xC1C1A23960C41B8ALL,0xC5982BB1978B48C5LL,8L}},{{0x3471D0A7559A7667LL,0x4D7CA4919DA52285LL,0xA31ED60428B7FEF6LL,0x00E2F117F70BB6B7LL,(-1L),0xC5982BB1978B48C5LL},{1L,1L,1L,(-9L),7L,0xE74B629B5ECAEBEALL},{0x5F6D5084C96E8907LL,0xE74B629B5ECAEBEALL,1L,1L,0xE74B629B5ECAEBEALL,0x5F6D5084C96E8907LL},{(-1L),0xB5BFF6D6A90C7A5BLL,1L,0xFF82F06ABA52B1CELL,9L,3L},{0xEC9DC4C740A73C93LL,1L,6L,0xA3C613E227B019DFLL,0x3E2BB3CCC11B6FD5LL,0x3B84E582EB244681LL}},{{0xEC9DC4C740A73C93LL,(-1L),0xA3C613E227B019DFLL,0xFF82F06ABA52B1CELL,6L,1L},{(-1L),9L,0x407144DD3BA38EDBLL,1L,7L,0xBB1851196B51B3FDLL},{0x5F6D5084C96E8907LL,1L,0xF8577B20EFB6AA7BLL,(-9L),0x3B84E582EB244681LL,(-9L)},{1L,0xA31ED60428B7FEF6LL,(-1L),0x00E2F117F70BB6B7LL,(-9L),6L},{0x3471D0A7559A7667LL,0xC1C1A23960C41B8ALL,(-9L),0xC1C1A23960C41B8ALL,0x3471D0A7559A7667LL,0x407144DD3BA38EDBLL}},{{0xB21DBB4F504AF395LL,(-9L),0x994469E707F9EFC3LL,7L,0xBB1851196B51B3FDLL,0x3E2BB3CCC11B6FD5LL},{1L,0xC1C1A23960C41B8ALL,8L,(-9L),6L,0x3E2BB3CCC11B6FD5LL},{0x5F6D5084C96E8907LL,0xEC9DC4C740A73C93LL,0x994469E707F9EFC3LL,(-1L),0x4D7CA4919DA52285LL,0xF8577B20EFB6AA7BLL},{6L,0xFF82F06ABA52B1CELL,0xA3C613E227B019DFLL,(-1L),0xEC9DC4C740A73C93LL,0xC1C1A23960C41B8ALL},{0x9B38004C26DD6DB2LL,0xB21DBB4F504AF395LL,0xB5BFF6D6A90C7A5BLL,(-9L),1L,(-1L)}},{{(-1L),0x903E6D9854CF9278LL,0xEC9DC4C740A73C93LL,0x407144DD3BA38EDBLL,(-1L),(-1L)},{0xEC9DC4C740A73C93LL,0xF8577B20EFB6AA7BLL,0xF8577B20EFB6AA7BLL,0xEC9DC4C740A73C93LL,0x532743A3689E3BCDLL,0xC5724FBD11622AC3LL},{0xE74B629B5ECAEBEALL,0x5F6D5084C96E8907LL,0x3471D0A7559A7667LL,8L,0xB21DBB4F504AF395LL,(-1L)},{0xF8577B20EFB6AA7BLL,1L,1L,1L,0xB21DBB4F504AF395LL,6L},{0xC5724FBD11622AC3LL,0x5F6D5084C96E8907LL,0x3E2BB3CCC11B6FD5LL,0x9B38004C26DD6DB2LL,0x532743A3689E3BCDLL,(-9L)}},{{0x994469E707F9EFC3LL,0xF8577B20EFB6AA7BLL,0xE74B629B5ECAEBEALL,0xFF82F06ABA52B1CELL,(-1L),7L},{0x3B84E582EB244681LL,0x903E6D9854CF9278LL,(-1L),6L,1L,9L},{0x4D7CA4919DA52285LL,0xB21DBB4F504AF395LL,0x532743A3689E3BCDLL,1L,0xEC9DC4C740A73C93LL,1L},{1L,0xFF82F06ABA52B1CELL,1L,1L,0x4D7CA4919DA52285LL,0x9B38004C26DD6DB2LL},{(-1L),0xEC9DC4C740A73C93LL,0xFF82F06ABA52B1CELL,0x4D7CA4919DA52285LL,6L,8L}},{{6L,0xC1C1A23960C41B8ALL,1L,0x4D7CA4919DA52285LL,0xBB1851196B51B3FDLL,1L},{(-1L),(-9L),0x00E2F117F70BB6B7LL,1L,1L,6L},{1L,0x00E2F117F70BB6B7LL,9L,1L,0xA3C613E227B019DFLL,1L},{0x4D7CA4919DA52285LL,0x532743A3689E3BCDLL,(-1L),6L,(-1L),0x994469E707F9EFC3LL},{0x3B84E582EB244681LL,0xC5982BB1978B48C5LL,0xB21DBB4F504AF395LL,0xFF82F06ABA52B1CELL,0xFF82F06ABA52B1CELL,0xB21DBB4F504AF395LL}},{{0x994469E707F9EFC3LL,0x994469E707F9EFC3LL,0x3B84E582EB244681LL,0x9B38004C26DD6DB2LL,0xC1C1A23960C41B8ALL,0x407144DD3BA38EDBLL},{0xC5724FBD11622AC3LL,0x4D7CA4919DA52285LL,7L,1L,7L,0x3B84E582EB244681LL},{0xF8577B20EFB6AA7BLL,0xC5724FBD11622AC3LL,7L,8L,0x994469E707F9EFC3LL,0x407144DD3BA38EDBLL},{0xE74B629B5ECAEBEALL,8L,0x3B84E582EB244681LL,0xEC9DC4C740A73C93LL,7L,0xB21DBB4F504AF395LL},{0xEC9DC4C740A73C93LL,7L,0xB21DBB4F504AF395LL,0x407144DD3BA38EDBLL,0x5F6D5084C96E8907LL,0x994469E707F9EFC3LL}}};
    struct S0 *l_1555 = &g_258;
    const uint16_t **l_1582 = (void*)0;
    const uint16_t ***l_1581 = &l_1582;
    uint32_t ****l_1585 = (void*)0;
    uint16_t l_1612 = 0x3167L;
    int16_t **l_1630 = (void*)0;
    int i, j, k;
    (*g_1442) = l_1439;
    if (((safe_rshift_func_uint16_t_u_s(p_60, 15)) || ((**p_61) = (((void*)0 != l_1445) && p_60))))
    { 
        uint64_t * const ***l_1448 = (void*)0;
        (**p_61) &= (((l_1448 != (void*)0) >= p_60) <= g_796.f1);
    }
    else
    { 
        int32_t **l_1454 = &g_169;
        int32_t l_1464 = (-1L);
        int32_t l_1465 = 1L;
        int32_t l_1466 = 0xFA01DCEAL;
        int32_t l_1467[10] = {0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L,0x3C0BD0A7L};
        int64_t l_1496 = (-7L);
        union U1 *l_1563 = &g_44;
        uint32_t ***l_1587 = &g_640;
        uint32_t **** const l_1586 = &l_1587;
        int16_t *l_1588 = &g_1217.f2;
        uint64_t *l_1613 = &g_1046;
        int8_t *l_1614 = &g_551;
        int64_t *l_1615 = &l_1496;
        int i;
        (**p_61) = ((safe_add_func_int32_t_s_s((p_60 || p_60), (*p_62))) == g_1217.f0);
        if ((**p_61))
        { 
            union U1 *l_1455 = &g_44;
            struct S0 *l_1460 = &g_504;
            int32_t l_1469 = 9L;
            int32_t l_1470 = 1L;
            int32_t l_1471 = 5L;
            int16_t *l_1490 = &g_600;
            (*g_169) = 1L;
            (*l_1454) = func_79((g_1451 != &g_1452[1][2][3]), l_1454, l_1455, ((**p_61) & (((4UL == ((**l_1454) && (((*l_1439) < (**l_1454)) || (*l_1439)))) ^ (**p_61)) <= (**l_1454))));
            if ((l_1439 == &p_60))
            { 
                for (g_126 = 0; (g_126 <= (-5)); g_126--)
                { 
                    (*g_1461) = l_1460;
                    (*l_1460) = g_1462;
                    (*l_1460) = (*g_1438);
                }
            }
            else
            { 
                int32_t l_1463 = 0xF76A2EEBL;
                int32_t l_1468 = 1L;
                int32_t l_1472 = 0x81FF8CF8L;
                uint32_t l_1473 = 0xE9F07E46L;
                l_1473++;
            }
            (*l_1454) = func_79((3L | (safe_add_func_int8_t_s_s((**l_1454), ((safe_add_func_int32_t_s_s((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_s((0UL | ((safe_lshift_func_int16_t_s_u((**l_1454), ((**l_1454) && (safe_sub_func_uint64_t_u_u(0UL, p_60))))) || (safe_lshift_func_int16_t_s_s(((*l_1490) = ((void*)0 != &g_427)), p_60)))), 9)) != (**l_1454)), l_1470)), 0x0474E31BL)) >= (**l_1454))))), p_61, p_59, p_60);
        }
        else
        { 
            uint8_t l_1522 = 0UL;
            int32_t l_1539 = 9L;
            int32_t l_1540 = 1L;
            int32_t l_1542 = 1L;
            int32_t l_1546 = 0xB7536707L;
            int32_t l_1547 = (-1L);
            int32_t l_1548 = 0x35544B08L;
            uint32_t l_1552 = 6UL;
            for (g_258.f0 = 0; (g_258.f0 != 49); g_258.f0 = safe_add_func_int16_t_s_s(g_258.f0, 1))
            { 
                int8_t l_1520[6][1][6] = {{{0x13L,0x14L,(-8L),0x13L,0x8BL,0L}},{{0x13L,0x8BL,0L,0x14L,0x14L,0L}},{{0xB7L,0xB7L,(-8L),0xB8L,0x14L,0L}},{{0x14L,0x8BL,0xCAL,0xB8L,0x8BL,(-8L)}},{{0xB7L,0x14L,0xCAL,0x14L,0xB7L,0L}},{{0xFFL,(-3L),0x13L,0xFFL,(-1L),0xB8L}}};
                int32_t **l_1523 = &g_169;
                int32_t l_1535 = 0L;
                int32_t l_1537 = 0x5AC4ED7CL;
                int32_t l_1538 = 0xC073BB51L;
                int32_t l_1543 = 0L;
                int32_t l_1545 = 0x79F4BA17L;
                int32_t l_1549[8][4][5] = {{{0x9EDF855FL,0x1DA4A2D3L,0x02F5C342L,1L,5L},{0L,1L,0xAB1898F0L,0L,0x0A27EC7CL},{0x27B4A777L,0xC5BA35C0L,0x9FE7B54EL,0x747327B4L,0xA40D7E8CL},{0x27B4A777L,0x4E86670DL,0x3FEE2443L,5L,0x747327B4L}},{{0L,0L,0L,0xEE59E0A7L,1L},{0x9EDF855FL,0L,4L,0x4E86670DL,0xC5BA35C0L},{5L,0x9EDF855FL,0x1E4C2E6BL,0x27B4A777L,0x27B4A777L},{0xC5BA35C0L,1L,0xC5BA35C0L,0xA40D7E8CL,0x0A27EC7CL}},{{0xF1E5D940L,0xAB1898F0L,0x80C1FDD5L,0x4E86670DL,1L},{1L,0x0A27EC7CL,0x3FC962B7L,0x9EDF855FL,0xEBD38A52L},{0L,0L,0x80C1FDD5L,1L,0L},{8L,0x325398CFL,0xC5BA35C0L,0x747327B4L,0x325398CFL}},{{0xEBD38A52L,0xEA221452L,0x1E4C2E6BL,0xF1E5D940L,0xAFEA2B92L},{0L,0L,4L,0x9FE7B54EL,(-10L)},{1L,0x2E7F3D40L,0L,0xA2714C55L,(-3L)},{0xAA5E7A49L,(-1L),0x3FEE2443L,8L,0x4E86670DL}},{{0xD6E7AC91L,1L,0x9FE7B54EL,0xD6E7AC91L,0x4E86670DL},{8L,0xEE59E0A7L,0xAB1898F0L,0xAA5E7A49L,(-3L)},{0xA2714C55L,0xAA5E7A49L,0x02F5C342L,1L,(-10L)},{0x9FE7B54EL,0L,(-1L),0L,0xAFEA2B92L}},{{0xF1E5D940L,0L,0x325398CFL,0xEBD38A52L,0x325398CFL},{0x747327B4L,0x747327B4L,0x12843A57L,8L,0L},{1L,1L,(-1L),0L,0xEBD38A52L},{0x9EDF855FL,0x64AC068EL,0x2E7F3D40L,1L,1L}},{{0x4E86670DL,1L,0x71437212L,0xF1E5D940L,0x0A27EC7CL},{0xA40D7E8CL,0x747327B4L,0x9FE7B54EL,0xC5BA35C0L,0x27B4A777L},{0x27B4A777L,0L,0xEE59E0A7L,5L,0xC5BA35C0L},{0x4E86670DL,0L,0xC6152B0FL,0x9EDF855FL,1L}},{{0xEE59E0A7L,0xAA5E7A49L,4L,0L,0x747327B4L},{5L,0xEE59E0A7L,0L,0x27B4A777L,0xA40D7E8CL},{0x747327B4L,1L,(-1L),0x27B4A777L,0x0A27EC7CL},{0L,(-1L),0x80C1FDD5L,0L,5L}}};
                int i, j, k;
                if ((**p_61))
                    break;
                for (g_1462.f0 = 0; (g_1462.f0 >= 20); g_1462.f0 = safe_add_func_uint16_t_u_u(g_1462.f0, 4))
                { 
                    int32_t l_1495 = 0L;
                    const int16_t **l_1504 = (void*)0;
                    const int16_t ***l_1503 = &l_1504;
                    int8_t *l_1510 = &g_74;
                    int32_t l_1515[6][10][4] = {{{0x783A69F7L,0xBBC795EAL,1L,3L},{(-3L),0x52F1E58DL,(-5L),0x3969B4EAL},{0x92FB464DL,0xF1C6B5A4L,0xBE4AE169L,0x86C0C156L},{0x86C0C156L,0L,0x3969B4EAL,5L},{(-7L),0x3EA44354L,0x66E89C8BL,0x2038F078L},{0x92FB464DL,5L,0L,0x4717047BL},{0x876539B5L,0x059721B4L,0x8A1CBEBCL,0x65AAF658L},{0x90AB37F0L,(-1L),0L,(-1L)},{(-4L),0xBBC795EAL,0x3EA44354L,0L},{0L,0x0B7749A8L,0x92F852CEL,(-4L)}},{{0xDA3340A2L,1L,0x1453895EL,(-9L)},{0xBBC795EAL,0xA3C01708L,(-1L),(-7L)},{0x892E225DL,(-1L),0x55D2A1A6L,9L},{1L,0x986BC335L,0x917D8C0CL,0x90AB37F0L},{0x986BC335L,0xA3C01708L,(-7L),0x52AC934AL},{0xCEBF67F2L,0xDA3340A2L,0x1B0E707DL,(-4L)},{8L,9L,0xDA3340A2L,0xF61A51CDL},{0x66E89C8BL,0xBBC795EAL,0x52F1E58DL,0x986BC335L},{0xC4A6D690L,1L,8L,0x65AAF658L},{0x89A9C69FL,1L,0x89A9C69FL,0x66D8F0F8L}},{{0x2038F078L,(-3L),0x92FB464DL,0xA3C01708L},{0x65AAF658L,0xBE4AE169L,0x90AB37F0L,(-3L)},{0xD7A35DE5L,0x52AC934AL,0x90AB37F0L,0L},{0x65AAF658L,0x892E225DL,0x92FB464DL,0xCEBF67F2L},{0x2038F078L,0x92F852CEL,0x89A9C69FL,0L},{0x89A9C69FL,0L,8L,0xFDC3EA06L},{0xC4A6D690L,0x3969B4EAL,0x52F1E58DL,0x87AC5AB2L},{0x66E89C8BL,8L,0xDA3340A2L,0xC9495908L},{8L,0L,0x1B0E707DL,8L},{0xCEBF67F2L,(-7L),(-7L),1L}},{{0x986BC335L,0xD7A35DE5L,0x917D8C0CL,0x9F5D5633L},{1L,3L,0x55D2A1A6L,0x4281A3CDL},{0x892E225DL,0xD9168AF2L,(-1L),1L},{0xBBC795EAL,0x90AB37F0L,0x1453895EL,3L},{0xDA3340A2L,0L,0x92F852CEL,0x66028E63L},{0L,0x8DF5133CL,0x3EA44354L,0x87AC5AB2L},{(-4L),0x1453895EL,0L,0L},{0x90AB37F0L,0L,0x8A1CBEBCL,0L},{0x6742086BL,(-4L),0x6375AFFAL,0xCEBF67F2L},{0x9F5D5633L,1L,0x3923640DL,(-5L)}},{{0L,0x52AC934AL,0x3C28ACD6L,0x89A9C69FL},{(-5L),0x66E89C8BL,(-3L),0xA3C01708L},{0x9F5D5633L,0x89A9C69FL,0x89F0D25FL,0x28D12ED0L},{0x8ED1618BL,1L,0x8A1CBEBCL,0x917D8C0CL},{(-7L),(-1L),0L,0x986BC335L},{(-4L),(-1L),0xD7A35DE5L,0L},{0x8A1CBEBCL,9L,0x92F852CEL,0x92F852CEL},{1L,1L,0x9C1A935DL,0x52AC934AL},{0xBBC795EAL,2L,0x0B7749A8L,(-7L)},{1L,0x986BC335L,0x55D2A1A6L,0x0B7749A8L}},{{0xD75A5604L,0x986BC335L,0xF1C6B5A4L,(-7L)},{0x986BC335L,2L,1L,0x52AC934AL},{0x3C28ACD6L,1L,0x1B0E707DL,0x92F852CEL},{3L,9L,(-2L),0L},{0x3923640DL,(-8L),0x72712823L,8L},{0L,0x1B0E707DL,0x86C0C156L,0x4717047BL},{0x8ED1618BL,1L,0x6742086BL,0x0B7749A8L},{2L,0x3998A2FCL,0x485EF3F4L,(-1L)},{0x4717047BL,0x3923640DL,0L,0x3998A2FCL},{0xBE4AE169L,(-1L),(-5L),0x89F0D25FL}}};
                    int16_t *l_1516 = &g_795[0].f2;
                    uint32_t *l_1517 = (void*)0;
                    uint32_t *l_1518 = &g_504.f0;
                    uint64_t *l_1521 = &g_1046;
                    int32_t l_1541[6][9][4] = {{{(-6L),0x3FE079B9L,1L,1L},{1L,1L,0x3FE079B9L,(-6L)},{0x0D88A575L,0xBAD6F230L,0L,0x9632BE1AL},{0xDC1634C6L,0L,1L,0L},{0x5E123CDBL,0L,0x26EF7307L,0x9632BE1AL},{0L,0xBAD6F230L,0x5E123CDBL,(-6L)},{1L,1L,9L,1L},{(-9L),0x3FE079B9L,0x9632BE1AL,0x0D88A575L},{0x5439454AL,0L,2L,0xDC1634C6L}},{{9L,1L,0xACD32F3AL,0x5E123CDBL},{9L,0x26EF7307L,2L,0L},{0x5439454AL,0x5E123CDBL,0x9632BE1AL,1L},{(-9L),9L,9L,(-9L)},{1L,0x9632BE1AL,0x5E123CDBL,0x5439454AL},{0L,2L,0x26EF7307L,9L},{0x5E123CDBL,0xACD32F3AL,1L,9L},{0xDC1634C6L,2L,0L,0x5439454AL},{0x0D88A575L,0x9632BE1AL,0x3FE079B9L,(-9L)}},{{1L,9L,1L,1L},{(-6L),0x5E123CDBL,0xBAD6F230L,0L},{0x9632BE1AL,0x26EF7307L,0L,0x5E123CDBL},{0L,1L,0L,0xDC1634C6L},{0x9632BE1AL,0L,0xBAD6F230L,0x0D88A575L},{(-6L),0x3FE079B9L,1L,1L},{1L,1L,0x3FE079B9L,(-6L)},{0x0D88A575L,0xBAD6F230L,0L,0x9632BE1AL},{0xDC1634C6L,0L,1L,0L}},{{0x5E123CDBL,0L,0x26EF7307L,0x9632BE1AL},{0L,0xBAD6F230L,0x5E123CDBL,(-6L)},{1L,1L,9L,1L},{(-9L),0x3FE079B9L,0x9632BE1AL,0x0D88A575L},{0x5439454AL,0L,2L,0xDC1634C6L},{9L,1L,0xACD32F3AL,0x5E123CDBL},{9L,0x26EF7307L,2L,0L},{0x5439454AL,0x5E123CDBL,0x9632BE1AL,1L},{(-9L),9L,9L,(-9L)}},{{1L,0x9632BE1AL,0x5E123CDBL,0x5439454AL},{0L,2L,0x26EF7307L,9L},{0x5E123CDBL,0xACD32F3AL,1L,9L},{0xDC1634C6L,2L,0L,0x5439454AL},{0x0D88A575L,0x9632BE1AL,0x3FE079B9L,(-9L)},{1L,9L,0x9EE82AEFL,0L},{1L,1L,1L,0x5439454AL},{9L,(-6L),0x5439454AL,1L},{0xBAD6F230L,0x3FE079B9L,0x5439454AL,(-4L)}},{{9L,0xBAD6F230L,1L,2L},{1L,0xACD32F3AL,0x9EE82AEFL,0x9EE82AEFL},{0x9EE82AEFL,0x9EE82AEFL,0xACD32F3AL,1L},{2L,1L,0xBAD6F230L,9L},{(-4L),0x5439454AL,0x3FE079B9L,0xBAD6F230L},{1L,0x5439454AL,(-6L),9L},{0x5439454AL,1L,1L,1L},{0L,0x9EE82AEFL,1L,0x9EE82AEFL},{1L,0xACD32F3AL,9L,2L}}};
                    int32_t **l_1562 = &g_169;
                    int i, j, k;
                    (*l_1439) ^= 0xEC1D4E18L;
                    (*l_1439) &= l_1495;
                    if (((l_1496 > ((*l_1521) = (safe_sub_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(((((*l_1518) ^= (safe_div_func_int16_t_s_s((((*l_1439) = ((void*)0 != l_1503)) >= (**g_1364)), ((*l_1516) |= ((safe_div_func_uint64_t_u_u((+((safe_lshift_func_int8_t_s_u(((*l_1510) = (0L > p_60)), ((*g_297) <= (safe_rshift_func_uint16_t_u_u(((safe_div_func_uint8_t_u_u(((p_60 || p_60) <= g_70[2][2][1]), 1UL)) ^ 0x4191L), 3))))) < l_1515[5][2][1])), 18446744073709551615UL)) || p_60))))) | g_1519) && 7L), l_1520[4][0][4])) >= 1L), (*p_62))))) > l_1522))
                    { 
                        int64_t *l_1529 = &l_1496;
                        int32_t l_1536 = 0x1DA5148EL;
                        int32_t l_1544 = 0x13E51258L;
                        int32_t l_1550 = 1L;
                        int32_t l_1551 = 1L;
                        (*l_1454) = func_79(p_60, l_1523, l_1524[5][3][2], (safe_unary_minus_func_int64_t_s(((safe_unary_minus_func_uint32_t_u((((p_60 != p_60) >= ((++(*l_1521)) && ((*l_1529) &= (-1L)))) != (safe_add_func_int64_t_s_s((((*p_62) <= (5L && ((l_1534[6][1][0] &= (safe_add_func_uint64_t_u_u(((*l_1439) < p_60), p_60))) < (*p_62)))) > 0UL), p_60))))) >= (*p_62)))));
                        --l_1552;
                        (*g_1437) = l_1555;
                        (*l_1439) &= (+(**p_61));
                    }
                    else
                    { 
                        const int64_t * const l_1558[3][7] = {{&l_1534[6][2][4],(void*)0,&l_1534[6][2][4],(void*)0,&l_1534[6][2][4],(void*)0,&l_1534[6][2][4]},{&l_1534[0][4][1],&l_1534[0][4][1],&l_1534[6][1][0],&l_1534[6][1][0],&l_1534[0][4][1],&l_1534[0][4][1],&l_1534[6][1][0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                        const int64_t * const *l_1557 = &l_1558[0][0];
                        const int64_t * const **l_1561 = &l_1557;
                        int32_t l_1574 = 0x96264CB2L;
                        int i, j;
                        (*l_1561) = l_1557;
                        (*l_1454) = func_79((0x87L & p_60), l_1562, l_1563, (safe_add_func_uint64_t_u_u(((*l_1521) = (safe_mul_func_uint16_t_u_u(65535UL, (((((**p_61) <= (safe_rshift_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u(((p_60 >= 0x93F7L) != (safe_div_func_uint16_t_u_u(((0xD7L ^ (*g_297)) <= (-1L)), (**l_1454)))), 1L)) != 18446744073709551615UL), p_60))) <= (**l_1523)) > p_60) || 0x11L)))), l_1574)));
                        if ((**p_61))
                            break;
                    }
                }
            }
        }
        (**p_61) = (safe_sub_func_int32_t_s_s((safe_add_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_s(p_60, 0)) >= ((*l_1615) &= (((l_1581 == (void*)0) || 0UL) ^ (safe_sub_func_int8_t_s_s(((**l_1454) || ((*l_1588) &= (l_1585 == l_1586))), ((*l_1614) = ((safe_unary_minus_func_int16_t_s((((safe_mul_func_int16_t_s_s(((safe_mod_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((****l_1445) = (safe_mod_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((safe_add_func_int16_t_s_s((safe_mod_func_uint64_t_u_u(((*l_1613) |= (safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((**p_61) >= ((**l_1454) == g_795[0].f2)), (*g_1365))), 2)) >= p_60), 1L)), l_1612))), 18446744073709551615UL)), (**l_1454))), (*g_297))), 0x4998L))), 0xC3L)), p_60)) < p_60), 0xEE36L)) && 0x90B7L) == 0x27L))) != (**l_1454)))))))), p_60)), (*p_62)));
        for (g_1462.f0 = 1; (g_1462.f0 <= 5); g_1462.f0 += 1)
        { 
            uint32_t l_1620[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1620[i] = 0xE6627B8FL;
            (*p_61) = (*p_61);
            for (l_1496 = 4; (l_1496 >= 0); l_1496 -= 1)
            { 
                int32_t l_1645 = 4L;
                int32_t **l_1646 = &g_169;
                if ((p_60 <= ((!0xEDC4A839L) >= (p_60 && 0x6585L))))
                { 
                    uint16_t **l_1617 = &g_152[0];
                    int32_t l_1619 = 4L;
                    (*p_61) = (*l_1454);
                    (*l_1454) = (*p_61);
                    l_1619 &= (((*l_1617) = &l_1612) == g_1618);
                    l_1620[0]--;
                }
                else
                { 
                    uint32_t l_1625 = 0xE2674E64L;
                    int16_t ***l_1631[2];
                    int64_t *l_1641[3][1][9] = {{{(void*)0,(void*)0,&l_1496,&l_1496,&l_1534[1][0][5],&l_1496,&l_1496,(void*)0,(void*)0}},{{&l_1534[1][1][5],&l_1534[6][1][0],(void*)0,&l_1496,(void*)0,&l_1534[6][1][0],&l_1534[1][1][5],&l_1534[1][1][5],&l_1534[6][1][0]}},{{&l_1496,&l_1534[6][1][0],&l_1496,&l_1534[6][1][0],&l_1496,&l_1534[5][4][2],&l_1534[5][4][2],&l_1496,&l_1534[6][1][0]}}};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1631[i] = (void*)0;
                    for (g_1375 = 5; (g_1375 >= 2); g_1375 -= 1)
                    { 
                        return l_1620[0];
                    }
                    (*l_1439) = (g_158.f2 & 0xC5L);
                    if ((safe_add_func_uint64_t_u_u(((((*g_297) == l_1625) || (safe_lshift_func_uint16_t_u_u(p_60, ((safe_add_func_uint64_t_u_u((l_1630 == (g_1632 = ((*g_896) = (*g_896)))), (g_1642 = ((safe_add_func_uint16_t_u_u(65535UL, (safe_mul_func_int16_t_s_s((((((0x48L != (safe_rshift_func_uint16_t_u_s(6UL, 5))) || (p_60 > 65531UL)) < l_1625) < 0UL) == 4UL), l_1625)))) >= p_60)))) & (-4L))))) ^ p_60), (**l_1454))))
                    { 
                        if ((**p_61))
                            break;
                        (*l_1454) = func_79(p_60, l_1454, p_59, (((((*l_1614) |= p_60) && (safe_add_func_uint64_t_u_u(((*l_1613) = (*l_1439)), p_60))) != ((l_1645 >= p_60) > ((((*l_1581) == (void*)0) > (-1L)) == (*g_1365)))) >= l_1625));
                    }
                    else
                    { 
                        (*l_1646) = func_79((**l_1454), l_1646, &g_44, l_1625);
                    }
                }
            }
        }
    }
    (*g_1647) = l_1524[5][3][2];
    (**p_61) = p_60;
    return p_60;
}



static int32_t ** func_64(int64_t  p_65, union U1  p_66, uint32_t  p_67)
{ 
    int32_t *l_91 = &g_4;
    int32_t **l_90 = &l_91;
    const uint64_t *l_483 = &g_484;
    const struct S0 *l_505 = &g_506;
    uint8_t *l_699 = (void*)0;
    uint8_t l_720[9];
    int64_t l_726 = 0xFB2FA3450C180EE8LL;
    union U1 *l_736 = &g_44;
    int32_t l_782 = 0xE532B8F9L;
    const int64_t l_849 = 0x911483B5B8816030LL;
    uint8_t ***l_872 = (void*)0;
    int32_t l_889 = 0xC4FF85B1L;
    uint32_t l_930 = 0UL;
    const int32_t ***l_940 = (void*)0;
    const int32_t ****l_939 = &l_940;
    int32_t l_961 = 4L;
    int32_t l_962 = 7L;
    int32_t l_965[7] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
    uint16_t l_966 = 0x965AL;
    int32_t l_1016 = 0xACE60608L;
    uint64_t *l_1025 = &g_154;
    uint64_t ***l_1031 = &g_427;
    int16_t * const *l_1040 = (void*)0;
    uint8_t l_1043 = 255UL;
    uint64_t *l_1044 = (void*)0;
    uint64_t *l_1045 = &g_1046;
    uint32_t l_1047 = 4294967295UL;
    uint16_t *l_1048[1];
    union U1 ***l_1058 = (void*)0;
    int32_t l_1082[5][7] = {{1L,0xE05F55B2L,1L,0xAC27C143L,(-1L),(-1L),0xAC27C143L},{1L,0xE05F55B2L,1L,0xAC27C143L,(-1L),(-1L),0xAC27C143L},{1L,0xE05F55B2L,1L,0xAC27C143L,(-1L),(-1L),0xAC27C143L},{1L,0xE05F55B2L,1L,0xAC27C143L,(-1L),(-1L),0xAC27C143L},{1L,0xE05F55B2L,1L,0xAC27C143L,(-1L),(-1L),0xAC27C143L}};
    int32_t l_1138 = 1L;
    int64_t *l_1307 = &l_726;
    int64_t **l_1306 = &l_1307;
    uint8_t l_1317[7][2][4] = {{{0xBFL,0xA8L,0x99L,0x8FL},{0xDCL,0x11L,0xDCL,0x99L}},{{0x90L,0xE3L,1UL,0x90L},{0xBFL,0x99L,0UL,0xE3L}},{{0x99L,0x11L,0UL,0UL},{0xBFL,0xBFL,1UL,0x8FL}},{{0x90L,0x9FL,0xDCL,0xE3L},{0xDCL,0xE3L,0x99L,0xDCL}},{{0xBFL,0xE3L,255UL,0xE3L},{0xE3L,0x9FL,0UL,0x8FL}},{{0xA8L,0xBFL,0x99L,0UL},{0x90L,0x11L,0xE9L,0xE3L}},{{0x90L,0x99L,0x99L,0UL},{1UL,0xDCL,255UL,0xE9L}}};
    uint32_t ** const *l_1361 = &g_640;
    uint8_t *l_1368 = (void*)0;
    int32_t l_1431 = 0x718FC5D5L;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_720[i] = 0x10L;
    for (i = 0; i < 1; i++)
        l_1048[i] = &g_669;
    for (g_4 = 2; (g_4 >= 0); g_4 -= 1)
    { 
        uint32_t *l_69 = &g_70[2][2][1];
        int8_t *l_73 = &g_74;
        int32_t *l_87 = &g_44.f0;
        int32_t **l_86 = &l_87;
        union U1 *l_92 = &g_44;
        uint64_t *l_470 = &g_154;
        int32_t l_507 = 1L;
        if ((0x4A0F0C0ED6D23037LL && (((*l_470) = (((*l_69) &= 0x8CA18188L) == ((safe_mul_func_int8_t_s_s((((*l_73) = p_65) || func_75(l_69, func_79((safe_lshift_func_int8_t_s_s((((*l_86) = &g_4) == &g_4), (safe_rshift_func_int8_t_s_s(((*l_73) ^= p_65), g_44.f0)))), l_90, l_92, g_4), g_258.f0)), 7UL)) == p_66.f0))) == 18446744073709551615UL)))
        { 
            const uint64_t *l_481 = &g_154;
            int32_t l_486 = 0xCFAA5EABL;
            for (g_153 = 0; (g_153 <= 2); g_153 += 1)
            { 
                const uint64_t **l_482[6][4][1];
                uint64_t **l_485 = &g_428;
                uint16_t *l_487 = &g_140;
                struct S0 *l_503 = &g_504;
                struct S0 **l_502 = &l_503;
                int32_t l_508 = 0x3036B0DBL;
                int i, j, k;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_482[i][j][k] = (void*)0;
                    }
                }
                l_507 = (safe_lshift_func_int8_t_s_s((safe_add_func_int32_t_s_s(((safe_sub_func_uint64_t_u_u(p_67, (safe_mod_func_int32_t_s_s((g_158.f1 && (((l_483 = l_481) != ((*l_485) = l_470)) > (l_486 < (--(*l_487))))), (safe_lshift_func_int8_t_s_s(((((*l_470) = ((**l_90) || (safe_rshift_func_uint16_t_u_u((+0x3C4DBFB7L), (safe_div_func_uint64_t_u_u((p_66.f0 != ((safe_rshift_func_int8_t_s_s((((safe_lshift_func_uint8_t_u_s((!((((*l_502) = (void*)0) == l_505) | (**g_168))), 0)) | p_66.f0) >= 0x2F6EL), 6)) ^ 0x8121A04AL)), 0xEA743AF3F68DA2B8LL)))))) > p_66.f0) >= p_65), 3)))))) >= l_507), l_508)), 7));
            }
            if (p_67)
                break;
        }
        else
        { 
            for (g_504.f2 = 0; (g_504.f2 <= 2); g_504.f2 += 1)
            { 
                struct S0 *l_509 = &g_258;
                struct S0 **l_510 = (void*)0;
                struct S0 **l_511 = &l_509;
                int i, j;
                (*l_511) = l_509;
                g_3[g_504.f2][(g_504.f2 + 3)] = g_3[g_4][(g_4 + 2)];
            }
            if ((**g_105))
                continue;
        }
        return &g_169;
    }
    if (((*g_169) = (safe_rshift_func_uint8_t_u_s(1UL, 2))))
    { 
        int32_t l_531 = (-1L);
        int32_t l_598 = 0xB670520BL;
        struct S0 *l_655 = &g_387;
        struct S0 ** const l_654[1][7][10] = {{{(void*)0,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655},{&l_655,&l_655,&l_655,(void*)0,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655},{(void*)0,&l_655,&l_655,(void*)0,&l_655,&l_655,&l_655,(void*)0,&l_655,&l_655},{&l_655,&l_655,(void*)0,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655},{&l_655,&l_655,&l_655,(void*)0,(void*)0,&l_655,&l_655,&l_655,&l_655,&l_655},{&l_655,(void*)0,&l_655,&l_655,&l_655,&l_655,(void*)0,&l_655,&l_655,&l_655},{&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,&l_655,(void*)0,&l_655}}};
        uint8_t **l_664 = &g_297;
        int32_t *l_723 = &g_4;
        uint8_t **l_735 = &l_699;
        union U1 *l_751 = (void*)0;
        uint32_t l_814 = 0xC256088BL;
        int32_t *l_848[6][8] = {{&l_598,&l_598,&l_598,&g_44.f0,&g_44.f0,&g_44.f0,&g_44.f0,&l_598},{&l_598,&l_598,&g_44.f0,&l_531,&l_782,&l_531,&g_44.f0,&l_598},{&l_598,&l_598,&g_44.f0,&g_44.f0,&g_44.f0,&g_44.f0,&l_598,&l_598},{&l_598,(void*)0,&l_598,&l_531,&l_598,(void*)0,&l_598,&l_598},{(void*)0,&l_531,&g_44.f0,&g_44.f0,&l_531,(void*)0,&g_44.f0,(void*)0},{&l_531,(void*)0,&g_44.f0,(void*)0,&l_531,&g_44.f0,&g_44.f0,&l_531}};
        union U1 *l_988 = &g_44;
        int i, j, k;
        for (g_387.f0 = 13; (g_387.f0 < 28); g_387.f0 = safe_add_func_uint16_t_u_u(g_387.f0, 8))
        { 
            uint64_t l_533 = 0x1309D1BD77567B50LL;
            int32_t l_574 = 1L;
            int32_t l_602 = 0x368A586DL;
            uint16_t l_625 = 0x95BDL;
            int32_t l_626[3][5][9] = {{{0x9E881589L,(-2L),4L,6L,3L,0x9E881589L,(-6L),3L,(-1L)},{0x4DB425F3L,1L,1L,0x78FB5487L,0xE043C6F0L,0x9E881589L,1L,0x948BDC22L,(-2L)},{(-1L),6L,7L,0xA15CE5BAL,4L,4L,0xA15CE5BAL,7L,6L},{6L,0xAA1111C2L,(-1L),0x36F28BD8L,0x8BFE4948L,(-1L),(-2L),(-1L),0x36F28BD8L},{0x6A58FD17L,0L,6L,0xE0703CA0L,0xA15CE5BAL,(-1L),0xCECCD96AL,1L,(-1L)}},{{0x5BEE56A3L,0xAA1111C2L,0x78FB5487L,0x948BDC22L,(-1L),(-2L),0x8BFE4948L,0x26A47E8BL,(-1L)},{0x4DB425F3L,6L,(-1L),0x26A47E8BL,0x8A566CC0L,4L,0x948BDC22L,0xA15CE5BAL,0x4C481E23L},{0xCECCD96AL,1L,7L,3L,1L,6L,1L,0x8A566CC0L,0x78FB5487L},{3L,1L,0xE043C6F0L,(-6L),(-2L),0x9E881589L,(-2L),(-6L),0xE043C6F0L},{(-1L),(-1L),3L,(-2L),0x5BEE56A3L,0x948BDC22L,6L,0x4DB425F3L,0x8A566CC0L}},{{(-1L),(-2L),0xE0703CA0L,3L,(-2L),(-1L),0L,(-1L),0x4DB425F3L},{(-1L),4L,3L,(-1L),1L,(-1L),0x4C481E23L,0x26A47E8BL,8L},{(-10L),0x5BEE56A3L,0xE043C6F0L,(-2L),3L,6L,0x5BEE56A3L,0L,4L},{0x9E881589L,0x6E3ECBADL,0L,(-6L),(-1L),0x5BEE56A3L,4L,0L,0x8BFE4948L},{3L,(-10L),0x9E881589L,0L,0x26A47E8BL,0x948BDC22L,0x948BDC22L,0x26A47E8BL,0L}}};
            uint32_t l_696 = 0UL;
            int32_t *l_737 = (void*)0;
            int32_t *l_738[9];
            union U1 *l_748[2][3] = {{(void*)0,&g_44,&g_44},{(void*)0,&g_44,&g_44}};
            int16_t *l_756 = &g_504.f2;
            uint8_t l_844[9] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
            int i, j, k;
            for (i = 0; i < 9; i++)
                l_738[i] = &l_602;
            for (g_258.f2 = 0; (g_258.f2 <= 3); g_258.f2 = safe_add_func_uint64_t_u_u(g_258.f2, 6))
            { 
                uint16_t l_520 = 0UL;
                uint32_t *l_532 = (void*)0;
                int32_t l_553 = 0L;
                int32_t **l_556[8][3][2];
                uint8_t **l_563 = &g_297;
                union U1 **l_643 = &g_180[0];
                union U1 *l_644[2][5][4] = {{{(void*)0,&g_44,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_44,(void*)0,&g_44,(void*)0},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,(void*)0,&g_44}},{{(void*)0,&g_44,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_44,(void*)0,&g_44,(void*)0},{(void*)0,&g_44,&g_44,&g_44},{&g_44,&g_44,(void*)0,&g_44}}};
                int32_t l_656 = (-1L);
                uint16_t **l_725 = &g_152[3];
                uint16_t *** const l_724 = &l_725;
                int16_t *l_727 = &g_600;
                int64_t *l_732 = &l_726;
                int i, j, k;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        for (k = 0; k < 2; k++)
                            l_556[i][j][k] = &l_91;
                    }
                }
            }
            if ((p_66.f0 |= (*l_723)))
            { 
                uint32_t l_744 = 0xC6B557CEL;
                uint32_t l_775 = 0x069136D1L;
                for (g_600 = 0; (g_600 <= 2); g_600 += 1)
                { 
                    union U1 **l_747 = &g_180[0];
                    union U1 **l_749 = (void*)0;
                    union U1 **l_750[6][7] = {{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0},{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0},{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0},{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0},{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0},{(void*)0,&l_736,(void*)0,(void*)0,&l_748[0][2],&l_748[0][2],(void*)0}};
                    const int16_t **l_752 = (void*)0;
                    const int16_t *l_754 = (void*)0;
                    const int16_t **l_753 = &l_754;
                    int16_t **l_755[2];
                    uint16_t *l_774[7][10] = {{&g_669,(void*)0,&g_153,(void*)0,&l_625,&l_625,&g_153,&g_140,&g_153,&l_625},{&l_625,(void*)0,&g_140,&l_625,&g_153,&l_625,&g_153,&g_669,&g_153,&g_153},{&g_669,(void*)0,&g_140,&g_669,(void*)0,(void*)0,&g_140,&g_153,&l_625,(void*)0},{&l_625,&g_669,&g_140,(void*)0,&g_153,(void*)0,&g_140,&g_153,&g_669,&g_669},{&g_140,(void*)0,&l_625,&g_669,&g_153,&l_625,(void*)0,&l_625,&g_153,(void*)0},{&g_153,&g_140,&g_153,&l_625,(void*)0,&l_625,&g_153,&g_140,&g_153,&l_625},{&g_140,&g_153,&g_669,&l_625,(void*)0,(void*)0,&g_153,&l_625,&l_625,&g_140}};
                    int32_t l_776 = 0x3BF149A4L;
                    int32_t l_777 = 0L;
                    const int32_t **l_778 = &g_104[0];
                    int32_t **l_779 = &l_91;
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_755[i] = (void*)0;
                    l_777 |= ((~(safe_lshift_func_int16_t_s_u((((((safe_mul_func_uint16_t_u_u(l_744, ((safe_rshift_func_int16_t_s_s(l_744, (((*l_747) = &g_44) == (l_751 = l_748[1][0])))) >= (((*l_753) = &g_217) != (g_218[2] = (l_756 = &g_600)))))) ^ p_65) && (safe_mul_func_uint8_t_u_u(((safe_add_func_int64_t_s_s((((((((((!(safe_add_func_int32_t_s_s((safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(((((safe_lshift_func_uint16_t_u_u((((safe_mul_func_uint16_t_u_u((l_775 |= ((safe_lshift_func_int8_t_s_u(((*l_91) ^ 0x40F1828AA16818E1LL), 5)) == 255UL)), 5UL)) != (*l_91)) < g_669), 12)) <= l_744) >= p_65) | p_65), p_67)), 0x98L)), 0xB8C623BFL))) >= p_67) && g_258.f2) ^ 0x5BA3L) | p_66.f0) | p_65) != g_387.f2) == (*l_723)) & (*l_91)), (*l_723))) ^ 0x3F59E911F9AAC44ALL), (*g_297)))) == (*g_297)) >= l_776), p_67))) != 0x449EL);
                    if (l_777)
                        break;
                    (*l_778) = (*g_315);
                    for (l_598 = 0; (l_598 <= 2); l_598 += 1)
                    { 
                        int i, j, k;
                        if (g_70[(g_600 + 2)][l_598][l_598])
                            break;
                        return &g_169;
                    }
                }
                (*l_90) = &l_574;
                if (p_67)
                    break;
            }
            else
            { 
                uint8_t l_786 = 0x32L;
                union U1 *l_790 = &g_44;
                uint32_t *l_815 = &g_258.f0;
                int32_t l_824[10];
                int i;
                for (i = 0; i < 10; i++)
                    l_824[i] = 0xD52FA3FEL;
                for (g_154 = (-2); (g_154 <= 59); g_154 = safe_add_func_int8_t_s_s(g_154, 2))
                { 
                    uint32_t l_783[7][1] = {{0x26DA54FCL},{1UL},{0x26DA54FCL},{1UL},{0x26DA54FCL},{1UL},{0x26DA54FCL}};
                    int i, j;
                    for (l_533 = 0; (l_533 <= 8); l_533 += 1)
                    { 
                        union U1 **l_789 = (void*)0;
                        (*g_525) = (*l_505);
                        l_783[0][0]--;
                        l_786--;
                        l_790 = (void*)0;
                    }
                    (*g_168) = (void*)0;
                    if (l_783[0][0])
                        break;
                }
                for (g_387.f2 = 0; (g_387.f2 == 8); ++g_387.f2)
                { 
                    for (l_574 = 0; (l_574 <= 26); l_574 = safe_add_func_uint64_t_u_u(l_574, 6))
                    { 
                        (*g_525) = g_795[0];
                        (*g_525) = (*g_388);
                        (*g_315) = (void*)0;
                    }
                    (*g_525) = (*l_655);
                    (*g_525) = g_796;
                }
                if ((l_782 |= (safe_div_func_int64_t_s_s((safe_add_func_int64_t_s_s(((((*l_815) &= (safe_add_func_int8_t_s_s(p_67, (((((((safe_unary_minus_func_uint64_t_u((0xFDB3L && (*l_723)))) != (safe_lshift_func_int8_t_s_s((+l_786), 6))) != ((p_66.f0 || (((safe_lshift_func_int16_t_s_s((((safe_add_func_uint64_t_u_u((65534UL || ((**l_90) >= (safe_rshift_func_int16_t_s_u((g_813 == (void*)0), 4)))), 0xD479374669890376LL)) > 0xE5L) <= (*l_723)), 9)) > l_786) & 0xF7L)) <= p_67)) | l_786) ^ p_67) ^ l_814) >= 247UL)))) ^ (-1L)) < 1L), (**l_90))), 0xE5ABE9EF1E536DFELL))))
                { 
                    (*l_90) = &l_531;
                    if (p_67)
                        continue;
                    (*l_723) = (*l_91);
                }
                else
                { 
                    uint32_t l_835 = 0UL;
                    int32_t l_845 = 0xD0DADFE9L;
                    for (g_387.f2 = 0; (g_387.f2 < 15); ++g_387.f2)
                    { 
                        uint16_t *l_822[5][10] = {{&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153},{(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0},{&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153,&g_153},{&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153},{(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0,&g_153,(void*)0,(void*)0}};
                        int32_t l_823 = 0xA9DE1A5DL;
                        uint32_t l_825 = 0xD930F1B7L;
                        int8_t *l_830 = &g_74;
                        int16_t *l_846[1][8] = {{&g_795[0].f2,&g_795[0].f2,&g_795[0].f2,&g_795[0].f2,&g_795[0].f2,&g_795[0].f2,&g_795[0].f2,&g_795[0].f2}};
                        int32_t *l_847 = &l_824[1];
                        int i, j;
                        l_847 = func_79((safe_add_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((((*l_723) = (((++l_825) | (l_845 ^= (((*l_756) &= (((*l_830) = (0x72L && (safe_add_func_uint32_t_u_u(((*l_815) = l_824[3]), p_66.f0)))) == (safe_add_func_int8_t_s_s((safe_add_func_int64_t_s_s(l_823, (l_835 | p_66.f0))), (((*l_723) && ((safe_rshift_func_uint16_t_u_s((safe_mod_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((((-1L) && l_844[4]) && 0x21L) ^ p_65), l_824[9])), l_823)) < 6L), p_67)), l_823)) | p_65)) == (*g_297)))))) || l_835))) >= (*l_91))) != 6UL), p_66.f0)), p_66.f0)), &l_738[7], l_748[1][2], p_67);
                        (*l_91) ^= (0UL > 4294967286UL);
                    }
                    (*l_91) = ((((*g_168) = &l_598) != l_848[4][4]) || p_67);
                }
                l_824[4] |= (l_849 ^ p_65);
            }
            return &g_169;
        }
        for (g_100 = 0; (g_100 <= 5); g_100 += 1)
        { 
            const uint32_t l_887 = 0xB2D87B01L;
            int32_t l_888 = 6L;
            int32_t l_958 = (-4L);
            int32_t l_963 = 6L;
            int32_t l_964 = 6L;
            uint32_t *l_981 = (void*)0;
            int32_t l_983 = 0x61A68599L;
            int32_t l_987 = (-2L);
            for (g_217 = 0; (g_217 <= 6); g_217 += 1)
            { 
                int8_t *l_852 = &g_551;
                (*l_723) ^= ((safe_lshift_func_int8_t_s_s((0xBE92F29EL || 0xFE17EBFEL), ((*l_852) = 0x51L))) <= 0xD0F8359F7E6F597FLL);
            }
            for (l_598 = 0; (l_598 <= 9); l_598 += 1)
            { 
                union U1 **l_853 = &g_180[0];
                uint8_t *l_854 = (void*)0;
                uint8_t *l_855 = &g_441;
                uint8_t ** const *l_871 = &l_664;
                uint16_t *l_881 = &g_140;
                int32_t l_886[4] = {5L,5L,5L,5L};
                uint32_t **l_971 = (void*)0;
                int i;
                if (p_66.f0)
                    break;
                (*l_90) = func_79(g_100, &g_169, ((*l_853) = l_751), (((*l_855) &= (*g_297)) <= (~(safe_lshift_func_uint8_t_u_s(((0xEAB73470L >= 0xEF3FDA06L) & 0L), 5)))));
                if ((safe_mul_func_uint8_t_u_u(((safe_div_func_int64_t_s_s(((safe_sub_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((*g_297), (safe_mod_func_int8_t_s_s(((&l_848[0][1] != (void*)0) == p_67), (**l_90))))) != (((l_871 != l_872) ^ (safe_mod_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((((*l_881) |= p_66.f0) & (((safe_rshift_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(1UL, p_65)), l_886[0])) || l_886[0]) <= 0x4559859FL)), p_65)), l_887)) >= 1UL), p_67))) != 0L)), l_886[0])) <= (*g_297)), p_66.f0)) > g_796.f0), p_66.f0)))
                { 
                    uint64_t l_893 = 0UL;
                    uint8_t *l_894 = &g_225;
                    for (p_66.f0 = 1; (p_66.f0 <= 5); p_66.f0 += 1)
                    { 
                        uint64_t l_890 = 1UL;
                        l_890--;
                    }
                    for (l_814 = 1; (l_814 <= 5); l_814 += 1)
                    { 
                        int i;
                        l_893 = ((*l_723) ^= (-2L));
                        g_795[g_100] = g_795[l_814];
                    }
                    for (g_258.f2 = 0; (g_258.f2 <= 5); g_258.f2 += 1)
                    { 
                        (*l_723) &= ((((**l_871) = l_894) != (void*)0) || p_67);
                        (*g_5) ^= p_67;
                    }
                }
                else
                { 
                    int16_t **l_895 = (void*)0;
                    int16_t *l_927 = &g_217;
                    uint32_t *l_928 = &g_504.f0;
                    uint32_t l_929[7][1][10] = {{{0x2E7D2A01L,0x0FC62B6CL,0x19CD8A6BL,0x19CD8A6BL,0x0FC62B6CL,0x2E7D2A01L,6UL,6UL,0x2E7D2A01L,0x0FC62B6CL}},{{0x0FC62B6CL,0x19CD8A6BL,0x19CD8A6BL,0x0FC62B6CL,0x2E7D2A01L,6UL,6UL,0x2E7D2A01L,0x0FC62B6CL,0x19CD8A6BL}},{{0x0FC62B6CL,0x0FC62B6CL,6UL,0x19CD8A6BL,0x2E7D2A01L,0x2E7D2A01L,0x19CD8A6BL,6UL,0x0FC62B6CL,0x0FC62B6CL}},{{0x2E7D2A01L,0x19CD8A6BL,6UL,0x0FC62B6CL,0x0FC62B6CL,6UL,0x19CD8A6BL,0x2E7D2A01L,0x2E7D2A01L,0x19CD8A6BL}},{{0x2E7D2A01L,0x0FC62B6CL,0x19CD8A6BL,0x19CD8A6BL,0x0FC62B6CL,0x2E7D2A01L,6UL,6UL,0x2E7D2A01L,0x0FC62B6CL}},{{0x0FC62B6CL,0x19CD8A6BL,0x19CD8A6BL,0x0FC62B6CL,0x2E7D2A01L,6UL,6UL,0x2E7D2A01L,0x0FC62B6CL,0x19CD8A6BL}},{{0x0FC62B6CL,0x0FC62B6CL,6UL,0x19CD8A6BL,0x2E7D2A01L,0x2E7D2A01L,0x19CD8A6BL,6UL,0x0FC62B6CL,0x0FC62B6CL}}};
                    int16_t l_950 = 0xE174L;
                    int32_t l_956 = 0xBC05BD5DL;
                    int32_t l_957 = 1L;
                    int32_t l_959 = 0x19E41BBEL;
                    int32_t l_960[10] = {0x71DD0F25L,0x71DD0F25L,0xFC9E71B0L,(-1L),0xFC9E71B0L,0x71DD0F25L,0x71DD0F25L,0xFC9E71B0L,3L,(-1L)};
                    const uint32_t * const l_974 = &g_70[4][1][2];
                    const uint32_t * const *l_973 = &l_974;
                    int32_t l_982 = 0xD60D7369L;
                    int32_t *l_989 = (void*)0;
                    int i, j, k;
                    (*g_896) = l_895;
                    g_388 = &g_795[g_100];
                    (*g_525) = g_795[g_100];
                    if ((safe_lshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(((*l_723) == p_65), 1)), ((~((+(((safe_div_func_int32_t_s_s(g_795[g_100].f2, (safe_mod_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u(p_67, (safe_sub_func_uint64_t_u_u(((18446744073709551615UL | (((l_888 = 0x8F42BFD5L) ^ 0x8C7D890FL) == ((safe_rshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((((((*l_928) ^= ((~(safe_div_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u(l_886[0], ((*l_927) &= ((((safe_add_func_uint32_t_u_u((g_462.f1 > (*l_723)), (-1L))) | 0x28AEL) | g_795[g_100].f0) | 65535UL)))) ^ p_66.f0), g_126))) & (**l_90))) >= g_795[g_100].f0) < p_66.f0) | g_387.f2), 7)), 3)) < p_66.f0))) || p_66.f0), l_929[5][0][2])))), 5)), l_930)))) <= l_886[0]) || p_65)) >= (-10L))) >= p_66.f0))))
                    { 
                        int32_t l_945 = 0x9C3CAD7DL;
                        uint64_t ***l_947[9][8] = {{&g_427,&g_427,(void*)0,(void*)0,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,(void*)0,&g_427,(void*)0,&g_427,&g_427,&g_427},{&g_427,(void*)0,&g_427,&g_427,&g_427,&g_427,(void*)0,&g_427},{&g_427,&g_427,&g_427,(void*)0,&g_427,(void*)0,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,(void*)0,(void*)0,&g_427,&g_427},{&g_427,&g_427,&g_427,(void*)0,&g_427,&g_427,&g_427,(void*)0},{&g_427,(void*)0,&g_427,(void*)0,(void*)0,&g_427,&g_427,(void*)0},{(void*)0,&g_427,&g_427,(void*)0,(void*)0,&g_427,(void*)0,&g_427},{(void*)0,&g_427,&g_427,&g_427,(void*)0,&g_427,&g_427,&g_427}};
                        uint64_t ****l_946 = &l_947[0][7];
                        uint64_t ***l_949[6] = {&g_427,&g_427,&g_427,&g_427,&g_427,&g_427};
                        uint64_t ****l_948 = &l_949[5];
                        int32_t l_955 = 1L;
                        int i, j;
                        (*l_723) = p_67;
                        p_66.f0 |= (safe_rshift_func_uint16_t_u_u((((*l_881) = l_886[0]) != (&g_169 == (void*)0)), 0));
                        (*l_723) ^= (safe_div_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(((safe_div_func_uint8_t_u_u(((l_939 != g_941) & (safe_rshift_func_uint8_t_u_u((0x10L <= ((l_945 = p_67) != (((*l_948) = ((*l_946) = &g_427)) != (void*)0))), l_950))), ((safe_mod_func_int64_t_s_s(p_65, (safe_mul_func_uint16_t_u_u(((*l_881) = ((-1L) <= l_955)), p_66.f0)))) ^ l_955))) >= 0xCCL), l_888)), l_886[0]));
                        l_966--;
                    }
                    else
                    { 
                        uint32_t ***l_972 = &g_640;
                        int64_t *l_975 = &l_726;
                        int32_t ***l_976[8] = {&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168,&g_168};
                        int i;
                        (*l_90) = func_79(((*l_975) = (safe_mul_func_int16_t_s_s((((*l_972) = l_971) != (l_973 = l_973)), p_66.f0))), (g_168 = (void*)0), l_736, ((safe_add_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((1L ^ ((**l_90) ^ 0xA6540C19L)), l_964)) <= g_796.f2), 3UL)) ^ 0L));
                        p_66.f0 = ((l_981 = l_981) == l_91);
                        g_984++;
                        l_989 = ((*l_90) = func_79(l_987, &g_169, l_988, p_66.f0));
                    }
                }
            }
        }
        return &g_169;
    }
    else
    { 
        uint64_t *l_990 = &g_154;
        int32_t l_1003 = 8L;
        struct S0 *l_1013 = &g_258;
        struct S0 **l_1012 = &l_1013;
        int32_t **l_1014[5][6][2] = {{{&l_91,&l_91},{&g_169,&l_91},{&l_91,(void*)0},{(void*)0,(void*)0},{&l_91,&l_91},{&g_169,&l_91}},{{&l_91,&l_91},{&g_169,&l_91},{&g_169,&l_91},{&g_169,&l_91},{&g_169,&l_91},{&l_91,&l_91}},{{&g_169,&l_91},{&l_91,(void*)0},{(void*)0,(void*)0},{&l_91,&l_91},{&g_169,&l_91},{&l_91,&l_91}},{{&g_169,&l_91},{&g_169,&l_91},{&g_169,&l_91},{&g_169,&l_91},{&l_91,&l_91},{&g_169,&l_91}},{{&l_91,(void*)0},{(void*)0,(void*)0},{&l_91,&l_91},{&g_169,&l_91},{&l_91,&l_91},{&g_169,&l_91}}};
        uint32_t *l_1015 = (void*)0;
        int32_t l_1017 = 7L;
        int i, j, k;
        l_961 |= (((0x95A241F0BB705C27LL <= ((((*l_990) = (&g_646 == (void*)0)) ^ p_66.f0) | (((((safe_mod_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s(((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s(l_1003, (safe_add_func_uint32_t_u_u((g_100 = (((((safe_rshift_func_int16_t_s_u((safe_div_func_int16_t_s_s(((l_1003 | (((*l_1012) = &g_795[4]) != (void*)0)) | (((void*)0 != l_1014[1][0][0]) <= p_66.f0)), (**l_90))), p_67)) > p_66.f0) || p_65) <= 4L) & p_67)), 0xAA797ED1L)))), (*g_297))), l_1016)), p_66.f0)) <= p_66.f0), p_67)) && p_65), p_67)) != 0xA581CE7AL) != p_65) < (-10L)) == l_1017))) || g_462.f1) < 65535UL);
        (**g_168) = (&l_726 != g_428);
        (**g_168) = (safe_mod_func_uint32_t_u_u(0x679FE8FDL, g_462.f0));
    }
    if (((0xE331L && 0x10DDL) & (~((safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u(((*l_1025) = (**l_90)), ((+((safe_add_func_uint64_t_u_u(0UL, (((l_962 = ((((&l_483 == ((*l_1031) = &l_1025)) != ((safe_sub_func_uint8_t_u_u(((*g_297) = (((((*l_1045) = (safe_add_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((safe_add_func_int64_t_s_s(((((void*)0 == l_1040) > (safe_mod_func_int8_t_s_s((**l_90), p_65))) <= l_1043), (-1L))), 0)), p_67))) == (*l_91)) || p_67) < (**l_90))), (-8L))) == p_65)) != p_65) && l_1047)) | 4UL) != g_462.f1))) > 0x27DE954755704BFDLL)) || 0x57L))), p_66.f0)) > (**l_90)))))
    { 
        uint8_t l_1057 = 0x2BL;
        uint8_t *l_1059 = &l_720[5];
        int64_t *l_1073 = &l_726;
        int32_t l_1084 = 0xC6696E77L;
        uint32_t l_1085 = 0x308BD248L;
        uint64_t ***l_1106[10][1] = {{(void*)0},{&g_427},{&g_427},{&g_427},{&g_427},{(void*)0},{&g_427},{&g_427},{&g_427},{&g_427}};
        int32_t l_1135 = (-10L);
        int32_t l_1136 = 1L;
        int32_t l_1139 = 0xCEC73D2EL;
        int32_t l_1141[5];
        uint64_t l_1143 = 18446744073709551615UL;
        struct S0 *l_1218 = &g_1217;
        int32_t l_1262 = 0xBA24AE70L;
        int32_t **l_1276 = &g_169;
        union U1 **l_1277 = (void*)0;
        int64_t l_1294 = (-6L);
        int32_t l_1295 = 0xF1252B5CL;
        int16_t **l_1301 = &g_218[7];
        uint8_t **l_1338 = &l_699;
        uint8_t ***l_1337[1][1];
        int32_t l_1385 = 0x56C3B8FCL;
        int32_t ***l_1414 = &l_1276;
        int32_t ****l_1413 = &l_1414;
        int32_t *l_1428 = &l_1136;
        int i, j;
        for (i = 0; i < 5; i++)
            l_1141[i] = 0xCAF36591L;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_1337[i][j] = &l_1338;
        }
        (**g_168) = 0xB2DCFB7BL;
        if ((safe_lshift_func_int8_t_s_s((g_551 = (safe_lshift_func_int16_t_s_s((safe_rshift_func_uint16_t_u_s(0x8E34L, (p_66.f0 >= (safe_mul_func_uint8_t_u_u(((*l_1059) = ((*g_297) = (l_1057 >= (l_1058 != g_813)))), (safe_rshift_func_uint16_t_u_s((((*l_1073) = (p_65 = ((safe_sub_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((1UL ^ (!((-1L) ^ (safe_sub_func_int32_t_s_s((safe_sub_func_int32_t_s_s((((((safe_mul_func_uint16_t_u_u((((**g_168) | (p_65 ^ p_66.f0)) | 9UL), 2UL)) == (-10L)) <= 7L) & l_1057) != p_65), (*g_169))), g_506.f0))))), 4)), (-6L))) | 18446744073709551614UL))) && p_65), p_67))))))), (**g_897)))), l_1057)))
        { 
            int32_t l_1076 = 0xE7A4FD13L;
            int32_t l_1078 = 0xCD9EDDB8L;
            int8_t l_1081 = 0x4EL;
            int32_t l_1083[1][10][8] = {{{0L,0x5D7B2D08L,(-1L),(-1L),0x5D7B2D08L,0L,7L,0L},{0x5D7B2D08L,0L,7L,0L,0x5D7B2D08L,(-1L),(-1L),0x5D7B2D08L},{0L,0L,0L,0L,(-9L),0x5D7B2D08L,(-9L),0L},{0L,(-9L),0L,(-1L),7L,7L,(-1L),0L},{(-9L),(-1L),(-9L),(-1L),0L,(-1L),(-9L),(-1L)},{(-1L),0x5D7B2D08L,7L,(-9L),(-9L),7L,0x5D7B2D08L,(-1L)},{0x5D7B2D08L,0L,(-1L),(-1L),(-1L),0L,0x5D7B2D08L,0x5D7B2D08L},{0L,(-1L),7L,7L,(-1L),0L,(-9L),0L},{(-1L),0L,(-9L),0L,(-1L),7L,7L,(-1L)},{0L,0x5D7B2D08L,0x5D7B2D08L,0L,(-1L),(-1L),(-1L),0L}}};
            int32_t **l_1104 = (void*)0;
            int64_t *l_1127[6] = {&l_726,&l_726,&l_726,&l_726,&l_726,&l_726};
            int16_t **l_1152 = &g_218[7];
            union U1 **l_1182 = &l_736;
            union U1 ***l_1181[9][1][3] = {{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}},{{&l_1182,&l_1182,&l_1182}}};
            uint32_t l_1195 = 1UL;
            int64_t l_1216 = 0x1F2792D44E87CE1ALL;
            uint64_t l_1239 = 0x9B57E9380A764283LL;
            uint32_t *l_1244 = &g_504.f0;
            uint32_t * const *l_1243[9] = {&l_1244,&l_1244,&l_1244,&l_1244,&l_1244,&l_1244,&l_1244,&l_1244,&l_1244};
            uint16_t l_1250 = 65534UL;
            int i, j, k;
            if (((**g_429) != (*g_427)))
            { 
                const int32_t **l_1075 = &g_104[4];
                int32_t *l_1077 = &l_889;
                int32_t *l_1079 = &l_1016;
                int32_t *l_1080[8] = {&l_782,(void*)0,(void*)0,&l_782,(void*)0,(void*)0,&l_782,(void*)0};
                int32_t **l_1092 = &l_91;
                int i;
                (*l_1075) = (**g_942);
                l_1085--;
                for (l_1085 = 0; (l_1085 >= 10); l_1085++)
                { 
                    for (l_1081 = 0; (l_1081 <= 5); l_1081 += 1)
                    { 
                        (*g_525) = g_1090;
                        return &g_169;
                    }
                    for (l_962 = 0; (l_962 <= 8); l_962 += 1)
                    { 
                        int32_t **l_1091 = &l_91;
                        return &g_169;
                    }
                    p_66.f0 ^= ((*g_169) = 1L);
                }
            }
            else
            { 
                uint32_t ***l_1093 = (void*)0;
                int32_t l_1120 = (-1L);
                int32_t l_1133 = 0x26F99160L;
                int32_t l_1134 = 1L;
                int32_t l_1137 = 0x150ADC46L;
                int32_t l_1140 = 7L;
                int32_t l_1142 = 0x85D996DDL;
                union U1 * const *l_1214[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t l_1215 = (-4L);
                uint32_t l_1222 = 0UL;
                uint32_t l_1263 = 8UL;
                int i;
                (*l_91) = l_1081;
                (**g_168) &= (&g_640 == l_1093);
                for (g_126 = 2; (g_126 >= 0); g_126 -= 1)
                { 
                    int32_t l_1111[5][8] = {{0L,1L,1L,0L,0xA4E30A68L,0L,1L,1L},{1L,0xA4E30A68L,0x537A8F4BL,0x537A8F4BL,0xA4E30A68L,1L,0xA4E30A68L,0x537A8F4BL},{0L,0xA4E30A68L,0L,1L,1L,0L,0xA4E30A68L,0L},{(-3L),1L,0x537A8F4BL,1L,(-3L),(-3L),1L,0x537A8F4BL},{(-3L),(-3L),1L,0x537A8F4BL,1L,(-3L),(-3L),1L}};
                    int16_t l_1163 = 0L;
                    uint64_t l_1194 = 8UL;
                    union U1 * const *l_1211 = &g_180[0];
                    int i, j;
                    for (l_889 = 2; (l_889 >= 0); l_889 -= 1)
                    { 
                        int32_t ***l_1105 = &g_168;
                        uint32_t *l_1128 = &g_387.f0;
                        int32_t *l_1129 = &l_1078;
                        int32_t *l_1130 = &g_44.f0;
                        int32_t *l_1131 = &l_782;
                        int32_t *l_1132[10][4][6] = {{{(void*)0,&l_1082[4][1],&l_1078,(void*)0,&l_1016,(void*)0},{&l_1111[3][6],(void*)0,&l_1083[0][1][3],(void*)0,&l_1111[3][6],&l_782},{&l_1084,&l_1078,&l_1084,(void*)0,&l_965[6],&l_962},{&l_1078,&l_1082[4][1],&l_1111[3][6],&l_1078,&l_1082[4][1],&l_962}},{{&l_1120,&l_965[6],&l_1084,&l_1083[0][5][4],(void*)0,&l_782},{&l_1082[4][1],&l_1016,&l_1083[0][1][3],&l_1082[4][1],&l_1082[4][1],(void*)0},{&l_1082[1][3],&l_1083[0][2][6],&l_1078,&l_965[1],&l_961,&l_1083[0][0][4]},{&l_961,&l_1078,&l_1082[1][3],&l_1120,&l_1078,&l_965[6]}},{{&l_1078,(void*)0,&l_1016,&l_965[6],&l_1082[4][1],&l_1120},{&l_965[6],&l_1084,&l_1111[3][6],&l_1111[3][6],&l_1083[0][5][4],&l_782},{&l_1078,&l_965[6],&l_1083[0][5][4],&l_782,&l_965[6],&l_1082[1][3]},{&l_1083[0][5][4],&l_1016,&l_1111[3][6],&l_965[0],&l_1084,&l_1016}},{{&l_1111[3][6],&l_962,&l_1084,&l_962,&l_1084,&l_782},{&l_1111[3][6],&l_1078,&l_965[6],&l_1111[2][7],&l_1111[3][6],&l_1120},{&l_1082[4][1],(void*)0,&l_1082[4][1],&l_782,&l_1083[0][5][4],&l_1084},{(void*)0,&l_1082[1][3],(void*)0,&l_965[6],&l_1120,&l_889}},{{(void*)0,&l_965[6],&l_1120,&l_1120,&l_965[6],(void*)0},{&g_44.f0,&l_1083[0][5][4],&l_1082[4][1],&l_1078,&l_965[6],&l_782},{&l_962,&l_1120,(void*)0,&l_1016,(void*)0,&l_1083[0][5][4]},{&l_962,&l_962,&l_1016,&l_1078,&l_1083[0][6][4],&l_782}},{{&g_44.f0,&l_965[6],&l_889,&l_1120,&l_1082[4][1],&l_1084},{(void*)0,&l_965[6],(void*)0,&l_965[6],&l_1082[4][1],(void*)0},{(void*)0,&l_782,&l_1111[3][6],&l_782,&l_1083[0][5][4],&l_1111[3][6]},{&l_1082[4][1],(void*)0,&l_1111[3][6],&l_1111[2][7],&l_1078,&l_1082[1][3]}},{{&l_1111[3][6],&l_1083[0][5][4],(void*)0,&l_962,&l_962,&l_1083[0][5][4]},{&l_1111[3][6],&l_889,&l_1016,&l_965[0],&l_1016,&l_1111[2][7]},{&l_1083[0][5][4],&l_1078,&l_1016,&l_782,&l_1120,(void*)0},{&l_1078,(void*)0,&l_1082[4][1],&l_1111[3][6],&l_1111[3][6],&l_1111[1][5]}},{{&l_965[6],&l_1083[0][5][4],&l_1111[3][6],&l_965[6],(void*)0,&l_1078},{&l_1078,&l_1083[0][5][4],&l_1082[4][1],&l_1082[1][3],&l_965[6],&l_889},{&l_965[6],&l_782,&l_1083[0][1][3],&l_1111[3][6],&l_1078,&l_782},{&l_889,&l_965[6],&g_44.f0,&l_1078,&g_44.f0,&l_965[6]}},{{&l_962,&l_1078,&l_889,&l_1111[3][6],(void*)0,&l_1111[2][7]},{&l_1078,&l_965[6],&l_1084,(void*)0,&l_1082[4][1],&l_965[6]},{&l_965[6],&l_965[6],(void*)0,&l_1083[0][0][4],(void*)0,&l_1082[4][1]},{&l_965[6],&l_1078,&l_1078,&l_782,&g_44.f0,&l_1120}},{{&l_1078,&l_965[6],&l_1111[3][6],&l_1083[0][5][4],&l_1078,&l_1120},{&l_1083[0][5][4],&l_782,&l_1111[3][6],(void*)0,&l_965[6],&l_1083[0][5][4]},{&l_1082[4][1],&l_1083[0][5][4],&l_962,&l_962,(void*)0,&l_1083[0][5][4]},{&l_1083[0][5][4],&l_1083[0][5][4],&l_889,&l_962,&l_1078,&l_961}}};
                        int i, j, k;
                        (**l_90) &= (((safe_lshift_func_uint8_t_u_s(p_66.f0, (0xD39E325FA5BCD29ALL != (p_66.f0 | (safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(((*l_1059) &= ((safe_mul_func_uint16_t_u_u(((((safe_mul_func_int16_t_s_s(p_67, (((((*l_1105) = l_1104) != l_1104) != (((void*)0 != l_1106[3][0]) <= ((safe_add_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((*g_297), p_66.f0)), l_1057)) & p_66.f0))) | l_1085))) != g_1090.f1) >= l_1057) & l_1057), 0xC49EL)) <= l_1111[3][6])), p_65)), l_1111[3][6])))))) ^ (*g_297)) | p_65);
                        (*l_1129) |= (((1UL <= (safe_mod_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u((safe_mod_func_uint8_t_u_u(((((((safe_add_func_uint32_t_u_u(l_1120, ((safe_add_func_int8_t_s_s(((*g_297) <= (safe_add_func_uint16_t_u_u(((l_1084 <= ((*l_1128) = ((safe_add_func_int16_t_s_s(((void*)0 != l_1127[5]), (-1L))) ^ (l_1120 < p_65)))) & 0x77L), (**g_897)))), (*g_297))) | (**g_105)))) < 0xCB9CL) > (**l_90)) <= 0xA8L) >= g_441) > g_74), l_1057)), 2)) > 0x656BL), p_66.f0))) != p_65) >= g_600);
                        l_1143++;
                    }
                    if ((safe_add_func_uint64_t_u_u(((void*)0 == &g_225), ((safe_mul_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u(((**l_1152) = (l_1152 == (void*)0)), (safe_add_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(0UL, (safe_mod_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(l_1163, ((**l_90) && 0x3267L))) ^ (safe_sub_func_uint32_t_u_u((((**g_896) == (*g_897)) != 0x75L), 0x14FBB7A3L))), (*g_297))))), l_1139)), p_67)))) != 0UL), 0x13L)) >= p_66.f0))))
                    { 
                        union U1 *** const l_1180 = (void*)0;
                        int32_t l_1193 = (-3L);
                        (*l_91) = (safe_rshift_func_int8_t_s_s(p_67, (safe_mul_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((p_66.f0 == p_66.f0), (safe_div_func_int8_t_s_s(l_1135, (safe_sub_func_int16_t_s_s((((l_1180 != l_1181[4][0][0]) <= (safe_mul_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((-1L), (safe_rshift_func_uint16_t_u_s(65535UL, ((**l_1152) = (safe_add_func_int8_t_s_s(g_796.f2, p_65))))))) <= 0x39L), l_1193)), l_1194))) & l_1120), p_65)))))) <= l_1195), 0xD31DL)), p_67)), p_67))));
                        if ((****g_941))
                            break;
                        p_66.f0 = p_67;
                        p_66.f0 |= ((*g_169) |= 0xF23141B3L);
                    }
                    else
                    { 
                        int64_t **l_1210 = &l_1127[2];
                        union U1 * const **l_1212 = (void*)0;
                        union U1 * const **l_1213[2];
                        struct S0 **l_1219 = &l_1218;
                        struct S0 *l_1221 = &g_795[0];
                        struct S0 **l_1220 = &l_1221;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1213[i] = &l_1211;
                        l_1142 |= (safe_sub_func_int16_t_s_s(1L, (safe_lshift_func_int16_t_s_u((***g_896), (safe_rshift_func_uint16_t_u_u(p_66.f0, (l_1134 = (safe_sub_func_int32_t_s_s(((**l_90) = ((((p_65 & (safe_add_func_uint16_t_u_u(p_67, (safe_rshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u((((*l_1210) = g_428) != (void*)0), ((l_1214[6] = l_1211) == l_1211))), 7))))) >= l_1215) < l_1216) & p_65)), 0xE6376BE3L)))))))));
                        (*g_525) = g_1217;
                        (*l_1220) = ((*l_1219) = l_1218);
                    }
                    if (p_66.f0)
                        continue;
                    for (l_961 = 2; (l_961 >= 0); l_961 -= 1)
                    { 
                        int32_t l_1227 = 0xDF86FC1CL;
                        p_66.f0 = 3L;
                        l_1222++;
                        (*g_169) &= (((l_1139 || ((safe_rshift_func_uint16_t_u_u((l_1227 ^= 1UL), 1)) || (l_1133 == 0x0BL))) & p_67) && p_67);
                    }
                }
                for (l_1078 = 0; (l_1078 > 21); l_1078++)
                { 
                    uint16_t l_1234 = 0UL;
                    int32_t l_1248 = (-7L);
                    int32_t l_1249 = (-6L);
                    int32_t l_1261 = 0x75DA97C6L;
                    uint16_t **l_1271 = &g_152[5];
                    uint16_t ***l_1270 = &l_1271;
                    if (((*l_91) = ((!((((*g_429) != (void*)0) || l_1222) == (safe_mul_func_uint16_t_u_u((~l_1234), (((void*)0 == l_1127[5]) | ((safe_rshift_func_uint16_t_u_s(0xA79AL, 0)) | (0x1B0276E82A9B9ED1LL | ((**g_427) < l_1239)))))))) == p_65)))
                    { 
                        struct S0 ** const l_1242 = &l_1218;
                        int32_t l_1245 = 0xAB3E55A7L;
                        if ((*g_169))
                            break;
                        p_66.f0 &= ((safe_add_func_int64_t_s_s(((void*)0 != l_1242), ((void*)0 != l_1243[2]))) || l_1245);
                    }
                    else
                    { 
                        int32_t *l_1247[3][4][1] = {{{&l_1083[0][4][0]},{&l_965[6]},{&l_1083[0][4][0]},{&l_965[6]}},{{&l_1083[0][4][0]},{&l_965[6]},{&l_1083[0][4][0]},{&l_965[6]}},{{&l_1083[0][4][0]},{&l_965[6]},{&l_1083[0][4][0]},{&l_965[6]}}};
                        int i, j, k;
                        if ((**g_105))
                            break;
                        (*l_91) ^= (~0x9D90F1B3L);
                        --l_1250;
                    }
                    for (g_74 = 1; (g_74 > (-1)); --g_74)
                    { 
                        int32_t *l_1255 = &l_961;
                        int32_t *l_1256 = &l_889;
                        int32_t *l_1257 = &g_4;
                        int32_t *l_1258 = &l_782;
                        int32_t *l_1259 = &l_1248;
                        int32_t *l_1260[9][7][4] = {{{&l_1215,(void*)0,&l_1120,&l_1120},{&l_1215,&l_1215,(void*)0,&l_1078},{&g_44.f0,&l_1120,&l_1215,&l_1137},{(void*)0,&l_1083[0][5][4],&l_1136,&l_1215},{(void*)0,&l_1083[0][5][4],(void*)0,&l_1137},{&l_1083[0][5][4],&l_1120,&l_1083[0][1][7],&l_1078},{&l_1078,&l_1215,(void*)0,&l_1120}},{{&l_1078,(void*)0,(void*)0,&l_1078},{&l_1078,&l_1137,&l_1083[0][1][7],(void*)0},{&l_1083[0][5][4],&g_4,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1136,(void*)0},{(void*)0,&g_4,&l_1215,(void*)0},{&g_44.f0,&l_1137,(void*)0,&l_1078},{&l_1215,(void*)0,&l_1120,&l_1120}},{{&l_1215,&l_1215,(void*)0,&l_1078},{&g_44.f0,&l_1120,&l_1215,&l_1137},{(void*)0,&l_1083[0][5][4],&l_1136,&l_1215},{(void*)0,&l_1083[0][5][4],(void*)0,&l_1137},{&l_1083[0][5][4],&l_1120,&l_1083[0][1][7],&l_1078},{&l_1078,&l_1215,(void*)0,&l_1120},{&l_1078,(void*)0,(void*)0,&l_1078}},{{&l_1078,&l_1137,&l_1083[0][1][7],(void*)0},{&l_1083[0][5][4],&g_4,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1136,(void*)0},{(void*)0,&g_4,&l_1215,(void*)0},{&g_44.f0,&l_1137,(void*)0,&l_1078},{&l_1215,(void*)0,&l_1120,&l_1120},{&l_1215,&l_1215,(void*)0,&l_1078}},{{&g_44.f0,&l_1120,&l_1215,&l_1137},{(void*)0,&l_1083[0][5][4],&l_1136,&l_1215},{(void*)0,&l_1083[0][5][4],(void*)0,&l_1137},{&l_1083[0][5][4],&l_1120,&l_1083[0][1][7],&l_1078},{&l_1078,&l_1215,(void*)0,&l_1120},{&l_1078,(void*)0,(void*)0,&l_1078},{&l_1078,&l_1137,&l_1083[0][1][7],(void*)0}},{{&l_1083[0][5][4],&g_4,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1136,(void*)0},{(void*)0,&g_4,&l_1215,(void*)0},{&g_44.f0,&l_1137,(void*)0,&l_1078},{&l_1215,(void*)0,&l_1120,&l_1120},{&l_1215,&l_1215,(void*)0,&l_1078},{&g_44.f0,&l_1120,&l_1215,&l_1137}},{{(void*)0,&l_1083[0][5][4],&l_1136,&l_1215},{(void*)0,&l_1083[0][5][4],(void*)0,&g_4},{&l_1078,(void*)0,&l_1136,&l_1137},{&l_1137,(void*)0,&l_1078,(void*)0},{&l_1215,&l_1078,&l_1078,&l_1215},{&l_1137,&g_4,&l_1136,&l_1120},{&l_1078,&l_1083[0][1][7],&l_1120,&l_1083[0][5][4]}},{{&l_1120,&l_1083[0][5][4],&g_44.f0,&l_1083[0][5][4]},{&l_1078,&l_1083[0][1][7],(void*)0,&l_1120},{(void*)0,&g_4,&l_1083[0][5][4],&l_1215},{(void*)0,&l_1078,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1083[0][5][4],&l_1137},{(void*)0,(void*)0,(void*)0,&g_4},{&l_1078,&l_1078,&g_44.f0,(void*)0}},{{&l_1120,&l_1078,&l_1120,&g_4},{&l_1078,(void*)0,&l_1136,&l_1137},{&l_1137,(void*)0,&l_1078,(void*)0},{&l_1215,&l_1078,&l_1078,&l_1215},{&l_1137,&g_4,&l_1136,&l_1120},{&l_1078,&l_1083[0][1][7],&l_1120,&l_1083[0][5][4]},{&l_1120,&l_1083[0][5][4],&g_44.f0,&l_1083[0][5][4]}}};
                        const int32_t **l_1267 = &g_104[4];
                        int i, j, k;
                        ++l_1263;
                        (*l_1267) = (**g_942);
                    }
                    (*g_169) ^= (safe_add_func_int32_t_s_s((&g_646 == l_1270), ((safe_mod_func_uint8_t_u_u((0UL | (safe_lshift_func_uint16_t_u_u(p_65, 7))), l_1262)) > p_65)));
                }
            }
            l_1276 = (void*)0;
        }
        else
        { 
            return &g_169;
        }
        if ((p_66.f0 = ((*l_91) = (l_1277 != l_1277))))
        { 
            int64_t l_1280 = 0x4A268183A7FAE179LL;
            uint32_t l_1281 = 0x7D26304DL;
            int32_t l_1287 = (-8L);
            int32_t l_1290 = 0xBC0E091FL;
            int32_t l_1293[7][5] = {{0xC41387A8L,0x61D8F774L,0x61D8F774L,0xC41387A8L,0x556D0AD9L},{8L,0x01180FD8L,0x09728BFFL,0x09728BFFL,0x01180FD8L},{0x556D0AD9L,0x61D8F774L,0x4F5E2768L,0L,0L},{(-2L),0x79E585CCL,(-2L),0x09728BFFL,5L},{0x05578BD1L,0xC41387A8L,0L,0xC41387A8L,0x05578BD1L},{(-2L),8L,0x79E585CCL,0x01180FD8L,0x79E585CCL},{0x556D0AD9L,0x556D0AD9L,0L,0x05578BD1L,0x760E449EL}};
            int32_t l_1318 = 1L;
            int32_t ***l_1332 = (void*)0;
            int i, j;
            for (g_1046 = 0; (g_1046 <= 5); g_1046 += 1)
            { 
                int32_t l_1284 = 3L;
                int32_t l_1288 = 0xECDF5736L;
                int32_t l_1289 = 0L;
                int32_t l_1291[8][6] = {{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L},{(-2L),0x21B26D56L,0x21B26D56L,(-2L),0x21B26D56L,0x21B26D56L}};
                int8_t l_1292 = 4L;
                uint16_t l_1296 = 0x0E7DL;
                int16_t **l_1299 = &g_218[0];
                int16_t ***l_1300[7] = {&l_1299,&l_1299,&l_1299,&l_1299,&l_1299,&l_1299,&l_1299};
                int64_t ***l_1308 = (void*)0;
                int64_t ***l_1309 = &l_1306;
                uint32_t l_1316 = 1UL;
                uint8_t ** const l_1335 = (void*)0;
                uint8_t ** const *l_1334 = &l_1335;
                int i, j;
                p_66.f0 = ((((safe_mod_func_int16_t_s_s(((((***l_1031) = p_67) < (l_1281--)) > (*g_297)), l_1284)) <= (((*l_91) = ((((((safe_add_func_uint64_t_u_u((++l_1296), ((((*g_896) = (l_1301 = l_1299)) != (void*)0) > (safe_mod_func_uint32_t_u_u(l_1289, ((safe_div_func_uint8_t_u_u((((((((*l_1309) = l_1306) != (void*)0) | (safe_mod_func_uint8_t_u_u(((((safe_rshift_func_int16_t_s_s(((safe_add_func_int8_t_s_s((-1L), 1UL)) > p_66.f0), p_65)) >= p_67) > 0x46E03C97470C9364LL) || l_1287), g_1217.f0))) <= p_67) & l_1316) | 255UL), l_1288)) | p_66.f0)))))) != l_1284) & 0UL) ^ p_67) < p_66.f0) <= l_1317[4][1][3])) | l_1290)) && (-5L)) != l_1318);
                for (g_1217.f2 = 0; (g_1217.f2 <= 5); g_1217.f2 += 1)
                { 
                    uint32_t *l_1327[9][7][4] = {{{&g_70[2][2][1],(void*)0,&g_504.f0,(void*)0},{(void*)0,&g_504.f0,(void*)0,&g_504.f0},{&g_1217.f0,&g_258.f0,&g_100,(void*)0},{&g_258.f0,&l_1047,&g_795[0].f0,&g_504.f0},{&g_258.f0,(void*)0,&g_100,&g_1217.f0},{&g_1217.f0,&g_504.f0,(void*)0,&l_1047},{(void*)0,&g_795[0].f0,&g_387.f0,&g_795[0].f0}},{{&l_1047,&g_100,&g_795[0].f0,&g_504.f0},{(void*)0,&g_70[2][2][1],&g_504.f0,&g_795[0].f0},{&g_70[0][1][2],(void*)0,(void*)0,&g_70[0][1][2]},{&g_504.f0,&g_258.f0,&l_1047,&g_1217.f0},{(void*)0,&g_795[0].f0,&g_795[0].f0,&g_70[3][1][1]},{&g_100,&l_1047,&g_795[0].f0,&g_70[3][1][1]},{(void*)0,&g_795[0].f0,&g_795[0].f0,&g_1217.f0}},{{(void*)0,&g_258.f0,&g_795[0].f0,&g_70[0][1][2]},{&g_387.f0,(void*)0,&g_70[3][1][1],&g_795[0].f0},{&g_258.f0,&g_70[2][2][1],&g_795[0].f0,&g_504.f0},{&g_504.f0,&g_100,(void*)0,&g_795[0].f0},{(void*)0,&g_795[0].f0,(void*)0,&g_795[0].f0},{&g_70[0][1][2],&g_1217.f0,&g_795[0].f0,&g_504.f0},{&g_387.f0,(void*)0,&g_387.f0,&g_1217.f0}},{{&g_1217.f0,&g_258.f0,&g_387.f0,&g_504.f0},{&g_387.f0,&g_504.f0,&g_795[0].f0,&g_387.f0},{&g_70[0][1][2],&g_70[2][2][1],(void*)0,&g_258.f0},{(void*)0,&g_258.f0,(void*)0,(void*)0},{&g_504.f0,(void*)0,&g_795[0].f0,&g_795[0].f0},{&g_258.f0,&g_70[4][2][2],&g_70[3][1][1],&g_70[3][1][1]},{&g_387.f0,&g_387.f0,&g_795[0].f0,&g_795[0].f0}},{{(void*)0,&g_258.f0,&g_795[0].f0,&g_258.f0},{(void*)0,(void*)0,&g_795[0].f0,&g_795[0].f0},{&g_100,(void*)0,&g_795[0].f0,&g_258.f0},{(void*)0,&g_258.f0,&l_1047,&g_795[0].f0},{&g_504.f0,&g_387.f0,(void*)0,&g_70[3][1][1]},{&g_70[0][1][2],&g_70[4][2][2],&g_504.f0,&g_795[0].f0},{(void*)0,(void*)0,&g_795[0].f0,(void*)0}},{{&l_1047,&g_258.f0,&g_387.f0,&g_258.f0},{(void*)0,&g_70[2][2][1],&g_70[3][1][1],&g_387.f0},{&g_100,&g_504.f0,(void*)0,&g_504.f0},{(void*)0,&g_258.f0,(void*)0,&g_1217.f0},{(void*)0,(void*)0,(void*)0,&g_504.f0},{&g_100,&g_1217.f0,&g_70[3][1][1],&g_795[0].f0},{(void*)0,&g_795[0].f0,&g_387.f0,&g_795[0].f0}},{{&l_1047,&g_100,&g_795[0].f0,&g_504.f0},{(void*)0,&g_70[2][2][1],&g_504.f0,&g_795[0].f0},{&g_70[0][1][2],(void*)0,(void*)0,&g_70[0][1][2]},{&g_504.f0,&g_258.f0,&l_1047,&g_1217.f0},{(void*)0,&g_795[0].f0,&g_795[0].f0,&g_70[3][1][1]},{&g_100,&l_1047,&g_795[0].f0,&g_70[3][1][1]},{(void*)0,&g_795[0].f0,&g_795[0].f0,&g_1217.f0}},{{(void*)0,&g_258.f0,&g_795[0].f0,&g_70[0][1][2]},{&g_387.f0,(void*)0,&g_70[3][1][1],&g_795[0].f0},{&g_258.f0,&g_70[2][2][1],&g_795[0].f0,&g_504.f0},{&g_504.f0,&g_100,(void*)0,&g_795[0].f0},{(void*)0,&g_795[0].f0,(void*)0,&g_795[0].f0},{&g_795[0].f0,&g_504.f0,&g_795[0].f0,&g_387.f0},{&g_258.f0,(void*)0,&g_795[0].f0,&g_504.f0}},{{&g_504.f0,&g_504.f0,&g_795[0].f0,&g_70[2][2][1]},{&g_258.f0,&g_1217.f0,&g_795[0].f0,&g_795[0].f0},{&g_795[0].f0,&l_1047,&g_795[0].f0,(void*)0},{&g_795[0].f0,(void*)0,(void*)0,&g_70[3][1][1]},{&g_1217.f0,(void*)0,&g_70[4][2][2],&g_795[0].f0},{(void*)0,&l_1085,&g_100,&g_100},{&g_258.f0,&g_258.f0,(void*)0,&l_1047}}};
                    int32_t ****l_1333 = &l_1332;
                    uint8_t ****l_1336[8] = {&l_872,&l_872,&l_872,&l_872,&l_872,&l_872,&l_872,&l_872};
                    uint16_t **l_1341 = &l_1048[0];
                    int i, j, k;
                    (*g_169) = (safe_sub_func_int16_t_s_s(p_66.f0, (safe_add_func_uint64_t_u_u((safe_lshift_func_int16_t_s_u(0x801AL, (((**g_427) = 18446744073709551615UL) ^ ((((g_387.f0--) ^ (safe_sub_func_int8_t_s_s((((*l_1333) = l_1332) != &l_1276), ((l_1334 == (l_1337[0][0] = (void*)0)) && (safe_mod_func_uint64_t_u_u((((*l_1341) = &g_669) != (**g_645)), g_1217.f1)))))) <= 0xFF91BFAD976AC297LL) == 0L)))), 0x87131205C869AF3ALL))));
                }
            }
        }
        else
        { 
            int32_t *l_1342 = &l_962;
            int32_t *l_1343 = &l_1135;
            int32_t *l_1344 = &g_4;
            int32_t *l_1345 = (void*)0;
            int32_t *l_1346 = &l_1262;
            int32_t *l_1347 = &g_4;
            int32_t *l_1348 = &g_44.f0;
            int32_t *l_1349 = &l_1082[4][1];
            int32_t l_1350 = 0x24517928L;
            int32_t *l_1351[2][10] = {{(void*)0,&l_1082[1][0],&l_1082[1][0],(void*)0,&l_965[6],&l_1084,&l_965[6],(void*)0,&l_1082[1][0],&l_1082[1][0]},{&l_965[6],&l_1082[1][0],&l_1084,&l_1295,&l_1295,&l_1084,&l_1082[1][0],&l_965[6],&l_1082[1][0],&l_1084}};
            uint32_t l_1352 = 0x4245549FL;
            int32_t l_1384 = 0x1BA27AA0L;
            uint64_t l_1387 = 18446744073709551615UL;
            int i, j;
            ++l_1352;
            (*l_90) = (*l_90);
            for (g_504.f0 = 9; (g_504.f0 < 52); g_504.f0 = safe_add_func_int32_t_s_s(g_504.f0, 5))
            { 
                int64_t l_1366[3];
                int32_t l_1371 = 2L;
                int32_t l_1380 = 0x0DB21327L;
                int32_t l_1383[9][4][7] = {{{(-9L),9L,9L,0x92B1FE9FL,0xDB5C2040L,5L,0xE44FA500L},{0x0BDEDCBFL,0x66B92825L,0x932BB62FL,0xFAF9BA24L,0x6825288DL,7L,0x072D2F84L},{1L,1L,(-1L),(-9L),0x0BDEDCBFL,0x932BB62FL,0x55CC959DL},{0x60C34AF1L,(-1L),0x05FB7DE6L,0x60C34AF1L,0L,1L,0x3FB18994L}},{{0xFAF9BA24L,1L,0x932BB62FL,(-1L),(-1L),(-9L),0x6D5E59B5L},{0x66B92825L,(-9L),0L,0xEA194259L,1L,0x6825288DL,0x932BB62FL},{0x8BE6F925L,0x92B1FE9FL,(-4L),0xBD585E48L,9L,3L,0x0BDEDCBFL},{0xAA75636AL,0x6825288DL,0x68E7C78BL,0xBD585E48L,0x6F02610CL,0xE73A59DBL,0x072D2F84L}},{{(-1L),1L,1L,0xEA194259L,0xDDEEB6C3L,(-1L),0L},{0xCE5B39A1L,0xAA75636AL,0xBE47FEAEL,(-1L),(-9L),0L,0xDDEEB6C3L},{(-1L),0xEA194259L,0xA6E11D67L,0x60C34AF1L,0x68E7C78BL,0x53311346L,0x8BE6F925L},{0x6F02610CL,0xBDF09BD4L,(-9L),(-9L),0xBDF09BD4L,0x6F02610CL,4L}},{{0x8BE6F925L,0xE44FA500L,0xBBD30491L,0xFAF9BA24L,(-9L),0xF2F2B140L,0x92B1FE9FL},{0L,1L,0x6825288DL,0x92B1FE9FL,(-1L),1L,0xFAF9BA24L},{0x8E3288EDL,0xE44FA500L,1L,4L,0xAA75636AL,0x68E7C78BL,(-9L)},{0x60C34AF1L,0xBDF09BD4L,0L,0x8BE6F925L,9L,0x7FBD899EL,0x60C34AF1L}},{{9L,0xEA194259L,(-1L),0xDDEEB6C3L,4L,1L,(-1L)},{0x55CC959DL,0xAA75636AL,0x99D68AB7L,0L,(-1L),(-1L),0xB3AFB621L},{0xA6E11D67L,0L,(-9L),0x66B92825L,0x6825288DL,0x8BE6F925L,7L},{0L,(-1L),6L,1L,0xC2C72957L,6L,7L}},{{0L,0x40BF254FL,0L,1L,7L,0xF2F2B140L,0xB3AFB621L},{0x6F02610CL,0xA6E11D67L,0xCE5B39A1L,0L,1L,(-8L),0x55CC959DL},{7L,(-9L),0x1CE9D404L,0x99D68AB7L,(-4L),0xAE54BD32L,0x53311346L},{0xF2F2B140L,0x55CC959DL,9L,0xC2C72957L,0x99D68AB7L,0xC2C72957L,9L}},{{(-1L),(-1L),0xAA75636AL,0x66B92825L,(-1L),0xCE5B39A1L,(-1L)},{7L,0x7FBD899EL,(-1L),1L,0xBBD30491L,9L,0x40BF254FL},{0xBBD30491L,1L,9L,0xA8DF9205L,(-1L),(-4L),0xF2F2B140L},{0x9C044A38L,(-9L),0L,(-1L),0x99D68AB7L,0x92B1FE9FL,(-1L)}},{{0L,0x843C0F31L,0L,(-1L),(-4L),0x460B2325L,0x932BB62FL},{5L,1L,0L,(-4L),1L,0xBBD30491L,(-1L)},{1L,0x68E7C78BL,0x92B1FE9FL,(-1L),7L,(-1L),0x66B92825L},{0x8E3288EDL,0xF2F2B140L,3L,1L,0xC2C72957L,1L,1L}},{{0L,0x55CC959DL,3L,0L,0x6825288DL,(-4L),1L},{(-1L),0x66B92825L,0x92B1FE9FL,0x55CC959DL,0x9C044A38L,9L,0L},{0x68E7C78BL,5L,0L,1L,0xF2F2B140L,(-1L),0x99D68AB7L},{0xBE47FEAEL,0x40BF254FL,0L,(-1L),1L,0xC2C72957L,0xC2C72957L}}};
                uint8_t l_1424 = 0x97L;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_1366[i] = 0x07B7A0D5327C9EBDLL;
                for (g_551 = 0; (g_551 < 2); ++g_551)
                { 
                    uint32_t ** const **l_1362 = (void*)0;
                    int16_t ***l_1370 = &g_897;
                    int16_t ****l_1369 = &l_1370;
                    int32_t l_1372 = (-1L);
                    int32_t l_1374 = 0x76D77CBEL;
                    int32_t l_1377[4] = {0xE9756C86L,0xE9756C86L,0xE9756C86L,0xE9756C86L};
                    int64_t l_1393[6];
                    int16_t **l_1410 = &g_218[0];
                    int8_t *l_1417 = &g_1382;
                    uint32_t l_1427[5][9] = {{1UL,0x32109563L,0UL,0UL,0UL,0x32109563L,1UL,0x32109563L,0UL},{1UL,6UL,6UL,1UL,6UL,6UL,1UL,6UL,6UL},{1UL,0x32109563L,0UL,0UL,0UL,0x32109563L,1UL,0x32109563L,0UL},{1UL,6UL,6UL,1UL,6UL,6UL,1UL,6UL,6UL},{1UL,0x32109563L,0UL,0UL,0UL,0x32109563L,1UL,0x32109563L,0UL}};
                    int i, j;
                    for (i = 0; i < 6; i++)
                        l_1393[i] = (-1L);
                    if ((safe_sub_func_uint16_t_u_u((((g_1363 = l_1361) != &g_640) >= l_1366[0]), (((safe_unary_minus_func_int64_t_s((p_66.f0 || (l_1368 != (void*)0)))) == ((((*l_1369) = (void*)0) != (void*)0) || ((void*)0 == &l_1352))) <= (**g_105)))))
                    { 
                        int32_t l_1373 = 0xB78E4780L;
                        int32_t l_1376 = 0x4B504388L;
                        int32_t l_1378 = 2L;
                        int32_t l_1379 = 8L;
                        int32_t l_1381 = (-1L);
                        int32_t l_1386 = 0xB30DEAC9L;
                        uint16_t l_1390 = 65535UL;
                        --l_1387;
                        if (p_67)
                            continue;
                        l_1390--;
                    }
                    else
                    { 
                        int8_t l_1394 = (-1L);
                        int32_t l_1395 = 0x85DAAA35L;
                        int32_t l_1396[8][2] = {{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)}};
                        uint32_t l_1397 = 0x0374BD26L;
                        const int16_t *l_1409 = &g_600;
                        const int16_t * const *l_1408 = &l_1409;
                        int i, j;
                        ++l_1397;
                        (*l_1346) = (p_65 | (safe_div_func_int32_t_s_s((p_67 >= ((safe_unary_minus_func_uint32_t_u(p_66.f0)) | (safe_unary_minus_func_int32_t_s((safe_rshift_func_uint8_t_u_s((safe_lshift_func_int8_t_s_s((l_1408 == l_1410), 3)), ((safe_lshift_func_uint8_t_u_s(((&g_942 != l_1413) & ((void*)0 != &p_66)), 1)) != g_1090.f1))))))), l_1374)));
                        return &g_169;
                    }
                    (*l_1342) = (safe_sub_func_int8_t_s_s(((*l_1417) = g_1046), (safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s(((*g_297)++), 5)) != (((**l_90) ^ 0x01L) || ((l_1383[0][3][2] ^ (((**g_427) &= (l_1424 || p_67)) == ((safe_mod_func_int16_t_s_s(l_1427[3][3], (***g_896))) != (l_1393[0] || g_551)))) | 0xFFL))), (-10L)))));
                }
            }
        }
        (*l_90) = l_1428;
    }
    else
    { 
        for (g_387.f0 = 0; (g_387.f0 <= 55); g_387.f0 = safe_add_func_uint64_t_u_u(g_387.f0, 2))
        { 
            l_1431 &= (***g_942);
            for (g_258.f0 = 0; (g_258.f0 <= 38); g_258.f0 = safe_add_func_uint16_t_u_u(g_258.f0, 1))
            { 
                struct S0 *l_1436[3][7] = {{(void*)0,&g_795[0],(void*)0,&g_258,&g_795[0],&g_1217,&g_1217},{&g_795[0],&g_504,&g_258,&g_504,&g_795[0],&g_258,(void*)0},{(void*)0,&g_1217,&g_258,(void*)0,&g_258,&g_1217,(void*)0}};
                int i, j;
                for (g_387.f2 = 0; (g_387.f2 >= (-6)); g_387.f2--)
                { 
                    (*g_1437) = l_1436[1][4];
                }
            }
        }
    }
    return &g_169;
}



static uint8_t  func_75(uint32_t * p_76, uint32_t * p_77, uint32_t  p_78)
{ 
    uint32_t l_384[7][4] = {{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L},{0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L,0xBEFFFF99L}};
    int32_t l_389 = 7L;
    int32_t l_391 = 0x10CE6646L;
    int32_t l_399[9][5] = {{(-1L),0x217372B4L,0x0554811BL,0x0554811BL,0x217372B4L},{0x217372B4L,(-4L),(-1L),0xD0A345A6L,0L},{0x8A595CDAL,(-4L),0xD0A345A6L,(-8L),0x3FE9918FL},{(-3L),0x217372B4L,0x217372B4L,(-3L),(-8L)},{0x8A595CDAL,0x0554811BL,0L,1L,(-8L)},{0x217372B4L,0x8A595CDAL,0x3FE9918FL,(-1L),0x3FE9918FL},{(-1L),(-1L),(-8L),1L,0L},{5L,0x8CBA1B5FL,(-8L),(-3L),0x217372B4L},{0xD0A345A6L,(-8L),0x3FE9918FL,(-8L),0xD0A345A6L}};
    int32_t l_408 = 0x39DBE0F0L;
    int64_t l_409[2][8] = {{0x2E12BC1A5B93EE89LL,0x775A7EF1A6B96E4FLL,0x775A7EF1A6B96E4FLL,0x2E12BC1A5B93EE89LL,0L,0x2E12BC1A5B93EE89LL,0x775A7EF1A6B96E4FLL,0x775A7EF1A6B96E4FLL},{0x775A7EF1A6B96E4FLL,0L,1L,1L,0L,0x775A7EF1A6B96E4FLL,0L,1L}};
    uint64_t ** const l_425 = (void*)0;
    int8_t *l_467 = &g_74;
    uint8_t *l_468 = &g_441;
    struct S0 *l_469 = &g_387;
    int i, j;
    for (g_217 = 0; (g_217 <= 2); g_217 += 1)
    { 
        int32_t *l_381 = &g_44.f0;
        int32_t l_390[10][4] = {{0xE73530DFL,(-1L),0xE73530DFL,0xA76D4F9EL},{0xE73530DFL,0xA76D4F9EL,0xA76D4F9EL,0xE73530DFL},{1L,0xA76D4F9EL,0L,0xA76D4F9EL},{0xA76D4F9EL,(-1L),0L,0L},{1L,1L,0xA76D4F9EL,0L},{0xE73530DFL,(-1L),0xE73530DFL,0xA76D4F9EL},{0xE73530DFL,0xA76D4F9EL,0xA76D4F9EL,0xE73530DFL},{1L,0xA76D4F9EL,0L,0xA76D4F9EL},{0xA76D4F9EL,(-1L),0L,0L},{1L,1L,0xA76D4F9EL,0L}};
        union U1 **l_443[7] = {(void*)0,&g_180[0],(void*)0,(void*)0,&g_180[0],(void*)0,(void*)0};
        int i, j;
        (*l_381) ^= (g_160.f1 >= 8UL);
        if (((safe_add_func_uint32_t_u_u((l_384[2][1] && (safe_mod_func_uint32_t_u_u(((-7L) || (*g_297)), 0x08A46075L))), (((*l_381) = p_78) == 4294967294UL))) & p_78))
        { 
            (*g_388) = g_387;
            (*l_381) ^= (l_389 = 0xDEE4A86CL);
        }
        else
        { 
            int32_t *l_392 = &l_391;
            int32_t *l_393 = &l_391;
            int32_t *l_394 = &l_391;
            int32_t *l_395 = &g_44.f0;
            int32_t *l_396 = &l_390[0][0];
            int32_t *l_397 = &l_390[0][3];
            int32_t *l_398 = &l_391;
            int32_t *l_400 = (void*)0;
            int32_t *l_401 = &g_44.f0;
            int32_t *l_402 = (void*)0;
            int32_t *l_403 = (void*)0;
            int32_t *l_404 = (void*)0;
            int32_t *l_405 = &l_389;
            int32_t *l_406 = &g_44.f0;
            int32_t *l_407[2][8] = {{&g_4,&l_399[6][3],&g_4,&l_399[6][3],&g_4,&l_399[6][3],&g_4,&l_399[6][3]},{&g_4,&l_399[6][3],&g_4,&l_399[6][3],&g_4,&l_399[6][3],&g_4,&l_399[6][3]}};
            uint64_t l_410 = 1UL;
            int i, j;
            l_410++;
        }
        for (g_258.f0 = 0; (g_258.f0 <= 2); g_258.f0 += 1)
        { 
            uint16_t *l_413 = &g_140;
            int32_t l_419 = 0x7888637AL;
            uint32_t l_422[7] = {0x8E4BD243L,4294967288UL,4294967288UL,0x8E4BD243L,4294967288UL,4294967288UL,0x8E4BD243L};
            int i, j;
            for (g_74 = 5; (g_74 >= 0); g_74 -= 1)
            { 
                return l_409[1][3];
            }
            if (p_78)
                continue;
            for (l_408 = 0; (l_408 <= 2); l_408 += 1)
            { 
                uint16_t l_439 = 65535UL;
                union U1 ***l_442[10][1];
                uint64_t *l_460[8][5] = {{&g_154,&g_154,&g_154,&g_154,&g_154},{&g_154,&g_154,&g_154,&g_154,&g_154},{&g_154,&g_154,&g_154,(void*)0,&g_154},{&g_154,&g_154,&g_154,&g_154,&g_154},{&g_154,(void*)0,&g_154,&g_154,(void*)0},{&g_154,&g_154,&g_154,&g_154,&g_154},{&g_154,&g_154,&g_154,&g_154,&g_154},{(void*)0,&g_154,&g_154,&g_154,(void*)0}};
                int32_t *l_461 = &l_390[8][0];
                int i, j;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_442[i][j] = (void*)0;
                }
                for (g_100 = 0; (g_100 <= 5); g_100 += 1)
                { 
                    uint32_t l_432 = 0x2ADF35EBL;
                    uint8_t *l_440 = &g_441;
                    if (((void*)0 == l_413))
                    { 
                        int32_t *l_418[6] = {&l_390[0][0],&l_390[0][0],&l_390[0][0],&l_390[0][0],&l_390[0][0],&l_390[0][0]};
                        int i;
                        l_389 ^= ((*l_381) = (safe_lshift_func_int8_t_s_u((safe_sub_func_uint64_t_u_u(p_78, ((*l_381) ^ 0xD199B151L))), 4)));
                        l_419 = ((*l_381) = (g_100 == p_78));
                    }
                    else
                    { 
                        int32_t l_420 = 2L;
                        int32_t *l_421 = &g_44.f0;
                        int i, j;
                        l_422[3]--;
                        (*g_168) = p_76;
                        (*l_421) |= p_78;
                        (*g_168) = p_76;
                    }
                    (*g_429) = l_425;
                    (*l_381) |= (safe_div_func_int16_t_s_s(l_432, (safe_rshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s(((safe_rshift_func_int16_t_s_s(l_439, 0)) == 253UL), (0UL < (p_78 | 0UL)))), ((*l_440) |= (*g_297))))));
                    for (g_126 = 0; (g_126 <= 5); g_126 += 1)
                    { 
                        return (*g_297);
                    }
                }
                (*l_461) ^= ((&g_180[3] == (l_443[3] = &g_180[0])) >= (safe_lshift_func_int16_t_s_u(((*l_381) || ((*g_168) == (*g_315))), ((++(*l_413)) > (((g_154 ^= ((safe_sub_func_uint8_t_u_u((safe_div_func_uint8_t_u_u(0xACL, (((l_399[5][3] = (safe_unary_minus_func_int8_t_s(p_78))) ^ (safe_mul_func_uint8_t_u_u(((safe_unary_minus_func_int16_t_s(p_78)) > ((safe_lshift_func_uint16_t_u_u(65530UL, (*l_381))) & l_409[1][2])), g_44.f0))) | (-1L)))), p_78)) && l_419)) | 0x649FF226455D9A9FLL) > 8UL)))));
                for (p_78 = 1; (p_78 <= 5); p_78 += 1)
                { 
                    for (g_154 = 0; (g_154 <= 3); g_154 += 1)
                    { 
                        volatile struct S0 *l_463[2][9] = {{&g_158,&g_462,&g_462,&g_158,&g_158,&g_462,&g_462,&g_158,&g_158},{&g_462,(void*)0,&g_462,(void*)0,&g_462,(void*)0,&g_462,(void*)0,&g_462}};
                        int i, j;
                        g_464[1][5][2] = g_462;
                        if (l_399[(g_154 + 2)][(g_217 + 1)])
                            break;
                        if (p_78)
                            break;
                    }
                }
                if ((**g_315))
                    continue;
            }
            if ((*l_381))
                continue;
        }
    }
    for (p_78 = 1; (p_78 <= 6); p_78 += 1)
    { 
        return p_78;
    }
    l_399[3][4] = ((**g_315) < (safe_rshift_func_uint8_t_u_u((p_78 && p_78), ((*l_468) &= ((*g_297) = (l_467 != &g_74))))));
    l_469 = l_469;
    return l_409[1][7];
}



static uint32_t * func_79(int64_t  p_80, int32_t ** p_81, union U1 * p_82, uint64_t  p_83)
{ 
    int32_t l_95[8];
    int32_t *l_96 = (void*)0;
    int32_t *l_97[6] = {&l_95[2],&l_95[2],&g_4,&l_95[2],&l_95[2],&g_4};
    int16_t l_98 = (-1L);
    uint32_t *l_101 = &g_100;
    uint32_t *l_102 = &g_100;
    int8_t *l_109 = (void*)0;
    int32_t **l_173 = (void*)0;
    union U1 *l_177[2];
    uint16_t **l_211 = &g_152[4];
    uint32_t l_229 = 2UL;
    struct S0 *l_257 = &g_258;
    uint16_t l_280[4];
    int8_t l_317[8][10] = {{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L},{0x24L,0x30L,0x24L,0x44L,0x44L,0x24L,0x30L,0x24L,0x44L,0x44L}};
    int i, j;
    for (i = 0; i < 8; i++)
        l_95[i] = 0x3A54D8FDL;
    for (i = 0; i < 2; i++)
        l_177[i] = &g_44;
    for (i = 0; i < 4; i++)
        l_280[i] = 1UL;
    l_98 = (p_80 == (safe_add_func_uint16_t_u_u(l_95[1], p_83)));
    for (p_80 = 0; (p_80 <= 2); p_80 += 1)
    { 
        uint32_t *l_99[4][1][2] = {{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}}};
        int8_t *l_108 = &g_74;
        int32_t l_127 = 0x45B60A5FL;
        uint16_t *l_151 = (void*)0;
        int32_t l_186 = (-1L);
        uint32_t l_266 = 18446744073709551615UL;
        uint32_t l_284[8] = {0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL,0x8A6C8DEDL};
        int32_t l_362 = (-10L);
        uint16_t l_363 = 65531UL;
        int32_t l_367 = (-5L);
        int32_t l_368 = 0x3ACD351BL;
        int32_t l_369 = 0xA7F7F5EDL;
        int32_t l_370 = 0x0F8F43F3L;
        int32_t l_371 = (-6L);
        int64_t l_372 = 0xEEA5F8B7EBC46D2ELL;
        uint32_t l_373 = 0x3443746FL;
        int i, j, k;
    }
    for (g_44.f0 = (-6); (g_44.f0 >= (-21)); g_44.f0 = safe_sub_func_int64_t_s_s(g_44.f0, 9))
    { 
        return &g_100;
    }
    return &g_100;
}


