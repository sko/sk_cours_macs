// Options:   --nomain --no-int8 --no-uint8 --quiet --no-checksum --no-bitfields --no-comma-operators --concise --no-jumps --no-longlong --max-expr-complexity 3 --paranoid -o ./foo3.c

#define NO_LONGLONG

#include "csmith.h"

volatile uint32_t csmith_sink_ = 0;

static long __undefined;


union U0 {
   int32_t  f0;
   volatile int32_t  f1;
   const int16_t  f2;
   const uint32_t  f3;
   uint32_t  f4;
};


static int32_t g_4[2] = {0x822E6E7FL,0x822E6E7FL};
static union U0 g_5[9] = {{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL},{0xAC12265EL}};



static union U0  func_1(void);




static union U0  func_1(void)
{ 
    uint16_t l_2[3][7] = {{0x4019L,0x4019L,0x4019L,0x4019L,0x4019L,0x4019L,0x4019L},{0xD214L,0xD214L,0xD214L,0xD214L,0xD214L,0xD214L,0xD214L},{0x4019L,0x4019L,0x4019L,0x4019L,0x4019L,0x4019L,0x4019L}};
    int32_t *l_3[7] = {&g_4[0],&g_4[0],&g_4[0],&g_4[0],&g_4[0],&g_4[0],&g_4[0]};
    int i, j;
    g_4[0] = l_2[0][5];
    return g_5[6];
}


