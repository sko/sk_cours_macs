// Options:   --nomain --no-int8 --no-uint8 --quiet --no-checksum --no-bitfields --no-comma-operators --concise --no-jumps --no-longlong --max-expr-complexity 3 --paranoid -o ./foo2.c

#define NO_LONGLONG

#include "csmith.h"

volatile uint32_t csmith_sink_ = 0;

static long __undefined;



static uint32_t g_4 = 0xB999FB02L;
static int32_t g_7 = 0xACC5D9B4L;
static int32_t g_29 = 0xFD8C6581L;
static int32_t *g_32 = (void*)0;
static int32_t **g_31 = &g_32;
static int32_t *** volatile g_30 = &g_31;
static uint32_t g_46 = 0x60C63704L;
static int16_t g_57[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static int16_t g_82[2] = {0xAE8EL,0xAE8EL};
static uint16_t g_103[8][2][8] = {{{6UL,1UL,65530UL,0xD98BL,65530UL,1UL,6UL,1UL},{6UL,0xD98BL,0xC50FL,0xD98BL,6UL,0xD965L,6UL,0xD98BL}},{{65530UL,0xD98BL,65530UL,1UL,6UL,1UL,65530UL,0xD98BL},{6UL,1UL,65530UL,0xD98BL,65530UL,1UL,6UL,1UL}},{{6UL,0xD98BL,0xC50FL,1UL,65530UL,0xD98BL,65530UL,1UL},{0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L,0xC50FL,1UL}},{{65530UL,0xD965L,0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L},{65530UL,1UL,6UL,1UL,65530UL,0xD98BL,65530UL,1UL}},{{0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L,0xC50FL,1UL},{65530UL,0xD965L,0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L}},{{65530UL,1UL,6UL,1UL,65530UL,0xD98BL,65530UL,1UL},{0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L,0xC50FL,1UL}},{{65530UL,0xD965L,0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L},{65530UL,1UL,6UL,1UL,65530UL,0xD98BL,65530UL,1UL}},{{0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L,0xC50FL,1UL},{65530UL,0xD965L,0xC50FL,1UL,0xC50FL,0xD965L,65530UL,0xD965L}}};
static int32_t g_108[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static const int32_t *g_132 = &g_108[4];
static int16_t g_161 = (-1L);
static int32_t g_163 = 0x500CCDFAL;
static volatile uint16_t g_247[5] = {8UL,8UL,8UL,8UL,8UL};
static int32_t * volatile g_284 = &g_108[4];
static int32_t ** volatile g_342[9][5] = {{&g_32,&g_32,&g_32,&g_32,&g_32},{(void*)0,&g_32,&g_32,(void*)0,&g_32},{&g_32,&g_32,&g_32,&g_32,&g_32},{&g_32,(void*)0,&g_32,&g_32,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_32,&g_32,&g_32,&g_32,&g_32},{(void*)0,&g_32,&g_32,(void*)0,&g_32},{&g_32,&g_32,&g_32,&g_32,&g_32}};
static volatile uint32_t * volatile * volatile g_399 = (void*)0;
static volatile uint32_t * volatile * volatile * volatile g_398 = &g_399;
static volatile uint32_t * volatile * volatile * volatile * volatile g_397 = &g_398;
static uint32_t *g_403 = &g_46;
static uint32_t **g_402[10] = {&g_403,&g_403,&g_403,&g_403,&g_403,&g_403,&g_403,&g_403,&g_403,&g_403};
static uint32_t *** const  volatile g_401 = &g_402[3];
static uint32_t g_428 = 1UL;
static uint32_t ***g_520 = &g_402[3];
static uint32_t ****g_519[1] = {&g_520};
static int16_t *g_600 = (void*)0;
static int16_t **g_599[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int16_t ** volatile *g_598 = &g_599[0];
static int32_t * volatile g_697 = &g_108[4];
static int32_t * volatile g_699[10] = {&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4],&g_108[4]};
static int16_t g_741[6][8][3] = {{{(-1L),(-1L),1L},{0xB9F8L,0x9F30L,0x83F9L},{1L,0xD219L,1L},{0x9C0AL,0x9C0AL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL}},{{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL},{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL}},{{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL},{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL}},{{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL},{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL}},{{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL},{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL}},{{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL},{0L,0L,(-1L)},{0xCCF4L,(-1L),0x9C0AL},{1L,(-8L),(-1L)},{0xD13EL,0xD13EL,0x9C0AL},{(-8L),1L,(-1L)},{(-1L),0xCCF4L,0x9C0AL}}};
static uint16_t g_745 = 0x1689L;
static int32_t * volatile g_763 = &g_108[4];
static int32_t * volatile g_801 = &g_108[3];
static int32_t * volatile g_820 = &g_29;
static const int32_t ***g_822 = (void*)0;
static int32_t *** volatile g_862 = &g_31;
static const uint32_t g_907[5] = {0x5EE8C235L,0x5EE8C235L,0x5EE8C235L,0x5EE8C235L,0x5EE8C235L};
static uint32_t g_952 = 1UL;
static int32_t ** volatile g_1041 = &g_32;
static int32_t g_1106 = 0L;
static uint16_t g_1121 = 0xB71AL;
static volatile int16_t g_1176 = 0x61C4L;
static const volatile int16_t *g_1175 = &g_1176;
static int16_t * const g_1225[5] = {&g_161,&g_161,&g_161,&g_161,&g_161};
static int32_t ** volatile g_1289 = &g_32;
static int16_t ***g_1292[8][9] = {{(void*)0,&g_599[0],&g_599[0],(void*)0,&g_599[0],&g_599[0],&g_599[9],&g_599[0],(void*)0},{&g_599[6],&g_599[0],&g_599[0],&g_599[5],&g_599[5],&g_599[0],&g_599[0],&g_599[6],&g_599[0]},{&g_599[8],&g_599[5],(void*)0,&g_599[6],&g_599[0],&g_599[0],&g_599[0],&g_599[0],&g_599[6]},{&g_599[6],&g_599[8],&g_599[6],&g_599[0],&g_599[0],&g_599[5],&g_599[9],&g_599[0],&g_599[0]},{&g_599[0],&g_599[8],&g_599[0],&g_599[0],&g_599[0],&g_599[8],&g_599[0],&g_599[9],(void*)0},{&g_599[9],&g_599[5],&g_599[0],&g_599[5],&g_599[5],&g_599[6],&g_599[5],&g_599[5],&g_599[0]},{&g_599[0],&g_599[0],&g_599[8],&g_599[0],&g_599[0],&g_599[0],&g_599[6],&g_599[6],&g_599[6]},{&g_599[0],(void*)0,&g_599[0],&g_599[0],(void*)0,&g_599[0],&g_599[0],&g_599[9],&g_599[0]}};
static int16_t ****g_1291 = &g_1292[5][7];



static int32_t  func_1(void);
static int32_t * func_12(int32_t * p_13, uint32_t  p_14, int32_t * p_15);
static int32_t * func_16(int32_t * p_17);
static int32_t * func_18(int32_t * const  p_19);
static int32_t ** func_20(int32_t ** p_21, int32_t  p_22, int16_t  p_23);
static int32_t  func_38(int32_t  p_39, int16_t  p_40, int32_t  p_41, int32_t * p_42, int32_t  p_43);
static uint16_t  func_58(int32_t  p_59, uint32_t  p_60, int16_t * p_61, int16_t * p_62);
static int32_t * func_67(const int32_t *** p_68);
static uint32_t  func_72(int32_t *** p_73, uint16_t  p_74);
static const int32_t  func_94(int32_t  p_95, uint32_t  p_96, int32_t * p_97, const uint32_t  p_98);




static int32_t  func_1(void)
{ 
    uint32_t l_5 = 0x60D00585L;
    uint16_t l_985 = 2UL;
    int32_t l_1013[6][9][4] = {{{0x64E33CB1L,1L,0xB43100A2L,0x3683C20DL},{0x1F2B185EL,0xE53109A4L,0xA6BDCE99L,0x9E0CDA26L},{(-5L),0x29DF73AEL,0L,7L},{0L,0xD6D6D696L,0x99460102L,0x29DF73AEL},{9L,0xAEC3B820L,8L,(-6L)},{0xE53109A4L,0x76C067DDL,0L,3L},{0xAEC3B820L,0x8868738AL,0x8868738AL,0xAEC3B820L},{0x1F2B185EL,(-5L),0xD6D6D696L,7L},{0x76C067DDL,0xE53109A4L,0x39673371L,2L}},{{0x85CE1AF8L,(-10L),1L,2L},{0xB43100A2L,0xE53109A4L,0x99460102L,7L},{0x29DF73AEL,(-5L),0xF780C120L,0xAEC3B820L},{0L,0x8868738AL,0x9E0CDA26L,3L},{0x01DFE1DCL,0x76C067DDL,0x1F2B185EL,(-6L)},{0x64E33CB1L,0xAEC3B820L,0xFFD67E01L,0x29DF73AEL},{0x76C067DDL,0xD6D6D696L,0x8868738AL,7L},{0x01DFE1DCL,0x29DF73AEL,0x83E7D3FDL,0x9E0CDA26L},{1L,0xE53109A4L,0xF780C120L,0x3683C20DL}},{{0x3683C20DL,1L,8L,0x85CE1AF8L},{0xB43100A2L,0x64E33CB1L,0x9E0CDA26L,0x99460102L},{(-5L),0xFDC8BA63L,0x39673371L,0xAEC3B820L},{0xFDC8BA63L,1L,0xD81FFCC1L,0x29DF73AEL},{0x1F2B185EL,1L,0x1F2B185EL,0x9E0CDA26L},{0x85CE1AF8L,0L,0L,0x9397AE67L},{1L,0xD6D6D696L,0xBBA1C9F5L,0L},{9L,(-5L),0xBBA1C9F5L,(-6L)},{1L,0x64E33CB1L,0L,0xF30F6CD4L}},{{0x85CE1AF8L,0x8868738AL,0x1F2B185EL,0x85CE1AF8L},{0x1F2B185EL,0x85CE1AF8L,0xD81FFCC1L,7L},{0xFDC8BA63L,0L,0x39673371L,7L},{(-5L),(-10L),0x9E0CDA26L,0x9397AE67L},{0xB43100A2L,1L,8L,7L},{0x3683C20DL,0xAEC3B820L,0xF780C120L,(-5L)},{1L,0x8868738AL,0x83E7D3FDL,1L},{0x01DFE1DCL,0xFDC8BA63L,0x8868738AL,(-6L)},{0x76C067DDL,0x85CE1AF8L,0xFFD67E01L,0x3683C20DL}},{{0x64E33CB1L,0xD6D6D696L,0x1F2B185EL,2L},{0x01DFE1DCL,0x3683C20DL,0x9E0CDA26L,0x9E0CDA26L},{0L,0L,0xF780C120L,0L},{0x29DF73AEL,1L,0x99460102L,(-5L)},{0xB43100A2L,0x76C067DDL,1L,0x99460102L},{0x85CE1AF8L,0x76C067DDL,0x29DF73AEL,0x1F2B185EL},{(-10L),0x39673371L,0xAF488512L,0x83E7D3FDL},{9L,0x4AFF3C9BL,1L,0xE4C770F9L},{0x8868738AL,0x9E0CDA26L,3L,0x99460102L}},{{0L,0xAF488512L,2L,0x9E0CDA26L},{0x214DDD5BL,0xA6BDCE99L,(-6L),0x0C396AD4L},{0x4AFF3C9BL,9L,3L,0xD6D6D696L},{0x1F2B185EL,1L,0x74F65B35L,0x1F2B185EL},{9L,0x8868738AL,(-1L),0xBDAC7383L},{7L,1L,0x29DF73AEL,8L},{0x8868738AL,0L,0xF780C120L,0xBBA1C9F5L},{(-1L),0x4AFF3C9BL,0xFFD67E01L,0xBDAC7383L},{0x83E7D3FDL,0xA6BDCE99L,0L,0xA6BDCE99L}}};
    const int32_t **l_1049 = (void*)0;
    const int32_t ***l_1048 = &l_1049;
    uint16_t l_1105 = 0xC4F8L;
    uint32_t l_1119 = 0x676FABF7L;
    uint32_t l_1128 = 0xC4CF0F02L;
    int16_t *l_1177[4][9][7] = {{{&g_741[2][6][1],&g_741[2][6][1],&g_741[2][1][2],&g_161,&g_161,&g_741[2][1][2],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_741[4][1][1],&g_741[2][6][1],&g_82[0],&g_82[0],&g_161},{&g_741[2][6][1],&g_82[1],&g_741[2][1][2],&g_741[1][4][2],&g_161,&g_741[2][6][1],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_57[2],&g_741[2][6][1],&g_82[0],&g_82[0],&g_741[2][6][1]},{&g_741[2][6][1],&g_741[2][6][1],&g_741[2][1][2],&g_161,&g_161,&g_741[2][1][2],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_741[4][1][1],&g_741[2][6][1],&g_82[0],&g_82[0],&g_161},{&g_741[2][6][1],&g_82[1],&g_741[2][1][2],&g_741[1][4][2],&g_161,&g_741[2][6][1],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_57[2],&g_741[2][6][1],&g_82[0],&g_82[0],&g_741[2][6][1]},{&g_741[2][6][1],&g_741[2][6][1],&g_741[2][1][2],&g_161,&g_161,&g_741[2][1][2],&g_741[2][6][1]}},{{&g_82[1],&g_57[2],&g_741[4][1][1],&g_741[2][6][1],&g_82[0],&g_82[0],&g_161},{&g_741[2][6][1],&g_82[1],&g_741[2][1][2],&g_741[1][4][2],&g_161,&g_741[2][6][1],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_57[2],&g_741[2][6][1],&g_82[0],&g_82[0],&g_741[2][6][1]},{&g_741[2][6][1],&g_741[2][6][1],&g_741[2][1][2],&g_161,&g_161,&g_741[2][1][2],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_741[4][1][1],&g_741[2][6][1],&g_82[0],&g_82[0],&g_161},{&g_741[2][6][1],&g_82[1],&g_741[2][1][2],&g_741[1][4][2],&g_161,&g_741[2][6][1],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_57[2],&g_741[2][6][1],&g_82[0],&g_82[0],&g_741[2][6][1]},{&g_741[2][6][1],&g_741[2][6][1],&g_741[2][1][2],&g_161,&g_161,&g_741[2][1][2],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_741[4][1][1],&g_741[2][6][1],&g_82[0],&g_82[0],&g_161}},{{&g_741[2][6][1],&g_82[1],&g_741[2][1][2],&g_741[1][4][2],&g_161,&g_741[2][6][1],&g_741[2][6][1]},{&g_82[1],&g_57[2],&g_57[2],&g_741[2][6][1],&g_82[0],&g_82[0],&g_741[2][6][1]},{&g_741[2][6][1],&g_741[5][2][1],&g_741[1][4][2],&g_741[2][6][1],&g_741[2][6][1],&g_741[1][4][2],&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_57[1]},{(void*)0,&g_741[2][6][1],&g_741[1][4][2],&g_741[2][5][1],&g_741[2][6][1],&g_161,&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_161},{(void*)0,&g_741[5][2][1],&g_741[1][4][2],&g_741[2][6][1],&g_741[2][6][1],&g_741[1][4][2],&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_57[1]},{(void*)0,&g_741[2][6][1],&g_741[1][4][2],&g_741[2][5][1],&g_741[2][6][1],&g_161,&g_741[5][2][1]}},{{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_161},{(void*)0,&g_741[5][2][1],&g_741[1][4][2],&g_741[2][6][1],&g_741[2][6][1],&g_741[1][4][2],&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_57[1]},{(void*)0,&g_741[2][6][1],&g_741[1][4][2],&g_741[2][5][1],&g_741[2][6][1],&g_161,&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_161},{(void*)0,&g_741[5][2][1],&g_741[1][4][2],&g_741[2][6][1],&g_741[2][6][1],&g_741[1][4][2],&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_57[1]},{(void*)0,&g_741[2][6][1],&g_741[1][4][2],&g_741[2][5][1],&g_741[2][6][1],&g_161,&g_741[5][2][1]},{&g_57[2],&g_741[5][6][2],&g_82[0],(void*)0,&g_82[1],&g_741[2][6][1],&g_161}}};
    int16_t l_1197 = 0xF978L;
    const int16_t *l_1203 = (void*)0;
    const int16_t **l_1202 = &l_1203;
    const int16_t ***l_1201 = &l_1202;
    const int16_t ****l_1200 = &l_1201;
    int32_t l_1237 = 0x79F48B4EL;
    int16_t **l_1242 = &l_1177[0][2][4];
    int32_t l_1269 = 1L;
    int32_t l_1270 = 0L;
    int32_t l_1273 = 1L;
    uint16_t l_1282[3][2];
    int32_t l_1311 = 7L;
    int32_t l_1316 = (-9L);
    int32_t l_1330 = (-1L);
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_1282[i][j] = 3UL;
    }
    if ((((safe_sub_func_int32_t_s_s((g_4 && 7L), g_4)) || l_5) ^ g_4))
    { 
        int32_t *l_6[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t ***l_984[5];
        int32_t *l_1068[4];
        int32_t l_1089 = 1L;
        int16_t l_1104 = 0x94BCL;
        int i;
        for (i = 0; i < 5; i++)
            l_984[i] = &g_402[4];
        for (i = 0; i < 4; i++)
            l_1068[i] = &g_29;
        g_7 ^= 0xCB63E8E1L;
        for (l_5 = 16; (l_5 > 34); l_5++)
        { 
            int32_t *l_961 = &g_108[5];
            int32_t l_967 = 1L;
            uint16_t l_979 = 0UL;
            int16_t ***l_988[1];
            int32_t ***l_1018 = &g_31;
            uint32_t l_1046 = 1UL;
            int i;
            for (i = 0; i < 1; i++)
                l_988[i] = &g_599[5];
            for (g_7 = 1; (g_7 != 16); ++g_7)
            { 
                uint32_t l_978[6][8] = {{2UL,6UL,0xBF4C2A9CL,2UL,2UL,0xBF4C2A9CL,6UL,2UL},{0x0B890619L,6UL,4294967291UL,0x0B890619L,2UL,4294967291UL,4294967291UL,2UL},{2UL,4294967291UL,4294967291UL,2UL,0x0B890619L,4294967291UL,6UL,0x0B890619L},{2UL,6UL,0xBF4C2A9CL,2UL,2UL,0xBF4C2A9CL,6UL,2UL},{0x0B890619L,6UL,4294967291UL,0x0B890619L,2UL,4294967291UL,4294967291UL,2UL},{2UL,4294967291UL,4294967291UL,2UL,0x0B890619L,4294967291UL,6UL,0x0B890619L}};
                int32_t l_980[9] = {0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L,0x96C4A275L};
                int i, j;
            }
            (**l_1018) = func_67(l_1048);
            (*g_32) |= (*g_284);
        }
        for (g_745 = 0; (g_745 == 25); ++g_745)
        { 
            int32_t l_1057 = (-1L);
            int32_t l_1076 = (-1L);
            uint32_t l_1086[1][3];
            uint32_t l_1103 = 0xC10060CFL;
            uint16_t *l_1114 = &g_103[5][1][4];
            int32_t *l_1118[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1086[i][j] = 1UL;
            }
            for (g_163 = 10; (g_163 == (-7)); g_163--)
            { 
                uint16_t l_1056 = 0x7F02L;
                int32_t l_1067 = 0x3FDABD05L;
                int32_t *l_1069[10][10] = {{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7,&g_7}};
                int16_t *l_1079 = &g_82[0];
                uint32_t l_1098 = 2UL;
                int i, j;
            }
            for (g_7 = 0; (g_7 <= 4); ++g_7)
            { 
                uint32_t l_1109 = 0xBE00D5B4L;
                ++l_1109;
                for (l_1103 = 1; (l_1103 <= 9); l_1103 += 1)
                { 
                    if ((*g_284))
                        break;
                    for (g_46 = 2; (g_46 <= 9); g_46 += 1)
                    { 
                        int i;
                        if ((*g_284))
                            break;
                        g_699[l_1103] = (**g_862);
                    }
                }
                (*g_697) = 0xBB0B4CA2L;
            }
            if ((safe_div_func_uint16_t_u_u((((*l_1114) = (1UL ^ (l_1086[0][0] && l_1076))) == l_1086[0][1]), g_247[4])))
            { 
                int32_t *l_1117 = &l_1076;
                (*g_820) = (l_1013[3][3][1] = ((*g_801) = l_1103));
                if (l_1103)
                    break;
                for (g_29 = 0; (g_29 <= (-1)); g_29--)
                { 
                    l_1118[2] = l_1117;
                }
                (*l_1117) = ((*g_763) = (l_1119 = (*l_1117)));
            }
            else
            { 
                int32_t l_1120 = 1L;
                ++g_1121;
            }
        }
        if (((((safe_lshift_func_int16_t_s_u((((safe_add_func_int16_t_s_s(l_1119, g_741[2][6][1])) & 6L) == 0x2126L), g_46)) >= l_1128) > (-8L)) == l_1013[5][0][3]))
        { 
            int32_t l_1133 = 0x42638733L;
            for (g_4 = 0; (g_4 > 6); ++g_4)
            { 
                int32_t *l_1136[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_1136[i] = &g_7;
                for (l_1105 = (-10); (l_1105 < 22); l_1105 = safe_add_func_int16_t_s_s(l_1105, 4))
                { 
                    int32_t l_1134 = (-1L);
                    if ((*g_763))
                    { 
                        const int32_t l_1135 = 1L;
                        l_1134 |= l_1133;
                        if (l_1135)
                            break;
                    }
                    else
                    { 
                        (*g_31) = l_1136[0];
                        (*g_284) |= ((+((void*)0 != &g_862)) | 0xAED24436L);
                        (*g_31) = (**g_862);
                    }
                    (**g_30) = func_67(g_822);
                }
                for (g_952 = 27; (g_952 <= 30); ++g_952)
                { 
                    uint32_t l_1140 = 0UL;
                    --l_1140;
                }
                if ((*g_801))
                    break;
                l_1013[3][7][0] = (safe_rshift_func_int16_t_s_u(l_1133, 4));
            }
            l_1013[3][3][1] ^= ((safe_lshift_func_int16_t_s_s(l_1133, 14)) < 0L);
        }
        else
        { 
            uint32_t l_1152[1][9][10] = {{{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL},{4294967295UL,4294967293UL,1UL,1UL,4294967293UL,4294967295UL,4294967293UL,1UL,1UL,4294967293UL}}};
            int32_t l_1155 = (-5L);
            int i, j, k;
            for (g_1106 = 0; (g_1106 != 5); ++g_1106)
            { 
                uint32_t **l_1149 = (void*)0;
                int32_t l_1150[10][3] = {{0xE57A6970L,0xE57A6970L,0x4BBDC520L},{0x66E432C4L,0x66E432C4L,0x97687F8CL},{0xE57A6970L,0xE57A6970L,0x4BBDC520L},{0x66E432C4L,0x545ACBEFL,0x66E432C4L},{0x4AADDE6CL,0x4AADDE6CL,0xE57A6970L},{0x545ACBEFL,0x545ACBEFL,0x66E432C4L},{0x4AADDE6CL,0x4AADDE6CL,0xE57A6970L},{0x545ACBEFL,0x545ACBEFL,0x66E432C4L},{0x4AADDE6CL,0x4AADDE6CL,0xE57A6970L},{0x545ACBEFL,0x545ACBEFL,0x66E432C4L}};
                int32_t l_1151 = 0L;
                int i, j;
                l_1150[8][0] = ((*g_398) == l_1149);
                l_1152[0][0][5]++;
                l_1155 = (0UL != (g_907[0] == l_1152[0][0][5]));
            }
            (*g_697) = l_1152[0][0][5];
        }
    }
    else
    { 
        uint32_t l_1156 = 4294967295UL;
        int32_t *l_1158[10][3] = {{(void*)0,(void*)0,(void*)0},{&l_1013[0][8][3],&l_1013[0][8][3],&l_1013[0][8][3]},{(void*)0,(void*)0,(void*)0},{&l_1013[0][8][3],&l_1013[0][8][3],&l_1013[0][8][3]},{(void*)0,(void*)0,(void*)0},{&l_1013[0][8][3],&l_1013[0][8][3],&l_1013[0][8][3]},{(void*)0,(void*)0,(void*)0},{&l_1013[0][8][3],&l_1013[0][8][3],&l_1013[0][8][3]},{(void*)0,(void*)0,(void*)0},{&l_1013[0][8][3],&l_1013[0][8][3],&l_1013[0][8][3]}};
        int32_t **l_1159 = &l_1158[0][1];
        int32_t l_1191 = 0xF94FDBF8L;
        uint16_t *l_1194 = &g_745;
        uint16_t *l_1204 = (void*)0;
        uint16_t *l_1205 = (void*)0;
        uint16_t *l_1206 = &g_103[7][0][6];
        uint32_t l_1229 = 0xA677A32AL;
        uint32_t l_1245 = 0x39C617BCL;
        uint32_t l_1266[5][8][6] = {{{0UL,0x0BB3444FL,0xEC908D77L,0x32B654DEL,0x95425E1BL,0xB8A61AA9L},{4294967287UL,4294967295UL,0x8E78B79FL,0x8E78B79FL,4294967295UL,4294967287UL},{0xC048D121L,0UL,0x695B5E03L,8UL,1UL,0xB2425265L},{4294967294UL,0xB8A61AA9L,0x6050DC25L,0x6ABA9432L,0xFFAF9284L,0x60A006D7L},{4294967294UL,0x32B654DEL,0x6ABA9432L,8UL,0xB7B021D6L,0x9D0569B7L},{0xC048D121L,1UL,0UL,0x8E78B79FL,0x7D224D3AL,4294967287UL},{4294967287UL,4294967286UL,0x20B57FF0L,0x32B654DEL,4294967286UL,0x8E392EEEL},{0UL,1UL,4294967295UL,0xCB91AA7EL,4294967294UL,0UL}},{{0xAF5DCFFBL,0x695B5E03L,0x4EE877DBL,0x254EBAABL,0xB901BF85L,0xC048D121L},{1UL,0x1AC9318DL,0x3CECBCA1L,0xC74A1D09L,6UL,0xB7B021D6L},{0xFFAF9284L,4294967295UL,2UL,0x61B5321CL,0UL,0x22CFD62AL},{0xEE9808A7L,0xA0AB3C8EL,0xADD5E73AL,3UL,0xC8F0D7F8L,0x317C07C8L},{0x1AC9318DL,0xDE3CAAA4L,0xC8F0D7F8L,0x925F3D02L,0xDB89D4BAL,1UL},{0x6050DC25L,0UL,4294967287UL,0x1AC9318DL,0x254EBAABL,0x61B5321CL},{0x61B5321CL,0x3CECBCA1L,0x04E13332L,0xC274A0B2L,9UL,1UL},{0x317C07C8L,4294967293UL,4294967286UL,4294967288UL,0UL,0xF8D0961BL}},{{0xB60B08E8L,4294967286UL,4294967286UL,4294967286UL,0xB60B08E8L,0xADD5E73AL},{0xC274A0B2L,4294967295UL,0x8E392EEEL,0xC8F0D7F8L,0x3CECBCA1L,0x6050DC25L},{0x695B5E03L,0xCB91AA7EL,0x317C07C8L,4294967295UL,0x9D0569B7L,0x6050DC25L},{8UL,9UL,0x8E392EEEL,0x95425E1BL,0UL,0xADD5E73AL},{0x9D0569B7L,0x61B5321CL,4294967286UL,1UL,0xB8A61AA9L,0xF8D0961BL},{0xB901BF85L,1UL,4294967286UL,0x18BA0D12L,0x71E79E17L,1UL},{4294967295UL,8UL,0x04E13332L,0UL,1UL,0x61B5321CL},{0UL,0xB4DBB7E5L,4294967287UL,0xDB89D4BAL,0x3C20DF81L,4294967286UL}},{{1UL,0x317C07C8L,0xEC908D77L,6UL,0UL,2UL},{4294967295UL,0xDE3CAAA4L,0UL,0x695B5E03L,0x95425E1BL,6UL},{0UL,0xEC908D77L,0xB901BF85L,0xCB91AA7EL,3UL,0x9D0569B7L},{0xB901BF85L,4294967293UL,0xEE9808A7L,0x3CECBCA1L,0UL,0xC5AA0857L},{4294967293UL,0xC335EFBEL,4294967295UL,0x3C20DF81L,0UL,0x317C07C8L},{9UL,0xB60B08E8L,0xF8D0961BL,1UL,4294967287UL,0x8E78B79FL},{0xEC908D77L,0UL,1UL,0xC18503C5L,4294967294UL,4294967294UL},{0x8E78B79FL,0x3DA327E7L,0x3DA327E7L,0x8E78B79FL,1UL,0xDE3CAAA4L}},{{0x695B5E03L,4294967286UL,0UL,0x60A006D7L,1UL,0x4EE877DBL},{0x6050DC25L,0x0BB3444FL,0UL,0xADD5E73AL,1UL,0x3C20DF81L},{0x6ABA9432L,4294967286UL,0x925F3D02L,2UL,1UL,4294967288UL},{0UL,0x3DA327E7L,0x254EBAABL,4294967295UL,4294967294UL,0x6ABA9432L},{0x20B57FF0L,0UL,0x695B5E03L,0UL,4294967287UL,0xC74A1D09L},{4294967295UL,0xB60B08E8L,0xA0AB3C8EL,4294967295UL,0UL,1UL},{0x4EE877DBL,0xC335EFBEL,4294967295UL,0x6ABA9432L,0UL,0x254EBAABL},{0x3CECBCA1L,4294967293UL,0UL,0x317C07C8L,3UL,0xB901BF85L}}};
        int32_t l_1279[1];
        uint32_t l_1285 = 0x60C7FDFFL;
        int32_t l_1308[2];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1279[i] = (-6L);
        for (i = 0; i < 2; i++)
            l_1308[i] = 0xBBB11E66L;
        l_1013[5][6][2] = (l_1156 >= ((~(0UL >= l_1156)) != l_1156));
        (*l_1159) = ((*g_31) = &l_1013[3][3][1]);
        if ((*g_763))
        { 
            return g_1106;
        }
        else
        { 
            int32_t l_1165[4];
            int32_t l_1183 = 0xEF248794L;
            int i;
            for (i = 0; i < 4; i++)
                l_1165[i] = 0xEA0C9554L;
            for (l_1119 = 0; (l_1119 >= 40); l_1119 = safe_add_func_int32_t_s_s(l_1119, 3))
            { 
                int16_t **l_1164 = (void*)0;
                int32_t l_1170 = 0L;
            }
            for (l_1183 = (-1); (l_1183 != (-20)); l_1183 = safe_sub_func_uint32_t_u_u(l_1183, 2))
            { 
                for (g_7 = 22; (g_7 <= (-20)); --g_7)
                { 
                    if (l_1191)
                        break;
                }
            }
        }
        if (((safe_sub_func_uint16_t_u_u(((*l_1194)--), ((*l_1206) = ((l_1197 > ((((safe_add_func_uint32_t_u_u((&g_598 == l_1200), (*g_403))) < (*g_820)) ^ 0x4BE0FF29L) != 65535UL)) >= (**g_31))))) || 0x91F8BBA2L))
        { 
            int32_t *l_1210 = &g_108[4];
            (*l_1159) = l_1210;
            (**g_862) = l_1210;
        }
        else
        { 
            int16_t l_1213 = 0x382BL;
            int32_t *l_1219[6][6] = {{&g_108[4],&l_1013[3][3][1],&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&l_1013[3][3][1],&g_108[4],&g_108[4]},{&g_7,&l_1013[3][3][1],&l_1013[3][3][1],&g_7,&g_108[4],&g_7},{&g_108[4],&g_7,&l_1013[3][3][1],&l_1013[3][3][1],&g_7,&g_108[4]},{&g_108[4],&l_1013[3][3][1],&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&l_1013[3][3][1],&g_108[4],&g_108[4]}};
            uint32_t l_1267 = 3UL;
            int16_t l_1315 = 0x14F3L;
            uint16_t l_1327 = 65535UL;
            int i, j;
            for (l_1191 = 24; (l_1191 == 17); l_1191 = safe_sub_func_uint32_t_u_u(l_1191, 5))
            { 
                uint32_t l_1216 = 4294967289UL;
                int32_t l_1223 = 0xE3C678D4L;
                int16_t *l_1224 = (void*)0;
                int32_t l_1243 = 0xB5311DDCL;
                int32_t l_1256 = (-1L);
            }
            (**g_31) = 1L;
            if (((safe_mod_func_int32_t_s_s((safe_rshift_func_int16_t_s_s(((safe_mod_func_int32_t_s_s((*g_132), (safe_sub_func_int16_t_s_s((g_907[0] & g_741[1][5][0]), (*g_1175))))) ^ l_1266[2][7][5]), l_1267)), 0xEDF517B5L)) || 0x1FCE4CD3L))
            { 
                int32_t l_1268 = 8L;
                int32_t l_1271 = 0xE9E3011EL;
                int32_t l_1272 = 0L;
                int32_t l_1274 = (-7L);
                int32_t l_1275 = 0x61F7F4C2L;
                int32_t l_1276 = 0xBA6540E7L;
                int32_t l_1277 = 0xC22E3E95L;
                int32_t l_1278 = 1L;
                int32_t l_1280 = (-1L);
                int32_t l_1281 = 0x37DE6287L;
                uint16_t l_1317 = 0x0D29L;
                --l_1282[1][0];
                for (l_1269 = 0; (l_1269 <= 2); l_1269 += 1)
                { 
                    int32_t *l_1307 = &l_1277;
                    int32_t l_1314 = 0xDAD1D0D6L;
                    int i;
                    for (l_1268 = 4; (l_1268 >= 0); l_1268 -= 1)
                    { 
                        int32_t * const l_1288 = &l_1279[0];
                        int16_t *****l_1293 = &g_1291;
                        (**g_30) = (void*)0;
                        --l_1285;
                        (*g_1289) = l_1288;
                        (*l_1288) = (+(((*l_1293) = g_1291) != &g_598));
                    }
                    if (g_247[l_1269])
                        continue;
                    for (l_1276 = 4; (l_1276 >= 1); l_1276 -= 1)
                    { 
                        uint32_t l_1294[10] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
                        int i;
                        l_1294[0] = (((((void*)0 == (*g_1289)) ^ 4294967295UL) || g_247[l_1269]) && g_247[l_1269]);
                        l_1273 = (safe_div_func_int16_t_s_s(((*l_1307) = ((safe_add_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((((*l_1194) = g_103[7][0][6]) & (safe_mod_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_u((((*g_403)--) && ((l_1307 == (*l_1159)) ^ (*l_1307))), 1)) == (-9L)), 0x0C286C3AL))), l_1271)), 0xA30BC938L)) | l_1277)), 0x3AA5L));
                    }
                    (*g_31) = &l_1278;
                    for (g_163 = 0; (g_163 <= 2); g_163 += 1)
                    { 
                        int32_t l_1309 = 0x6F7C446AL;
                        int32_t l_1310 = 1L;
                        int32_t l_1312 = 1L;
                        int32_t l_1313[5] = {0x1F74DF9CL,0x1F74DF9CL,0x1F74DF9CL,0x1F74DF9CL,0x1F74DF9CL};
                        int i;
                        l_1317++;
                        if ((*l_1307))
                            continue;
                    }
                }
                return g_247[4];
            }
            else
            { 
                uint32_t l_1320 = 1UL;
                l_1320--;
                (**g_31) |= (+(*g_403));
                (**g_31) ^= 0L;
            }
            for (l_1105 = 0; (l_1105 <= 26); l_1105++)
            { 
                uint32_t ** const *l_1326 = (void*)0;
                (*g_32) = (l_1326 != (void*)0);
                ++l_1327;
            }
        }
    }
    return l_1330;
}



static int32_t * func_12(int32_t * p_13, uint32_t  p_14, int32_t * p_15)
{ 
    uint32_t l_810[2];
    int32_t *l_821 = &g_108[4];
    int32_t *l_825[9] = {&g_29,&g_29,&g_7,&g_29,&g_29,&g_7,&g_29,&g_29,&g_7};
    uint32_t l_832 = 4294967287UL;
    uint32_t l_857 = 4294967295UL;
    uint32_t **l_868 = &g_403;
    uint16_t *l_881 = (void*)0;
    uint32_t l_887 = 4294967288UL;
    uint32_t l_956 = 0x9844CBD8L;
    int i;
    for (i = 0; i < 2; i++)
        l_810[i] = 0xB4367371L;
    if ((safe_add_func_uint16_t_u_u(0x908DL, l_810[0])))
    { 
        int32_t *l_811 = &g_108[2];
        int32_t **l_823 = (void*)0;
        int32_t **l_824 = &l_821;
        (*l_811) = 0xD8900314L;
        (*g_820) = ((*l_811) ^= (safe_rshift_func_int16_t_s_u(p_14, (safe_add_func_int32_t_s_s((safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s(((l_810[0] || 4294967292UL) >= l_810[0]), 6)), 0x8C6B6103L)), (*p_15))))));
        l_821 = p_15;
        (*l_824) = func_67(g_822);
    }
    else
    { 
        int32_t **l_826[4] = {&l_825[4],&l_825[4],&l_825[4],&l_825[4]};
        uint32_t l_838[1][8][4] = {{{4294967295UL,4294967295UL,0xE82A09B8L,0xE9F8604EL},{0x852D66ECL,0xB5E707AEL,0x852D66ECL,0xE82A09B8L},{0x852D66ECL,0xE82A09B8L,0xE82A09B8L,0x852D66ECL},{4294967295UL,0xE82A09B8L,0xE9F8604EL,0xE82A09B8L},{0xE82A09B8L,0xB5E707AEL,0xE9F8604EL,0xE9F8604EL},{4294967295UL,4294967295UL,0xE82A09B8L,0xE9F8604EL},{0x852D66ECL,0xB5E707AEL,0x852D66ECL,0xE82A09B8L},{0xE9F8604EL,0x852D66ECL,0x852D66ECL,0xE9F8604EL}}};
        uint16_t l_869 = 0xD1CDL;
        int i, j, k;
        p_13 = l_825[4];
        l_832 = ((*l_821) = (safe_lshift_func_uint16_t_u_s(((!(*p_13)) < ((safe_mul_func_int16_t_s_s(((((*p_15) & (*g_801)) | p_14) && 1L), 0x1400L)) < p_14)), 12)));
        for (l_832 = 22; (l_832 != 37); l_832++)
        { 
            int32_t l_837 = (-1L);
            uint16_t *l_886 = &g_745;
            int32_t l_898 = 1L;
            int32_t l_910[4] = {1L,1L,1L,1L};
            int i;
        }
        (*g_801) |= (*p_13);
    }
    return p_15;
}



static int32_t * func_16(int32_t * p_17)
{ 
    uint32_t l_44 = 0xEAA473ECL;
    uint32_t *l_45 = &g_46;
    int32_t *l_299 = (void*)0;
    int32_t l_311 = (-3L);
    int32_t l_314 = (-8L);
    int32_t l_316 = 0x89EF7AD8L;
    uint32_t **l_364 = &l_45;
    uint32_t ** const *l_363 = &l_364;
    uint32_t ** const **l_362 = &l_363;
    int16_t *l_391 = &g_82[0];
    int16_t **l_390 = &l_391;
    uint32_t l_418 = 0x1C537931L;
    int16_t l_466 = 1L;
    int16_t l_469 = 8L;
    int32_t l_530 = 0xCA9A1090L;
    int32_t l_531 = 0xF9F944C4L;
    int32_t l_543 = 1L;
    const int16_t l_602[9] = {0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L,0xB8F8L};
    const uint32_t l_603 = 4294967291UL;
    int32_t ** const *l_710 = &g_31;
    int32_t l_750 = 1L;
    int32_t l_751[4][5] = {{0x46CA77A7L,0L,0x46CA77A7L,0L,0x46CA77A7L},{0xD4A91918L,0xD4A91918L,(-1L),(-1L),0xD4A91918L},{2L,0L,2L,0L,2L},{0xD4A91918L,(-1L),(-1L),0xD4A91918L,0xD4A91918L}};
    int32_t l_761 = 0xD26E0501L;
    int i, j;
    if ((safe_rshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s(func_38((l_44 > (++(*l_45))), l_44, l_44, l_45, l_44), g_7)), l_44)))
    { 
        int16_t l_265 = 6L;
        uint16_t *l_270[8][8] = {{&g_103[7][0][6],&g_103[1][0][3],&g_103[1][0][3],&g_103[7][0][6],&g_103[7][0][6],&g_103[5][0][3],&g_103[7][0][6],&g_103[7][0][6]},{&g_103[7][0][6],&g_103[7][0][6],(void*)0,&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6]},{(void*)0,&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],&g_103[5][0][3],&g_103[5][0][3],&g_103[7][0][6],&g_103[7][0][6]},{&g_103[1][0][3],&g_103[1][0][3],&g_103[7][0][6],&g_103[7][0][6],&g_103[5][0][3],&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6]},{(void*)0,&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[1][0][3],&g_103[7][0][6]},{&g_103[7][0][6],&g_103[5][0][3],&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],&g_103[5][0][3],&g_103[7][0][6]},{&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][3],(void*)0,&g_103[1][0][3],&g_103[7][0][6]},{&g_103[7][0][6],&g_103[2][0][4],&g_103[7][0][6],&g_103[7][0][6],&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[7][0][6]}};
        int32_t *l_345 = &g_108[5];
        int32_t ***l_347 = &g_31;
        int32_t ****l_346 = &l_347;
        const int32_t **l_368 = &g_132;
        const int32_t ***l_367[1];
        int32_t l_413 = (-7L);
        int32_t l_416[3];
        uint32_t **l_425[4] = {&l_45,&l_45,&l_45,&l_45};
        int32_t l_467[1];
        int16_t l_472 = 0x13F0L;
        uint32_t l_473 = 0xB7FAC2B0L;
        uint16_t l_658 = 0x771AL;
        int16_t l_659 = 1L;
        uint16_t l_686 = 1UL;
        int i, j;
        for (i = 0; i < 1; i++)
            l_367[i] = &l_368;
        for (i = 0; i < 3; i++)
            l_416[i] = 0x1B4413D1L;
        for (i = 0; i < 1; i++)
            l_467[i] = (-1L);
        l_265 = (safe_rshift_func_uint16_t_u_u(0UL, (((void*)0 != &g_82[0]) && 0xD4735693L)));
        for (g_29 = 0; (g_29 != 23); ++g_29)
        { 
            uint32_t l_286 = 0xBD8A15FCL;
            int32_t l_313 = 0x74596741L;
            int32_t l_315 = 3L;
            uint32_t l_317[1];
            int32_t *l_337 = &g_108[0];
            int i;
            for (i = 0; i < 1; i++)
                l_317[i] = 4294967293UL;
            for (g_161 = (-3); (g_161 >= (-17)); --g_161)
            { 
                int32_t *l_271 = &g_108[4];
                int32_t **l_272 = &l_271;
                (*l_271) |= (((l_270[2][7] != &g_103[4][0][5]) | (-9L)) > l_44);
                (*l_272) = (void*)0;
                (*g_284) = (safe_sub_func_uint32_t_u_u((((*l_45)--) >= (~(safe_div_func_int32_t_s_s(0xF4E4425EL, (safe_sub_func_int16_t_s_s((safe_sub_func_int16_t_s_s((-3L), l_265)), 0x65E2L)))))), (*g_132)));
            }
            for (g_161 = 0; (g_161 <= 4); g_161 += 1)
            { 
                int32_t *l_285[1][9] = {{&g_108[2],&g_108[2],&g_108[2],&g_108[2],&g_108[2],&g_108[2],&g_108[2],&g_108[2],&g_108[2]}};
                int i, j;
                return l_285[0][8];
            }
            if ((l_286 != (((safe_unary_minus_func_int32_t_s(l_265)) && l_265) && l_265)))
            { 
                (*g_284) = l_44;
                if (l_286)
                { 
                    uint16_t l_294 = 0x54E2L;
                    int32_t l_295 = (-10L);
                    l_295 = ((safe_lshift_func_uint16_t_u_u(((((safe_mod_func_uint32_t_u_u((((*l_45) = ((safe_add_func_uint16_t_u_u(65528UL, l_294)) <= l_265)) != g_103[5][0][4]), l_265)) && l_265) | (-4L)) & g_57[2]), 0)) != l_286);
                    p_17 = (void*)0;
                }
                else
                { 
                    int32_t **l_296 = &g_32;
                    (*l_296) = p_17;
                }
                for (g_4 = (-4); (g_4 < 6); g_4 = safe_add_func_int16_t_s_s(g_4, 8))
                { 
                    return p_17;
                }
            }
            else
            { 
                int32_t **l_300 = &g_32;
                (*l_300) = (l_299 = p_17);
            }
            for (g_4 = 0; (g_4 > 16); ++g_4)
            { 
                int32_t *l_303 = &g_108[5];
                int32_t *l_304 = (void*)0;
                int32_t *l_305 = &g_108[2];
                int32_t *l_306 = &g_108[0];
                int32_t l_307 = (-1L);
                int32_t *l_308 = &g_108[1];
                int32_t l_309 = 0x7A934EFDL;
                int32_t *l_310 = &g_108[4];
                int32_t *l_312[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i;
                l_317[0]--;
                for (l_311 = 0; (l_311 == 27); l_311 = safe_add_func_int16_t_s_s(l_311, 5))
                { 
                    int16_t l_326 = 1L;
                    for (l_307 = 0; (l_307 <= 1); l_307 += 1)
                    { 
                        int i;
                        (*l_310) = (safe_rshift_func_int16_t_s_s((g_57[(l_307 + 6)] = (safe_mul_func_uint16_t_u_u((((0xA18CL > l_265) | g_247[1]) || 0L), l_326))), (*l_306)));
                        if (l_317[0])
                            break;
                    }
                }
                for (g_46 = 0; (g_46 != 5); g_46 = safe_add_func_int32_t_s_s(g_46, 7))
                { 
                    uint16_t l_335[10][7][2] = {{{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL}},{{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL}},{{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL}},{{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL}},{{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,65535UL},{0x029FL,0x571DL},{0x029FL,65535UL},{0x973CL,0x571DL},{0x973CL,65528UL}},{{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL}},{{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL}},{{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL}},{{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL}},{{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL},{0x973CL,65528UL},{0x973CL,0x571DL},{0x710AL,0x571DL}}};
                    int32_t l_341[10] = {(-5L),5L,(-5L),5L,(-5L),5L,(-5L),5L,(-5L),5L};
                    int i, j, k;
                }
            }
        }
        (*l_345) = l_265;
        if (((void*)0 != l_346))
        { 
            uint16_t l_348 = 0x18EDL;
            int32_t l_349 = 0L;
            int32_t l_414 = 0xED6EA74DL;
            int32_t l_415[4][5];
            int32_t * const l_497 = &l_314;
            uint32_t l_514 = 0xD1698D62L;
            int16_t l_544 = 0xD4A9L;
            uint32_t l_567[3];
            int32_t *l_577[8][10][3] = {{{&l_414,&g_108[4],&l_414},{&l_314,&g_108[4],(void*)0},{&l_415[3][4],(void*)0,&l_415[3][1]},{&l_314,&l_316,(void*)0},{&l_414,(void*)0,&l_314},{&l_314,&g_108[4],&l_531},{&l_415[3][4],&g_108[4],&l_415[3][4]},{&l_314,(void*)0,(void*)0},{&l_414,(void*)0,&g_7},{&l_314,&l_316,(void*)0}},{{&l_415[3][4],(void*)0,(void*)0},{&l_314,&l_467[0],&l_531},{&l_414,&g_108[4],&l_414},{&l_314,&g_108[4],(void*)0},{&l_415[3][4],(void*)0,&l_415[3][1]},{&l_314,&l_316,(void*)0},{&l_414,(void*)0,&l_314},{&l_314,&g_108[4],&l_531},{&l_415[3][4],&g_108[4],&l_415[3][4]},{&l_314,(void*)0,(void*)0}},{{&l_414,(void*)0,&g_7},{&l_314,&l_316,(void*)0},{&l_415[3][4],(void*)0,(void*)0},{&l_314,&l_467[0],&l_531},{&l_414,&g_108[4],&l_414},{&l_314,&g_108[4],(void*)0},{&l_415[3][4],(void*)0,&l_415[3][1]},{&l_314,&l_316,(void*)0},{&l_414,(void*)0,&l_314},{&l_314,&g_108[4],&l_531}},{{&l_415[3][4],&g_108[4],&l_415[3][4]},{&l_314,(void*)0,(void*)0},{&l_414,(void*)0,&g_7},{&l_314,&l_316,(void*)0},{&l_415[3][4],(void*)0,(void*)0},{&l_314,&l_467[0],&l_531},{&l_413,&g_29,&l_413},{&g_7,(void*)0,&l_311},{&l_413,(void*)0,&l_311},{&g_7,(void*)0,&l_530}},{{&l_413,&l_413,&l_349},{&g_7,&l_531,&l_316},{&l_413,&g_29,&l_413},{&g_7,&l_314,&l_311},{&l_413,(void*)0,&l_316},{&g_7,(void*)0,&l_530},{&l_413,&l_413,(void*)0},{&g_7,&l_316,&l_316},{&l_413,&g_29,&l_413},{&g_7,(void*)0,&l_311}},{{&l_413,(void*)0,&l_311},{&g_7,(void*)0,&l_530},{&l_413,&l_413,&l_349},{&g_7,&l_531,&l_316},{&l_413,&g_29,&l_413},{&g_7,&l_314,&l_311},{&l_413,(void*)0,&l_316},{&g_7,(void*)0,&l_530},{&l_413,&l_413,(void*)0},{&g_7,&l_316,&l_316}},{{&l_413,&g_29,&l_413},{&g_7,(void*)0,&l_311},{&l_413,(void*)0,&l_311},{&g_7,(void*)0,&l_530},{&l_413,&l_413,&l_349},{&g_7,&l_531,&l_316},{&l_413,&g_29,&l_413},{&g_7,&l_314,&l_311},{&l_413,(void*)0,&l_316},{&g_7,(void*)0,&l_530}},{{&l_413,&l_413,(void*)0},{&g_7,&l_316,&l_316},{&l_413,&g_29,&l_413},{&g_7,(void*)0,&l_311},{&l_413,(void*)0,&l_311},{&g_7,(void*)0,&l_530},{&l_413,&l_413,&l_349},{&g_7,&l_531,&l_316},{&l_413,&g_29,&l_413},{&g_7,&l_314,&l_311}}};
            const uint32_t l_601 = 9UL;
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 5; j++)
                    l_415[i][j] = 0x650FA8E4L;
            }
            for (i = 0; i < 3; i++)
                l_567[i] = 0x76E812EFL;
            if (l_348)
            { 
                int16_t l_354 = 0x5E89L;
                int32_t l_365 = 0xF2161BBAL;
                int32_t l_412 = 1L;
                int32_t l_417 = 0x01E9E20CL;
                uint32_t **l_423 = &l_45;
                if ((((l_349 = 0L) != (((*l_345) = (safe_div_func_int32_t_s_s(((safe_mod_func_uint16_t_u_u((((++(*l_45)) | (safe_add_func_uint16_t_u_u(0x957FL, g_57[1]))) > l_348), l_348)) >= g_163), l_354))) | l_348)) < l_354))
                { 
                    int32_t l_366 = 0L;
                    if ((safe_lshift_func_int16_t_s_u(((+((l_365 ^= ((void*)0 != l_362)) > l_366)) || g_161), l_366)))
                    { 
                        uint32_t l_372 = 4294967295UL;
                        (*l_368) = func_67(l_367[0]);
                        (*l_345) &= (safe_lshift_func_int16_t_s_u((~l_348), (((l_365 == 0xAABBD9F9L) & 0x547EL) == l_372)));
                        (*l_345) = (g_247[0] || l_365);
                        (*l_345) = (safe_div_func_int16_t_s_s((-10L), (safe_mod_func_uint16_t_u_u((safe_unary_minus_func_uint16_t_u((l_366 = (((0L > 0x595CL) & 4L) >= 0xF5DEL)))), g_103[7][0][6]))));
                    }
                    else
                    { 
                        uint16_t l_380 = 0x1964L;
                        int16_t *l_389 = &g_161;
                        int16_t ***l_392 = &l_390;
                        (*l_345) = (safe_div_func_uint16_t_u_u(((l_380 ^ (safe_sub_func_uint16_t_u_u(((~(~(safe_sub_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_u((l_366 != 0x7442L), 0)) >= 1UL), l_348)))) >= (*g_284)), 2L))) || l_349), l_380));
                        (*l_345) = ((((((*l_389) = 0xF4F5L) <= (l_380 & l_380)) == l_365) > l_366) | (*g_132));
                        (*l_392) = l_390;
                    }
                    l_366 = (safe_mul_func_int16_t_s_s(((((*l_345) = 0L) | (safe_mod_func_uint16_t_u_u(((void*)0 == g_397), 0x19D6L))) & l_354), l_354));
                }
                else
                { 
                    uint32_t **l_400 = &l_45;
                    int32_t *l_404 = (void*)0;
                    int32_t *l_405 = &g_29;
                    int32_t *l_406 = &l_316;
                    int32_t *l_407 = &g_108[3];
                    int32_t *l_408 = &l_365;
                    int32_t *l_409 = &g_29;
                    int32_t *l_410 = &g_108[4];
                    int32_t *l_411[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_411[i] = &l_349;
                    (*g_401) = l_400;
                    l_404 = (void*)0;
                    ++l_418;
                    for (l_412 = 0; (l_412 <= 8); ++l_412)
                    { 
                        uint32_t ***l_424[7] = {&g_402[3],&g_402[3],&g_402[3],&g_402[3],&g_402[3],&g_402[3],&g_402[3]};
                        int i;
                        l_425[2] = l_423;
                        (*l_405) = ((safe_mul_func_uint16_t_u_u((((*l_406) = ((void*)0 == p_17)) & g_428), (*l_407))) != 0UL);
                    }
                }
            }
            else
            { 
                int32_t ***l_433 = (void*)0;
                int32_t *l_434 = &g_29;
                int32_t l_463 = 1L;
                int32_t l_465 = (-4L);
                int32_t l_470 = 4L;
                int32_t l_471 = 0x4AE4D4F3L;
                (*l_434) |= ((*l_345) &= (safe_lshift_func_int16_t_s_s((safe_div_func_uint16_t_u_u((l_433 == l_433), g_103[2][1][7])), l_348)));
                for (g_428 = 0; (g_428 <= 60); g_428 = safe_add_func_uint32_t_u_u(g_428, 3))
                { 
                    const int16_t *l_437[10][5][5] = {{{&g_82[1],&g_57[2],&g_57[5],&g_161,&g_57[2]},{(void*)0,&g_57[2],&g_57[7],&l_265,&g_161},{&g_57[3],&g_57[7],&g_57[8],&g_82[1],&g_82[1]},{&g_161,(void*)0,&g_82[1],&g_161,&g_161},{(void*)0,&g_57[0],(void*)0,(void*)0,&g_57[2]}},{{&g_57[3],&g_82[0],(void*)0,(void*)0,&g_82[1]},{&g_82[1],&l_265,&g_82[1],&g_82[0],&g_57[5]},{&l_265,&g_57[2],&g_57[8],(void*)0,&g_57[2]},{&g_82[1],&g_82[1],&g_57[7],&g_82[1],(void*)0},{&g_57[3],&l_265,&g_57[5],&g_82[1],&g_57[2]}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_57[8]},{(void*)0,&g_57[1],&l_265,&g_82[1],&g_82[1]},{&g_161,&l_265,(void*)0,&g_82[1],&g_57[3]},{&g_57[4],&l_265,(void*)0,&l_265,&g_57[3]},{(void*)0,&g_82[1],(void*)0,(void*)0,&g_82[1]}},{{&g_161,(void*)0,&g_82[1],(void*)0,&g_57[8]},{(void*)0,&l_265,(void*)0,&g_82[1],&g_57[2]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_265},{(void*)0,(void*)0,(void*)0,&g_82[1],&g_57[2]},{(void*)0,&l_265,&l_265,(void*)0,&g_57[3]}},{{&g_161,(void*)0,(void*)0,&g_82[1],(void*)0},{(void*)0,(void*)0,&g_57[3],(void*)0,&g_57[2]},{&g_57[4],(void*)0,(void*)0,&g_82[1],&g_57[8]},{&g_161,(void*)0,(void*)0,(void*)0,&g_57[2]},{(void*)0,&l_265,&g_82[1],(void*)0,&g_57[8]}},{{(void*)0,(void*)0,&l_265,&l_265,&g_57[2]},{&g_161,(void*)0,&l_265,&g_82[1],(void*)0},{&g_161,&l_265,&g_82[1],&g_82[1],&g_57[3]},{&g_82[1],(void*)0,(void*)0,(void*)0,&g_57[2]},{&g_161,&g_82[1],(void*)0,(void*)0,&l_265}},{{&g_161,&l_265,&g_57[3],(void*)0,&g_57[2]},{(void*)0,&l_265,(void*)0,(void*)0,&g_57[8]},{(void*)0,&g_57[1],&l_265,&g_82[1],&g_82[1]},{&g_161,&l_265,(void*)0,&g_82[1],&g_57[3]},{&g_57[4],&l_265,(void*)0,&l_265,&g_57[3]}},{{(void*)0,&g_82[1],(void*)0,(void*)0,&g_82[1]},{&g_161,(void*)0,&g_82[1],(void*)0,&g_57[8]},{(void*)0,&l_265,(void*)0,&g_82[1],&g_57[2]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_265},{(void*)0,(void*)0,(void*)0,&g_82[1],&g_57[2]}},{{(void*)0,&l_265,&l_265,(void*)0,&g_57[3]},{&g_161,(void*)0,(void*)0,&g_82[1],(void*)0},{(void*)0,(void*)0,&g_57[3],(void*)0,&g_57[2]},{&g_57[4],(void*)0,(void*)0,&g_82[1],&g_57[8]},{&g_161,(void*)0,(void*)0,(void*)0,&g_57[2]}},{{(void*)0,&l_265,&g_82[1],(void*)0,&g_57[8]},{(void*)0,(void*)0,&l_265,&l_265,&g_57[2]},{&g_161,(void*)0,&l_265,&g_82[1],(void*)0},{&g_161,&l_265,(void*)0,&l_265,&g_57[1]},{&g_57[5],&g_57[4],&g_161,&g_57[2],&g_57[5]}}};
                    int32_t l_452[7][7][5] = {{{(-10L),(-2L),0L,(-6L),0xC589CC45L},{0x2D918AA1L,0x6D60E2B3L,0xC4623E14L,0xA2A1AF0DL,(-1L)},{1L,0xEDA09387L,0x3A311576L,0x901BFB57L,1L},{(-1L),(-9L),1L,1L,0xC4623E14L},{0x31BCF7D7L,6L,(-1L),0x7A38E729L,8L},{1L,0x4C10918BL,0x2D918AA1L,1L,1L},{0x2417CC15L,0xDA9F92D7L,0x7B2C11F9L,0x543435C7L,1L}},{{4L,0L,1L,1L,(-1L)},{9L,0xC42E87CEL,0L,0x2717A9D4L,0x58047DA7L},{0xC42E87CEL,(-1L),0x923204FEL,(-1L),(-2L)},{3L,0xD8661B11L,0x794B96A3L,0L,(-1L)},{0x3A311576L,0L,5L,0L,1L},{0xE4AB8086L,0x6D60E2B3L,0xC589CC45L,(-1L),8L},{1L,0L,(-10L),0x2717A9D4L,0x7A38E729L}},{{0x2EA2263BL,1L,1L,1L,0L},{(-2L),0x0CDF4D3CL,0x7EA98A6BL,0x543435C7L,0x50847183L},{0x923204FEL,6L,0xE4AB8086L,1L,(-3L)},{1L,0x2717A9D4L,1L,0x7A38E729L,(-9L)},{(-5L),0x0CEE6850L,0x2417CC15L,1L,(-1L)},{1L,0L,1L,0x901BFB57L,0x54F23389L},{0x4C10918BL,0xE8D058AEL,(-1L),0xA2A1AF0DL,(-1L)}},{{3L,3L,0xE8D058AEL,(-6L),0x938E7E73L},{6L,0x7E9DCAC3L,1L,1L,0x2D918AA1L},{0L,0x6D60E2B3L,0L,(-10L),0xB7E426E6L},{5L,0x7E9DCAC3L,0xC42E87CEL,0x575C2917L,0x97BEEA31L},{(-1L),3L,1L,6L,5L},{0L,0xE8D058AEL,0L,0xAE05A20FL,(-2L)},{(-1L),0L,0L,1L,0x31BCF7D7L}},{{6L,0x0CEE6850L,0x80849CD3L,0xCB1DB57FL,0L},{0x80849CD3L,0x2717A9D4L,(-1L),0xE8D058AEL,(-1L)},{0xC4623E14L,6L,0xCCF02EBDL,0xDA9F92D7L,(-10L)},{1L,0x0CDF4D3CL,(-3L),5L,0xCCF02EBDL},{3L,1L,1L,0x2D918AA1L,0L},{0x47F5803CL,0L,1L,0xE4AB8086L,0L},{(-6L),0x6D60E2B3L,0x82B0DEA5L,1L,0xCB1DB57FL}},{{0L,0L,0x4C10918BL,0x0CEE6850L,0xCB1DB57FL},{1L,0xD8661B11L,1L,0x54F23389L,(-8L)},{1L,0x54F23389L,0L,0L,1L},{0L,0x6D60E2B3L,0x2717A9D4L,0x38116876L,(-5L)},{0xD8661B11L,0xA8454A53L,(-6L),1L,0L},{0x2D918AA1L,0x0CDF4D3CL,3L,0x58047DA7L,(-1L)},{0x575C2917L,(-1L),1L,0xE8D058AEL,(-1L)}},{{0L,0L,0x2BBE92ECL,0x47F5803CL,0x7B2C11F9L},{0x1D49E02EL,0xF2314057L,0x54F23389L,0L,(-1L)},{3L,0xCD2F1CDFL,9L,(-6L),(-6L)},{(-6L),0xCB3A5899L,(-6L),0x3A311576L,0L},{0xE4AB8086L,(-1L),0x38116876L,0x794B96A3L,0x7E9DCAC3L},{0xACF0830DL,0xC2AC7893L,8L,0xD8661B11L,0x0CEE6850L},{(-5L),(-9L),0x38116876L,0x7E9DCAC3L,0x923204FEL}}};
                    uint32_t l_478 = 1UL;
                    int i, j, k;
                    if ((&g_82[1] != (l_437[5][4][3] = l_437[8][0][1])))
                    { 
                        (*l_434) = ((*g_284) = (*g_284));
                        (*l_345) |= l_349;
                    }
                    else
                    { 
                        int32_t * const l_438 = (void*)0;
                        (*l_368) = func_18(l_438);
                        (*l_434) = ((*l_434) <= 0x81B8B2BCL);
                    }
                    for (g_163 = 0; (g_163 == 29); ++g_163)
                    { 
                        int32_t *l_453 = &g_29;
                        int32_t *l_454 = &l_452[3][0][3];
                        int32_t *l_455 = &l_452[2][5][3];
                        int32_t *l_456 = &l_414;
                        int32_t *l_457 = &l_349;
                        int32_t *l_458 = &l_316;
                        int32_t *l_459 = &l_311;
                        int32_t *l_460 = &g_29;
                        int32_t *l_461 = &l_349;
                        int32_t *l_462 = &l_415[2][3];
                        int32_t *l_464[2][5] = {{&g_108[1],&g_108[1],&l_349,&g_108[1],&g_108[1]},{&l_414,&g_108[1],&l_414,&l_414,&g_108[1]}};
                        int16_t l_468 = 0xCE23L;
                        int i, j;
                        (*l_345) ^= ((((*l_391) = (safe_sub_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_mod_func_uint32_t_u_u((((((!g_428) > ((++(**l_364)) ^ (safe_div_func_uint16_t_u_u((*l_434), g_103[5][0][2])))) > l_314) && 0xBFC1L) ^ 0xE0759871L), l_349)), 0xD0E2C794L)), l_452[4][5][2]))) < 0xE311L) && l_452[5][3][0]);
                        ++l_473;
                    }
                    for (l_265 = 0; (l_265 >= 0); l_265 -= 1)
                    { 
                        (*l_434) ^= (safe_mul_func_int16_t_s_s((0x5095L | (-9L)), 0xC87DL));
                    }
                    if (l_478)
                        break;
                }
                (*l_368) = p_17;
            }
            if (((safe_mul_func_uint16_t_u_u((safe_mul_func_uint32_t_u_u(l_414, ((*l_345) == 4294967295UL))), g_247[2])) > l_349))
            { 
                uint16_t l_487 = 0xE25FL;
                int32_t l_490 = (-4L);
                int32_t l_492[9][10] = {{0x72C388BDL,0x7F9B7349L,0x72C388BDL,1L,0xE95A4929L,0xE80C4C83L,(-7L),(-9L),(-1L),(-7L)},{(-7L),0xE80C4C83L,0xD48BE78EL,0x6B19010AL,(-9L),0xBC8BBC2BL,0xBC8BBC2BL,(-9L),0x6B19010AL,0xD48BE78EL},{2L,2L,0x72C388BDL,0L,(-1L),0x7F9B7349L,(-1L),0x0F10C9E6L,(-9L),0L},{0xBC8BBC2BL,(-7L),2L,0x23374735L,(-1L),0xE95A4929L,(-1L),0x23374735L,2L,(-7L)},{(-9L),2L,0x6B19010AL,(-7L),0L,1L,0xBC8BBC2BL,0xE95A4929L,0L,0x0F10C9E6L},{0L,0xE80C4C83L,(-1L),(-7L),1L,1L,(-7L),(-1L),0xE80C4C83L,0L},{(-9L),0x7F9B7349L,1L,(-1L),2L,0xE95A4929L,0x6B19010AL,0xE80C4C83L,0x23374735L,0L},{0xBC8BBC2BL,0xD48BE78EL,0x0F10C9E6L,0x7F9B7349L,2L,0x7F9B7349L,0x0F10C9E6L,0xD48BE78EL,0xBC8BBC2BL,0L},{2L,0x6B19010AL,(-7L),0L,1L,0xBC8BBC2BL,0xE95A4929L,0L,0x0F10C9E6L,0x0F10C9E6L}};
                uint32_t l_532 = 0x0BE3693DL;
                int i, j;
                (*l_368) = func_18(p_17);
                for (g_163 = 3; (g_163 > 14); g_163 = safe_add_func_int32_t_s_s(g_163, 1))
                { 
                    uint32_t l_493 = 0xBD8BC8ADL;
                    int32_t *l_496 = &g_29;
                    int32_t l_503 = (-9L);
                    uint32_t l_505 = 4294967295UL;
                    if ((safe_add_func_int16_t_s_s((4294967287UL < l_487), g_46)))
                    { 
                        int32_t *l_488 = &g_108[4];
                        int32_t *l_489 = &l_415[1][2];
                        int32_t *l_491[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        ++l_493;
                        (*l_368) = l_496;
                        (*l_368) = func_18(func_18(l_497));
                        (*l_489) = (safe_div_func_int32_t_s_s(0xA84E4360L, (+(0x51390641L >= (*l_497)))));
                    }
                    else
                    { 
                        return l_496;
                    }
                    for (l_466 = 22; (l_466 <= 20); l_466 = safe_sub_func_int32_t_s_s(l_466, 2))
                    { 
                        int32_t *l_504[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_504[i] = &l_349;
                        l_505++;
                        (*l_345) |= ((g_57[2] &= (safe_sub_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((((*l_496) && ((safe_div_func_int16_t_s_s((*l_497), g_82[0])) & 0x33F3EA73L)) <= g_161), (*g_403))), 0xEF94L))) ^ g_428);
                        if (l_514)
                            continue;
                    }
                    return p_17;
                }
                if ((((l_270[2][7] == (void*)0) | 0x94AAL) || (*l_497)))
                { 
                    (*l_345) &= (safe_rshift_func_uint16_t_u_u(((*g_398) == &g_403), 12));
                    for (g_428 = 1; (g_428 <= 4); g_428 += 1)
                    { 
                        uint32_t *****l_521 = (void*)0;
                        uint32_t *****l_522 = &g_519[0];
                        (*l_497) = (((g_103[7][0][6] = (*l_497)) > (((*l_522) = g_519[0]) == (void*)0)) ^ 9UL);
                    }
                }
                else
                { 
                    int32_t l_525 = (-8L);
                    int32_t l_529 = (-1L);
                    for (g_29 = 21; (g_29 >= (-1)); g_29--)
                    { 
                        int32_t *l_526 = &l_414;
                        int32_t *l_527 = &l_415[3][1];
                        int32_t *l_528[7][6] = {{&l_492[0][3],&l_415[3][1],(void*)0,&g_108[3],&g_108[3],(void*)0},{&l_492[0][3],&l_492[0][3],&g_108[3],&l_414,&l_415[3][0],&l_414},{&l_415[3][1],&l_492[0][3],&l_415[3][1],(void*)0,&g_108[3],&g_108[3]},{&l_314,&l_415[3][1],&l_415[3][1],&l_314,&l_492[0][3],&l_414},{&l_414,&l_415[3][1],&l_492[0][3],&l_415[3][1],(void*)0,&g_108[3]},{&l_415[3][1],(void*)0,&g_108[3],&g_108[3],(void*)0,&l_415[3][1]},{&l_414,&l_415[3][1],&l_415[3][0],(void*)0,&l_415[3][0],&l_415[3][1]}};
                        int i, j;
                        l_532++;
                        (*l_527) |= (!(l_490 | (l_492[0][3] <= 0UL)));
                        (*l_497) |= l_492[0][3];
                        (*l_368) = p_17;
                    }
                }
            }
            else
            { 
                int32_t l_536 = 0L;
                int32_t l_562 = 0xBEBA3B46L;
                int32_t l_563 = 0xBD73ED2BL;
                int32_t l_564 = 1L;
                int32_t l_565 = 0xE6855A4FL;
                (*l_497) = l_536;
                p_17 = func_18(p_17);
                l_536 ^= ((*l_345) >= (safe_mul_func_int16_t_s_s((0x163DD0E6L & (*l_497)), g_7)));
                if (((l_544 &= ((safe_rshift_func_uint16_t_u_u(((*l_497) = g_108[4]), (safe_rshift_func_int16_t_s_u((((((****l_362) = l_348) <= l_543) < (*g_284)) >= l_536), 9)))) ^ g_57[2])) | l_536))
                { 
                    (*l_345) |= ((*l_497) = l_536);
                    for (l_473 = 0; (l_473 >= 56); l_473 = safe_add_func_int32_t_s_s(l_473, 2))
                    { 
                        return p_17;
                    }
                }
                else
                { 
                    int32_t l_549 = 0x3498FE5BL;
                    int32_t l_553 = 0x1BA1EE4CL;
                    int32_t l_566 = (-1L);
                    for (g_29 = 0; (g_29 == (-12)); --g_29)
                    { 
                        int32_t *l_550 = &l_530;
                        int32_t *l_551 = (void*)0;
                        int32_t *l_552 = &l_314;
                        int32_t *l_554 = &g_108[4];
                        int32_t *l_555 = (void*)0;
                        int32_t *l_556 = &l_415[3][3];
                        int32_t *l_557 = &g_108[4];
                        int32_t *l_558 = &l_314;
                        int32_t *l_559 = &l_549;
                        int32_t *l_560 = &l_531;
                        int32_t *l_561[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_561[i] = &l_467[0];
                        (*l_368) = p_17;
                        (*l_345) = ((*l_497) ^= (-1L));
                        --l_567[2];
                        (*l_556) = (safe_lshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_u(7L, 15)) != ((safe_lshift_func_int16_t_s_s(((+(((((void*)0 != l_577[5][4][1]) < l_565) & (*l_559)) & l_549)) <= (*l_552)), 0)) > (*l_497))), g_103[7][0][6]));
                    }
                    if (l_549)
                    { 
                        const uint16_t l_580[9] = {0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL,0x5F3EL};
                        int i;
                        (*l_368) = &l_530;
                        l_553 |= (safe_mod_func_uint16_t_u_u((l_580[4] && (((safe_unary_minus_func_uint16_t_u((safe_sub_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((safe_div_func_uint16_t_u_u(((void*)0 == &g_519[0]), 0xA717L)), l_549)), l_564)))) != (*g_284)) | g_163)), l_549));
                        l_565 ^= l_562;
                    }
                    else
                    { 
                        (*l_345) &= 0x378E5905L;
                    }
                }
            }
            l_577[4][2][0] = &l_415[3][4];
            (*l_345) = (((*l_345) < (safe_div_func_uint32_t_u_u((safe_add_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u((((((safe_lshift_func_int16_t_s_s((&l_390 != g_598), (*l_497))) == l_601) <= l_311) >= (*l_497)) ^ 0x94CB6561L), l_602[1])) ^ (*l_497)), g_57[3])), l_603)), (*l_497)))) != l_316);
        }
        else
        { 
            uint32_t l_617 = 0xEBE62804L;
            const uint32_t l_637[3][10] = {{6UL,1UL,1UL,6UL,4UL,6UL,1UL,1UL,6UL,4UL},{6UL,1UL,1UL,6UL,4UL,6UL,1UL,1UL,6UL,4UL},{6UL,1UL,1UL,6UL,4UL,6UL,1UL,1UL,6UL,4UL}};
            int32_t l_669 = (-10L);
            int32_t l_670 = 0x0B90E3A9L;
            int32_t l_671 = (-3L);
            const int32_t ***l_685[4][4] = {{&l_368,&l_368,&l_368,&l_368},{&l_368,&l_368,&l_368,&l_368},{&l_368,&l_368,&l_368,&l_368},{&l_368,&l_368,&l_368,&l_368}};
            int i, j;
            for (g_163 = 19; (g_163 <= (-3)); g_163 = safe_sub_func_uint32_t_u_u(g_163, 2))
            { 
                uint32_t l_628 = 4294967291UL;
                for (l_311 = (-16); (l_311 <= 15); l_311 = safe_add_func_uint16_t_u_u(l_311, 4))
                { 
                    (*l_368) = func_18(p_17);
                }
                for (l_265 = 4; (l_265 < 22); l_265 = safe_add_func_int16_t_s_s(l_265, 1))
                { 
                    int32_t l_610 = 0x3F7C9BBEL;
                    int32_t l_611 = (-1L);
                    int32_t *l_612 = (void*)0;
                    int32_t *l_613 = &l_413;
                    int32_t *l_614 = &l_316;
                    int32_t *l_615 = &g_29;
                    int32_t *l_616[9][7] = {{&g_7,&g_7,(void*)0,&l_314,&l_316,&l_467[0],(void*)0},{&l_316,&g_29,(void*)0,(void*)0,&g_29,&l_316,&l_316},{&l_530,(void*)0,&g_108[4],(void*)0,&l_316,&l_316,(void*)0},{&l_467[0],&g_7,&l_467[0],(void*)0,&l_316,&g_108[2],&l_316},{&g_108[4],(void*)0,&l_530,&l_467[0],&l_530,(void*)0,&g_108[4]},{(void*)0,&g_29,&l_316,&l_316,&g_29,&g_108[2],&g_29},{(void*)0,&g_7,&g_7,(void*)0,&l_314,&l_316,&l_467[0]},{&l_467[0],(void*)0,&l_316,&l_467[0],&l_467[0],&l_316,(void*)0},{&l_314,&g_108[4],&l_530,(void*)0,&g_7,&l_467[0],&l_467[0]}};
                    int16_t *l_623[8][4][4] = {{{&g_57[2],&l_265,(void*)0,&l_265},{&l_265,&l_472,(void*)0,(void*)0},{&g_57[2],&g_57[2],&l_265,(void*)0},{(void*)0,&l_472,(void*)0,&l_265}},{{(void*)0,&l_265,&l_265,(void*)0},{&g_57[2],&l_265,(void*)0,&l_265},{&l_265,&l_472,(void*)0,(void*)0},{&g_57[2],&g_57[2],&l_265,(void*)0}},{{(void*)0,&l_472,(void*)0,&l_265},{(void*)0,&l_265,&l_265,(void*)0},{&g_57[2],&l_265,(void*)0,&l_265},{&l_265,&l_472,(void*)0,(void*)0}},{{&g_57[2],&g_57[2],&l_265,(void*)0},{(void*)0,&l_472,(void*)0,&l_265},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_265,(void*)0,&l_472,(void*)0}},{{(void*)0,&g_57[2],&l_472,&l_472},{&l_265,&l_265,(void*)0,&l_472},{(void*)0,&g_57[2],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_265,(void*)0,&l_472,(void*)0},{(void*)0,&g_57[2],&l_472,&l_472},{&l_265,&l_265,(void*)0,&l_472},{(void*)0,&g_57[2],(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{&l_265,(void*)0,&l_472,(void*)0},{(void*)0,&g_57[2],&l_472,&l_472},{&l_265,&l_265,(void*)0,&l_472}},{{(void*)0,&g_57[2],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_265,(void*)0,&l_472,(void*)0},{(void*)0,&g_57[2],&l_472,&l_472}}};
                    int i, j, k;
                    ++l_617;
                    if ((safe_add_func_int16_t_s_s(((safe_unary_minus_func_int16_t_s(((l_623[2][1][2] == (*l_390)) != 6L))) > g_247[1]), g_161)))
                    { 
                        uint16_t l_624 = 0xD173L;
                        int32_t *l_625 = &l_610;
                        if (l_624)
                            break;
                        (*l_368) = l_625;
                        if ((*l_614))
                            continue;
                    }
                    else
                    { 
                        return p_17;
                    }
                }
                (*l_345) &= (safe_mul_func_uint16_t_u_u(0x0397L, (5UL != l_543)));
                ++l_628;
            }
            for (g_4 = 0; (g_4 <= 3); g_4 += 1)
            { 
                uint32_t l_642[5][2][9] = {{{1UL,1UL,4294967295UL,1UL,4294967295UL,1UL,1UL,0x2A539D08L,0x7302D452L},{0x9D4900C1L,0xD0ED0CD9L,9UL,1UL,4294967293UL,0x45DF33B1L,0x285C6223L,0x9D4900C1L,1UL}},{{6UL,4294967295UL,0x2A539D08L,0x45DF33B1L,0xFE9FEB6EL,9UL,0xFADCEFF7L,0x2A539D08L,0x3DA29361L},{0xFC397D64L,0x2A539D08L,0xDFF6AA03L,6UL,1UL,0xB2C6952CL,0x2A539D08L,7UL,0x2A539D08L}},{{1UL,0x4133D63DL,0xDFF6AA03L,0xDFF6AA03L,0x4133D63DL,1UL,0xFE9FEB6EL,0x7302D452L,1UL},{1UL,1UL,0x2A539D08L,0x7302D452L,4294967295UL,0x773509B1L,0xFC397D64L,1UL,4294967295UL}},{{0x3DA29361L,4294967295UL,9UL,4294967293UL,4294967294UL,0x9D4900C1L,0xFE9FEB6EL,0x3DA29361L,4294967295UL},{6UL,1UL,4294967295UL,0x9D4900C1L,4294967295UL,4294967295UL,0x2A539D08L,0x2A539D08L,4294967295UL}},{{0x773509B1L,1UL,3UL,1UL,0x773509B1L,0xB2C6952CL,0xFADCEFF7L,1UL,1UL},{0x2A539D08L,4294967295UL,4294967295UL,0xDFF6AA03L,0xFE9FEB6EL,6UL,0x285C6223L,4294967295UL,7UL}}};
                int32_t *l_643 = &l_314;
                int32_t ***l_650[7] = {&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31};
                int32_t l_668 = 0x6C259E8DL;
                uint32_t l_684 = 0x40E97CEDL;
                int i, j, k;
                (*l_643) = ((safe_mul_func_int16_t_s_s(((l_467[0] = ((*l_345) = ((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int32_t_s_s(l_637[1][5], ((((safe_mod_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(0x72D5L, l_531)), l_642[3][1][1])) == l_530) == l_642[2][0][1]) & l_637[0][4]))) && l_642[1][1][1]), 1)) && l_637[1][5]))) == 4294967295UL), g_82[0])) & g_4);
                if ((((safe_div_func_int16_t_s_s((safe_add_func_int16_t_s_s(l_617, (safe_add_func_uint16_t_u_u(((*l_346) != l_650[0]), l_617)))), l_617)) | 0x47ACL) && (*g_284)))
                { 
                    int32_t l_657[1][8] = {{0x6908017AL,0x6908017AL,0x2F8AFCF2L,0x6908017AL,0x6908017AL,0x2F8AFCF2L,0x6908017AL,0x6908017AL}};
                    int32_t l_667 = 1L;
                    int i, j;
                    (*l_368) = p_17;
                    if ((*g_284))
                        continue;
                    (*l_643) |= (g_108[4] || (0xBA07L == 4UL));
                    if ((safe_div_func_int32_t_s_s((safe_unary_minus_func_uint32_t_u(0x389418C7L)), ((!((((((safe_sub_func_int32_t_s_s(2L, l_657[0][2])) <= l_658) < l_659) | l_657[0][3]) || l_637[1][5]) | l_657[0][2])) & l_657[0][2]))))
                    { 
                        int16_t l_662 = 1L;
                        (*l_643) ^= (safe_mod_func_int16_t_s_s((l_662 & (safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u((l_667 &= 0xF41BL), g_29)), 1L))), l_668));
                    }
                    else
                    { 
                        uint32_t l_672 = 4294967295UL;
                        l_672--;
                    }
                }
                else
                { 
                    uint32_t l_675 = 0xB1C20998L;
                    for (l_311 = 2; (l_311 >= 0); l_311 -= 1)
                    { 
                        int i;
                        (*l_368) = (void*)0;
                        if (l_416[l_311])
                            break;
                        (*l_345) = l_675;
                    }
                    (*l_345) = ((safe_rshift_func_uint16_t_u_s((safe_mul_func_uint16_t_u_u((((((**l_364)++) | ((safe_add_func_uint16_t_u_u(0x7C36L, ((void*)0 == &g_599[3]))) >= l_675)) > 0x16E6L) & 0xCE98FFB6L), l_675)), 3)) | l_684);
                    p_17 = func_18(func_67(l_685[3][3]));
                    if ((*l_643))
                        continue;
                }
                l_686++;
            }
        }
    }
    else
    { 
        int32_t *l_691 = &g_108[2];
        (*l_691) |= (safe_add_func_int32_t_s_s(((void*)0 != &g_30), l_314));
    }
    if ((safe_mul_func_uint16_t_u_u((safe_add_func_int16_t_s_s(((void*)0 == &g_599[9]), l_44)), g_163)))
    { 
        int32_t l_696 = 0x54E6E36AL;
        int32_t *l_698 = (void*)0;
        int32_t *l_700[9] = {&l_311,&l_311,&l_311,&l_311,&l_311,&l_311,&l_311,&l_311,&l_311};
        int i;
        (*g_697) &= l_696;
        (*g_284) |= ((-9L) == l_696);
    }
    else
    { 
        uint16_t l_701 = 0x630CL;
        int32_t *l_713 = (void*)0;
        int32_t l_732[4];
        uint32_t l_734 = 0x0C8F1A00L;
        int32_t l_752 = 0x819A0368L;
        int32_t l_804 = 0xCCC61B96L;
        int i;
        for (i = 0; i < 4; i++)
            l_732[i] = 5L;
        if (((l_701 >= ((safe_rshift_func_int16_t_s_u((-1L), l_701)) && l_701)) | l_701))
        { 
            for (l_418 = 0; (l_418 > 18); l_418 = safe_add_func_uint32_t_u_u(l_418, 1))
            { 
                int16_t l_706 = 0xBF4AL;
                l_311 &= l_706;
            }
        }
        else
        { 
            int32_t ***l_709 = &g_31;
            int32_t ****l_708 = &l_709;
            int32_t ** const **l_711 = (void*)0;
            int32_t ** const **l_712 = &l_710;
            int32_t l_729[8][1][4] = {{{0x22A4EA11L,(-3L),0x22A4EA11L,(-7L)}},{{0x17576AA3L,0x22A4EA11L,0x602B9674L,0x17576AA3L}},{{(-7L),(-4L),1L,0x22A4EA11L}},{{(-4L),1L,1L,1L}},{{(-7L),(-7L),0x602B9674L,6L}},{{0x17576AA3L,0L,0xE3148E4FL,0x22A4EA11L}},{{0xE3148E4FL,0x22A4EA11L,(-4L),0xE3148E4FL}},{{(-7L),0x22A4EA11L,(-3L),0x22A4EA11L}}};
            int32_t l_742 = 0x086999B3L;
            int32_t l_746 = 0xC151B6EBL;
            int16_t l_753 = 0x3CDAL;
            int32_t l_762 = (-8L);
            int32_t *l_802 = &l_531;
            int32_t *l_803[6];
            uint32_t l_805 = 0x1EFB28A4L;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_803[i] = &l_314;
            if ((((~((((*l_708) = (void*)0) != ((*l_712) = l_710)) >= l_701)) >= l_701) || 0x9D91L))
            { 
                int32_t **l_714 = &g_32;
                (*l_714) = l_713;
            }
            else
            { 
                int32_t l_720 = (-1L);
                int32_t l_726[7] = {0x3D433C66L,0x3D433C66L,0x3D433C66L,0x3D433C66L,0x3D433C66L,0x3D433C66L,0x3D433C66L};
                int16_t l_800[6][1][10] = {{{0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L}},{{(-1L),(-1L),0xB2F8L,(-1L),(-1L),0xB2F8L,(-1L),(-1L),0xB2F8L,(-1L)}},{{(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L)}},{{0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L}},{{(-1L),(-1L),0xB2F8L,(-1L),(-1L),0xB2F8L,(-1L),(-1L),0xB2F8L,(-1L)}},{{(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L),0x9D33L,0x9D33L,(-1L)}}};
                int i, j, k;
                for (l_311 = 1; (l_311 <= 4); l_311 += 1)
                { 
                    const uint16_t l_723 = 65535UL;
                    int32_t *l_727 = &l_314;
                    int32_t *l_728 = &l_530;
                    int32_t *l_730 = &l_729[3][0][0];
                    int32_t *l_731[7][4] = {{&l_726[6],&l_530,&l_726[3],&l_311},{&l_720,&l_720,&l_311,&l_314},{&l_311,&l_314,&g_7,&l_314},{&g_29,&l_720,&g_108[4],&l_311},{&l_726[0],&l_530,&l_314,(void*)0},{&g_108[4],&g_29,&l_726[3],&l_726[3]},{&g_108[4],&g_108[4],&l_314,&l_726[6]}};
                    int16_t l_733 = 0xD162L;
                    uint32_t l_754 = 0xB77ED543L;
                    int i, j;
                    for (l_418 = 0; (l_418 <= 4); l_418 += 1)
                    { 
                        uint16_t *l_717 = &g_103[7][0][6];
                        int32_t l_718 = 0xEEC5342BL;
                        int32_t *l_719[10][10][2] = {{{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]},{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]}},{{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]},{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]}},{{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]},{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_108[3],&g_108[4]}},{{&g_108[4],&g_108[4]},{&g_108[3],&g_108[4]},{&l_314,&l_314},{&l_314,&g_108[4]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}},{{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314},{&g_108[3],&l_314},{&g_7,&g_108[3]},{&g_108[4],&g_108[4]},{&g_108[4],&g_108[3]},{&g_7,&l_314}}};
                        int16_t *l_725 = &g_57[6];
                        int i, j, k;
                        l_720 = (((*l_391) |= (((safe_sub_func_int16_t_s_s((l_717 != &l_701), 0x193AL)) > l_718) != (*g_284))) >= 8UL);
                        l_713 = &l_720;
                        if ((*l_713))
                            continue;
                        (*l_713) = (safe_rshift_func_int16_t_s_s(l_723, ((*l_725) ^= ((*l_391) &= (!((*l_713) == 0x4C018180L))))));
                    }
                    l_734++;
                    if (((safe_mul_func_int16_t_s_s(((*g_697) ^ (safe_mod_func_int16_t_s_s((1UL | g_741[2][6][1]), g_4))), 0xD2BAL)) != l_742))
                    { 
                        int32_t *l_743 = (void*)0;
                        uint16_t *l_744 = &g_103[3][0][1];
                        p_17 = l_743;
                        (*l_727) = ((((g_745 = ((*l_744) = (l_720 != 65529UL))) <= l_746) && (-3L)) == l_720);
                        (*l_727) = (((&g_519[0] == (void*)0) & (*l_730)) == 0UL);
                    }
                    else
                    { 
                        uint32_t l_747 = 0UL;
                        l_747++;
                        return p_17;
                    }
                    l_754--;
                    for (l_530 = 0; (l_530 <= 4); l_530 += 1)
                    { 
                        (*l_730) |= (!(safe_add_func_int32_t_s_s(0xF96DABDEL, (((!l_761) && l_726[3]) == 0x6A81L))));
                        p_17 = p_17;
                    }
                }
                (*g_763) = ((((*g_403) ^= l_726[3]) && (l_726[2] || g_103[0][1][7])) >= l_762);
                for (g_161 = 0; (g_161 != 3); g_161++)
                { 
                    int16_t l_775 = 0x4D69L;
                    uint16_t *l_776 = &l_701;
                    int16_t *l_777 = &l_753;
                    if ((g_29 < ((safe_lshift_func_int16_t_s_u(((+((*l_777) ^= (safe_div_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_u(((*l_776) &= (safe_div_func_uint32_t_u_u(l_775, (-8L)))), 1)) ^ g_82[1]), (*g_763))))) > l_726[3]), 3)) || 0x9CA18094L)))
                    { 
                        int32_t l_782[7];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_782[i] = 1L;
                        g_29 ^= (l_782[1] ^= ((safe_div_func_int32_t_s_s((*g_763), (l_720 |= ((*g_403) = (safe_mod_func_uint16_t_u_u(g_428, l_726[3])))))) == 65531UL));
                    }
                    else
                    { 
                        uint16_t l_783 = 0x11E7L;
                        int32_t l_795 = 0L;
                        int32_t *l_796 = &l_729[1][0][1];
                        int32_t *l_797 = &l_726[5];
                        l_783--;
                        (*l_797) = (safe_mul_func_uint16_t_u_u((--(*l_776)), ((safe_mod_func_int32_t_s_s(((!(((l_720 ^= 0x4DFCEA2BL) & ((*l_796) = (safe_mod_func_int32_t_s_s((l_795 = 0L), l_726[2])))) <= l_775)) | (*g_284)), l_775)) == 0x140E235DL)));
                    }
                }
                (*g_801) ^= (((**l_390) = ((l_726[3] && (safe_add_func_uint16_t_u_u(l_734, l_726[2]))) ^ l_800[3][0][9])) > 65528UL);
            }
            --l_805;
        }
    }
    return p_17;
}



static int32_t * func_18(int32_t * const  p_19)
{ 
    int32_t *l_25 = &g_7;
    int32_t **l_24 = &l_25;
    int32_t *l_33[3];
    int i;
    for (i = 0; i < 3; i++)
        l_33[i] = (void*)0;
    (*g_30) = func_20(l_24, g_4, g_7);
    return l_33[1];
}



static int32_t ** func_20(int32_t ** p_21, int32_t  p_22, int16_t  p_23)
{ 
    uint32_t l_26 = 4294967295UL;
    uint32_t l_27 = 4294967288UL;
    int32_t *l_28[2][7][10] = {{{(void*)0,&g_29,&g_7,&g_7,&g_29,&g_29,&g_7,(void*)0,&g_7,&g_7},{(void*)0,&g_29,&g_7,(void*)0,&g_7,&g_7,&g_7,(void*)0,&g_7,&g_29},{&g_29,&g_7,&g_7,&g_29,&g_7,&g_7,&g_7,&g_7,&g_29,&g_7},{(void*)0,&g_29,&g_29,&g_7,&g_29,&g_7,&g_7,&g_29,&g_7,&g_7},{&g_29,&g_29,&g_7,&g_29,&g_29,&g_7,&g_29,&g_29,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_29,&g_7,&g_29,&g_29,&g_7,&g_7,&g_7},{&g_7,&g_29,&g_7,&g_7,&g_29,&g_7,&g_29,&g_7,&g_7,&g_7}},{{&g_7,&g_29,&g_29,&g_7,&g_29,&g_7,&g_7,&g_7,&g_29,&g_29},{&g_7,&g_29,&g_7,&g_7,&g_7,&g_7,&g_29,&g_7,&g_7,&g_29},{&g_7,&g_29,&g_7,&g_7,&g_29,&g_7,&g_29,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_29,&g_7,&g_29,&g_7,&g_29,&g_7,&g_29,&g_7},{&g_29,&g_29,&g_7,&g_7,&g_7,(void*)0,&g_29,&g_7,&g_7,&g_29},{&g_7,&g_7,&g_7,&g_7,&g_7,(void*)0,&g_29,&g_7,&g_7,&g_7},{&g_29,&g_29,&g_29,&g_7,&g_7,&g_7,&g_29,&g_7,&g_29,&g_29}}};
    int i, j, k;
    g_29 |= (l_26 && (l_27 ^ p_23));
    return p_21;
}



static int32_t  func_38(int32_t  p_39, int16_t  p_40, int32_t  p_41, int32_t * p_42, int32_t  p_43)
{ 
    uint16_t l_52 = 0x7C31L;
    int32_t l_262 = (-2L);
    for (p_41 = (-28); (p_41 >= 6); p_41 = safe_add_func_int32_t_s_s(p_41, 1))
    { 
        int32_t *l_51 = &g_29;
        int16_t *l_56[2][7] = {{(void*)0,&g_57[8],&g_57[8],(void*)0,&g_57[8],&g_57[8],(void*)0},{&g_57[8],(void*)0,&g_57[8],&g_57[8],(void*)0,&g_57[8],&g_57[8]}};
        int32_t l_257 = 0x01DFA635L;
        int i, j;
        --l_52;
        l_257 = (!(((*l_51) = 0L) || (func_58((safe_sub_func_int32_t_s_s((safe_add_func_int16_t_s_s((&p_39 != &p_39), g_7)), p_39)), g_57[8], l_56[1][5], &g_57[2]) ^ p_39)));
    }
    (*p_42) = (l_262 = (safe_sub_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(p_39, g_103[6][0][7])), l_52)));
    return l_262;
}



static uint16_t  func_58(int32_t  p_59, uint32_t  p_60, int16_t * p_61, int16_t * p_62)
{ 
    const int32_t ***l_69 = (void*)0;
    int32_t **l_89 = &g_32;
    int32_t *l_91 = &g_29;
    int32_t **l_90 = &l_91;
    int32_t l_120 = (-1L);
    uint32_t l_138 = 2UL;
    int32_t l_197 = 0x03A9D8E9L;
    int32_t l_198[8][5] = {{0x75AF26B0L,0x75AF26B0L,1L,0x75AF26B0L,0x75AF26B0L},{0x240FB07EL,0x75AF26B0L,0x240FB07EL,0x240FB07EL,0x75AF26B0L},{0x75AF26B0L,0x240FB07EL,0x240FB07EL,0x75AF26B0L,0x240FB07EL},{0x75AF26B0L,0x75AF26B0L,1L,0x75AF26B0L,0x75AF26B0L},{0x240FB07EL,0x75AF26B0L,0x240FB07EL,0x240FB07EL,0x75AF26B0L},{0x75AF26B0L,0x240FB07EL,0x240FB07EL,0x75AF26B0L,0x240FB07EL},{0x75AF26B0L,0x75AF26B0L,1L,0x75AF26B0L,0x75AF26B0L},{0x240FB07EL,0x75AF26B0L,0x240FB07EL,0x240FB07EL,0x75AF26B0L}};
    int16_t l_213[2][3];
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
            l_213[i][j] = 0L;
    }
    (*l_90) = ((*l_89) = func_67(l_69));
    if ((*g_32))
    { 
        uint32_t l_128 = 0xA59BD7B0L;
        const int32_t *l_131 = &g_29;
        int32_t l_133 = 0L;
        int32_t l_162[4] = {0x74B80A73L,0x74B80A73L,0x74B80A73L,0x74B80A73L};
        int i;
        for (g_29 = (-23); (g_29 == 16); g_29 = safe_add_func_uint32_t_u_u(g_29, 8))
        { 
            const int16_t l_105 = (-1L);
            int32_t ***l_148 = (void*)0;
            uint32_t l_164 = 0xFBF1EE3AL;
            for (p_60 = 0; (p_60 <= 1); p_60 += 1)
            { 
                uint16_t *l_102 = &g_103[7][0][6];
                int32_t *l_104 = (void*)0;
                int32_t l_119 = 0x3CCEB7EDL;
                int i;
                if (func_94(g_82[p_60], (((safe_sub_func_uint16_t_u_u(((*l_102) = ((((+(*l_91)) < 0x4167L) | 1L) & 0x4AE9DA5FL)), 0xE21EL)) <= g_82[p_60]) <= g_82[p_60]), l_104, l_105))
                { 
                    uint16_t l_121[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
                    int i;
                    for (g_46 = 0; (g_46 <= 1); g_46 += 1)
                    { 
                        return p_60;
                    }
                    for (g_4 = 0; (g_4 <= 1); g_4 += 1)
                    { 
                        int32_t *l_116 = &g_108[4];
                        int32_t *l_117 = &g_108[4];
                        int32_t *l_118[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_118[i] = (void*)0;
                        --l_121[1];
                        (*l_117) |= (safe_add_func_uint32_t_u_u(0UL, (safe_mul_func_uint16_t_u_u((((*l_90) = &p_59) != &p_59), 65535UL))));
                        --l_128;
                        g_132 = l_131;
                    }
                    l_133 |= (*g_32);
                    l_133 &= ((safe_add_func_uint16_t_u_u(((((safe_add_func_uint32_t_u_u(l_138, (safe_add_func_uint32_t_u_u(((safe_div_func_int32_t_s_s((safe_mod_func_uint32_t_u_u(((*g_32) & p_60), p_59)), 0x108A32CFL)) & l_121[5]), g_29)))) != p_59) & 0x97654C18L) <= p_60), g_82[1])) == g_46);
                }
                else
                { 
                    int32_t *l_149 = &g_108[4];
                    for (l_133 = 1; (l_133 >= 0); l_133 -= 1)
                    { 
                        int32_t *l_145 = &l_119;
                        (*l_145) |= p_60;
                        (*l_145) = (0xEF286D0CL < (safe_unary_minus_func_uint32_t_u((((l_145 == &l_119) == 0xFB1F6A92L) == 0x06325A2BL))));
                    }
                    (*l_149) = (safe_unary_minus_func_int32_t_s((l_148 != &g_31)));
                }
            }
            for (p_60 = 0; (p_60 <= 5); p_60 += 1)
            { 
                uint32_t * const *l_150 = (void*)0;
                uint32_t * const **l_151 = &l_150;
                int i;
                (*l_151) = l_150;
                (*l_89) = &g_108[p_60];
                for (g_46 = 1; (g_46 <= 5); g_46 += 1)
                { 
                    int i;
                    if (g_108[p_60])
                    { 
                        return g_108[p_60];
                    }
                    else
                    { 
                        uint16_t *l_154[2][10][10] = {{{&g_103[2][0][4],&g_103[5][0][4],&g_103[7][0][6],(void*)0,&g_103[6][1][3],&g_103[2][0][4],&g_103[7][0][6],&g_103[1][1][4],&g_103[1][1][4],&g_103[7][0][6]},{&g_103[2][0][4],&g_103[7][0][6],&g_103[1][1][4],&g_103[1][1][4],&g_103[7][0][6],&g_103[2][0][4],&g_103[6][1][3],(void*)0,&g_103[7][0][6],&g_103[5][0][4]},{&g_103[2][0][4],&g_103[6][1][3],(void*)0,&g_103[7][0][6],&g_103[5][0][4],&g_103[2][0][4],&g_103[4][1][7],&g_103[2][0][7],&g_103[5][1][7],&g_103[7][0][6]},{&g_103[2][0][4],&g_103[4][1][7],&g_103[2][0][7],&g_103[5][1][7],&g_103[7][0][6],&g_103[2][0][4],&g_103[7][0][6],&g_103[5][1][7],&g_103[2][0][7],&g_103[4][1][7]},{&g_103[2][0][4],&g_103[7][0][6],&g_103[5][1][7],&g_103[2][0][7],&g_103[1][1][4],(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0},{(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0,(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5]},{(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5],(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4]},{(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4],(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6]},{(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4]},{(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4],(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0}},{{(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0,(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5]},{(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5],(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4]},{(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4],(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6]},{(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4]},{(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4],(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0},{(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0,(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5]},{(void*)0,&g_103[1][0][5],&g_103[7][0][6],&g_103[7][0][6],&g_103[1][0][5],(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4]},{(void*)0,(void*)0,&g_103[4][1][7],&g_103[7][0][6],&g_103[6][1][4],(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6]},{(void*)0,&g_103[1][1][4],&g_103[5][0][4],&g_103[6][1][3],&g_103[7][0][6],(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4]},{(void*)0,&g_103[7][0][6],&g_103[6][1][3],&g_103[5][0][4],&g_103[1][1][4],(void*)0,&g_103[6][1][4],&g_103[7][0][6],&g_103[4][1][7],(void*)0}}};
                        int32_t *l_157 = &l_120;
                        int32_t *l_158 = &g_108[0];
                        int32_t *l_159 = (void*)0;
                        int32_t *l_160[8] = {&g_108[3],&l_133,&g_108[3],&l_133,&g_108[3],&l_133,&g_108[3],&l_133};
                        int i, j, k;
                        (*g_32) = (safe_mod_func_uint16_t_u_u((l_133 = 5UL), (safe_mod_func_int32_t_s_s(0x7643AB9FL, (*g_132)))));
                        l_164--;
                    }
                    for (l_120 = 0; (l_120 <= 5); l_120 += 1)
                    { 
                        int i;
                        (*l_90) = &g_108[l_120];
                    }
                }
                for (l_133 = 2; (l_133 <= (-2)); l_133--)
                { 
                    int32_t l_171 = 1L;
                    for (l_120 = 0; (l_120 < 28); l_120 = safe_add_func_int16_t_s_s(l_120, 5))
                    { 
                        int32_t *l_172 = &g_108[4];
                        p_59 &= (l_171 && 0x31C8L);
                        (*l_89) = l_172;
                    }
                    if (l_171)
                        break;
                }
            }
        }
    }
    else
    { 
        (*l_89) = func_67(l_69);
    }
    if ((g_7 != ((safe_lshift_func_uint16_t_u_u(((safe_lshift_func_int16_t_s_s((-1L), 15)) > g_4), 7)) == 0x567E1754L)))
    { 
        uint32_t *l_183 = &g_46;
        const int32_t l_187 = 0x2BB420BBL;
        int32_t l_195 = (-1L);
        int32_t l_196 = 0xA31060E6L;
        int32_t l_199 = 0x0C3B0A84L;
        int32_t l_200 = 8L;
        int32_t l_201 = 1L;
        int32_t l_202[1];
        int i;
        for (i = 0; i < 1; i++)
            l_202[i] = 3L;
        if (((65527UL == (safe_rshift_func_uint16_t_u_s((safe_rshift_func_uint16_t_u_s((((*l_183) &= ((*g_32) >= 0xC11F17EFL)) && g_103[7][0][6]), (*l_91))), 7))) <= g_4))
        { 
            for (g_29 = 0; (g_29 <= 8); g_29 += 1)
            { 
                uint16_t l_186 = 65529UL;
                int32_t *l_188[8][4][7] = {{{&l_120,&g_7,&g_108[4],&g_7,&g_108[4],&g_7,&l_120},{&g_108[3],&g_7,&g_108[4],&g_7,&g_108[4],&g_7,&g_108[3]},{&l_120,&g_7,&g_108[4],&g_7,&g_108[4],&g_7,&l_120},{&g_108[3],&g_7,&g_108[4],&g_7,&g_108[4],&g_7,&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7}},{{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_108[3],&g_108[4],&g_108[2],&g_108[4],&g_108[3],&g_7},{&g_108[4],&g_108[3],&g_29,&g_29,&g_29,&g_108[3],&g_108[4]},{&g_7,&g_7,&g_7,&g_108[3],&g_7,&g_7,&g_108[2]}}};
                int i, j, k;
                g_108[4] |= (safe_lshift_func_uint16_t_u_s(0xFEA6L, (((((g_57[g_29] < 0UL) || p_60) & l_186) == l_187) || l_187)));
                p_59 |= ((!(+(safe_lshift_func_int16_t_s_s(4L, 10)))) > (g_57[5] <= l_187));
                if ((*l_91))
                    break;
                p_59 &= l_187;
            }
        }
        else
        { 
            int32_t *l_193 = &l_120;
            int32_t *l_194[4][4][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
            uint16_t l_203 = 0x4F48L;
            int i, j, k;
            ++l_203;
        }
    }
    else
    { 
        int32_t l_208[7][2][1] = {{{0xC05B6241L},{0xAC9B6341L}},{{0xAC9B6341L},{0xC05B6241L}},{{3L},{(-1L)}},{{3L},{0xC05B6241L}},{{0xAC9B6341L},{0xAC9B6341L}},{{0xC05B6241L},{3L}},{{(-1L)},{3L}}};
        uint32_t l_212[1][9] = {{0x1310D9C4L,1UL,1UL,0x1310D9C4L,1UL,1UL,0x1310D9C4L,1UL,1UL}};
        int32_t l_214 = 0xDCA199E2L;
        int i, j, k;
        for (p_60 = 10; (p_60 >= 14); p_60 = safe_add_func_uint32_t_u_u(p_60, 4))
        { 
            uint32_t l_211 = 0x7F8BF722L;
            (*l_89) = (void*)0;
            l_208[4][1][0] = (p_59 = (-10L));
            l_214 = (safe_rshift_func_int16_t_s_u((1UL < (((l_212[0][5] |= (l_211 < p_60)) == p_59) || 0L)), l_213[1][0]));
        }
    }
    if ((+(g_46 >= g_103[7][0][6])))
    { 
        return p_60;
    }
    else
    { 
        int32_t l_240 = 9L;
        int32_t l_241 = 0L;
        int32_t l_245 = (-3L);
        for (g_4 = 4; (g_4 != 24); g_4++)
        { 
            int32_t ** const *l_221 = &g_31;
            int32_t ** const **l_220 = &l_221;
            uint32_t *l_229 = &g_46;
            uint32_t **l_228 = &l_229;
            uint32_t ***l_227[9];
            int32_t l_232 = 0x7E63882DL;
            int32_t l_246 = (-1L);
            int i;
            for (i = 0; i < 9; i++)
                l_227[i] = &l_228;
            if ((safe_rshift_func_uint16_t_u_s((l_220 == (void*)0), 12)))
            { 
                int16_t l_242 = 0x89F5L;
                int32_t l_244[9] = {0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL,0xAEDCD98AL};
                int i;
                for (g_161 = 0; (g_161 <= (-5)); g_161 = safe_sub_func_int16_t_s_s(g_161, 1))
                { 
                    uint32_t ***l_226 = (void*)0;
                    uint32_t ****l_225 = &l_226;
                    uint32_t ****l_230 = (void*)0;
                    uint32_t ****l_231 = &l_227[8];
                    int32_t l_243[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_243[i] = 0x51D1072CL;
                    l_232 ^= (!(((*l_225) = (void*)0) == ((*l_231) = l_227[8])));
                    for (g_29 = 0; (g_29 <= 5); g_29 += 1)
                    { 
                        int32_t *l_233 = &g_108[g_29];
                        int32_t *l_234 = (void*)0;
                        int32_t *l_235 = &g_108[4];
                        int32_t *l_236 = &l_120;
                        int32_t *l_237 = &l_197;
                        int32_t *l_238 = (void*)0;
                        int32_t *l_239[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_239[i] = &g_108[4];
                        g_108[g_29] = 0x0DA34F26L;
                        g_247[4]--;
                    }
                    return g_82[0];
                }
                (*l_90) = &p_59;
                (*l_89) = (void*)0;
            }
            else
            { 
                uint16_t l_252[1][5][8] = {{{65530UL,0xD50BL,0xD50BL,65530UL,0xD50BL,0xD50BL,65530UL,0xD50BL},{65530UL,65530UL,0xC1EBL,65530UL,65530UL,0xC1EBL,65530UL,65530UL},{0xD50BL,65530UL,0xD50BL,0xD50BL,65530UL,0xD50BL,0xD50BL,65530UL},{65530UL,0xD50BL,0xD50BL,65530UL,0xD50BL,0xD50BL,65530UL,0xD50BL},{65530UL,65530UL,0xC1EBL,65530UL,65530UL,0xC1EBL,65530UL,65530UL}}};
                int32_t l_256[2];
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_256[i] = 0x4E927EDBL;
                for (l_246 = 8; (l_246 >= 3); l_246 -= 1)
                { 
                    int32_t *l_250 = (void*)0;
                    int32_t *l_251[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_251[i] = &l_197;
                    ++l_252[0][3][7];
                    l_256[0] = (!g_57[l_246]);
                }
            }
        }
    }
    return g_57[2];
}



static int32_t * func_67(const int32_t *** p_68)
{ 
    int32_t ***l_76[3][6][7] = {{{&g_31,&g_31,&g_31,&g_31,&g_31,(void*)0,&g_31},{(void*)0,&g_31,(void*)0,&g_31,&g_31,&g_31,(void*)0},{(void*)0,&g_31,(void*)0,&g_31,&g_31,(void*)0,(void*)0},{&g_31,&g_31,(void*)0,&g_31,&g_31,(void*)0,&g_31},{&g_31,(void*)0,(void*)0,(void*)0,&g_31,&g_31,(void*)0},{&g_31,&g_31,&g_31,&g_31,&g_31,(void*)0,&g_31}},{{&g_31,(void*)0,(void*)0,&g_31,&g_31,&g_31,&g_31},{&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31},{(void*)0,(void*)0,&g_31,(void*)0,&g_31,(void*)0,&g_31},{(void*)0,(void*)0,&g_31,(void*)0,&g_31,&g_31,&g_31},{&g_31,&g_31,&g_31,(void*)0,&g_31,(void*)0,&g_31},{&g_31,&g_31,&g_31,&g_31,(void*)0,&g_31,(void*)0}},{{&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31},{(void*)0,&g_31,(void*)0,&g_31,&g_31,&g_31,&g_31},{&g_31,&g_31,&g_31,&g_31,&g_31,(void*)0,&g_31},{&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,(void*)0},{&g_31,(void*)0,&g_31,&g_31,&g_31,(void*)0,&g_31},{&g_31,&g_31,&g_31,&g_31,&g_31,&g_31,&g_31}}};
    int32_t ****l_75 = &l_76[1][0][1];
    int32_t l_88 = 0xC0820F61L;
    int i, j, k;
    l_88 = (safe_div_func_int32_t_s_s((func_72(((*l_75) = &g_31), g_46) != 4294967295UL), 3L));
    return &g_7;
}



static uint32_t  func_72(int32_t *** p_73, uint16_t  p_74)
{ 
    uint32_t l_77 = 0x3E3B667DL;
    int32_t *l_78 = &g_29;
    int32_t **l_79 = &l_78;
    int32_t *l_80 = &g_29;
    int32_t *l_81[3];
    uint32_t l_83 = 4294967293UL;
    const int32_t * const *l_86 = (void*)0;
    const int32_t * const **l_87 = &l_86;
    int i;
    for (i = 0; i < 3; i++)
        l_81[i] = &g_29;
    (*l_78) ^= l_77;
    (*l_80) ^= (((*l_79) = l_78) != l_80);
    l_83++;
    (*l_87) = l_86;
    return g_46;
}



static const int32_t  func_94(int32_t  p_95, uint32_t  p_96, int32_t * p_97, const uint32_t  p_98)
{ 
    int16_t l_106 = 0x5FE4L;
    int32_t *l_107 = &g_108[4];
    int32_t *l_109[5];
    uint32_t l_110[4][9][6] = {{{0x73F556C0L,4294967287UL,4294967295UL,0x4AEE0EB6L,0x667010A8L,0x34F8EFC5L},{0x6A152264L,0xBD1ED58CL,0x667010A8L,4294967290UL,0x1974AA16L,4294967291UL},{0x1787F865L,0x184417F0L,2UL,0x25B6AC95L,1UL,0x9B53D0E5L},{0x21FA5157L,0xD15494F3L,0x4B26C247L,0xC01541C7L,0x3D8BF850L,0x42021A1DL},{0xEF0A2812L,0xD647035FL,0x9D697DB5L,1UL,6UL,4UL},{4294967289UL,4294967294UL,0xB119D2B5L,0xB119D2B5L,4294967294UL,4294967289UL},{0x3A658485L,0xCDA42CEDL,4294967288UL,4294967295UL,0x67568466L,0x447AD0EDL},{0xBD1ED58CL,0x19192873L,0UL,4294967290UL,0xA490DA09L,0x28A08A75L},{0xBD1ED58CL,0xD06C0765L,4294967290UL,4294967295UL,0x563F1C15L,4294967291UL}},{{0x3A658485L,0x67568466L,0xBCA9B0E0L,0xB119D2B5L,0xCDAFBE71L,0x667010A8L},{4294967289UL,2UL,0xD15494F3L,1UL,4294967288UL,0x1BBB9F26L},{0xEF0A2812L,0x447AD0EDL,1UL,0xC01541C7L,3UL,4294967295UL},{0x21FA5157L,0xEF0A2812L,0x95B69D60L,0x25B6AC95L,0xF5A5DA86L,0xD647035FL},{0x1787F865L,0xCDA42CEDL,0x04884803L,4294967290UL,0xBD1ED58CL,8UL},{0x6A152264L,0xBCA9B0E0L,0x34F8EFC5L,0x4AEE0EB6L,0x8EAE7A24L,0x25B6AC95L},{0x73F556C0L,0x184417F0L,4294967290UL,4294967291UL,0x988D0130L,0xEF0A2812L},{0x8B53E2DDL,0x04884803L,0x4B26C247L,4294967295UL,1UL,0xC8792F59L},{1UL,0x12D82128L,0x8FC39F97L,1UL,0x04884803L,0xF5A5DA86L}},{{0xF5A5DA86L,8UL,0xCDAFBE71L,0xD647035FL,4294967294UL,4UL},{1UL,4294967291UL,1UL,0x95B69D60L,0xF5A5DA86L,0x447AD0EDL},{0xBCA9B0E0L,1UL,4294967295UL,0xF5A5DA86L,4294967295UL,1UL},{0xBD1ED58CL,4294967295UL,4UL,1UL,0x9D697DB5L,0x25B6AC95L},{1UL,0x67568466L,0x667010A8L,0x1BBB9F26L,0x3B6E2EDBL,4294967294UL},{2UL,0x67568466L,0xD15494F3L,0x57036B52L,0x9D697DB5L,0xCDAFBE71L},{1UL,0x279E96FCL,0x1BBB9F26L,0x19192873L,0x16C98A27L,0x28A08A75L},{0xB119D2B5L,0x3A658485L,0x184417F0L,4294967295UL,4294967295UL,0x667010A8L},{0x8B53E2DDL,1UL,0x9B53D0E5L,4294967290UL,0x02624D59L,0UL}},{{0xF5A5DA86L,0UL,0x876425F5L,4UL,0x3C0FC352L,4294967295UL},{0x988D0130L,4294967294UL,0x8EAE7A24L,0xD06C0765L,0x184417F0L,1UL},{0x3B6E2EDBL,0x3C0FC352L,0x73F556C0L,0x28A08A75L,0xD647035FL,1UL},{0x557AD609L,0x812A85F4L,3UL,4294967295UL,0x54E034D0L,0x04884803L},{0xD15494F3L,4294967290UL,4294967294UL,0xBCA9B0E0L,0x876425F5L,4294967295UL},{0x25B6AC95L,4294967295UL,4294967295UL,0x12D82128L,4294967295UL,0x9D697DB5L},{0UL,0x57036B52L,0x8E2B0832L,0x8EAE7A24L,2UL,0x3A658485L},{4294967290UL,0x8FC39F97L,0x1974AA16L,0xB119D2B5L,0x609A124DL,4294967295UL},{9UL,6UL,0x4AEE0EB6L,0xBD1ED58CL,0xBD1ED58CL,0x4AEE0EB6L}}};
    uint32_t *l_114 = &l_110[1][2][1];
    int32_t **l_115[8][4][8] = {{{(void*)0,&l_109[4],&l_107,&g_32,&l_109[4],&l_107,&l_109[1],&l_109[4]},{&l_109[4],&l_109[0],&l_107,&g_32,&l_109[4],&l_107,&l_109[0],(void*)0},{&l_107,&l_109[4],&l_109[4],(void*)0,&g_32,&l_109[4],&l_109[1],&l_109[4]},{&g_32,&g_32,&l_109[4],(void*)0,&l_109[0],&l_109[0],&l_109[4],&g_32}},{{&l_109[4],&l_109[4],&l_109[1],&l_109[0],&g_32,&l_109[1],&l_109[4],&l_107},{&l_109[4],&l_109[0],&l_109[4],(void*)0,(void*)0,&g_32,&l_109[1],&l_109[4]},{&l_109[1],&l_109[0],&g_32,&l_109[4],&l_109[4],&g_32,(void*)0,(void*)0},{&l_109[4],(void*)0,&l_107,&g_32,&l_107,&l_109[4],(void*)0,&g_32}},{{&g_32,&l_109[1],&l_109[1],&l_109[4],&g_32,&l_109[1],(void*)0,&l_109[0]},{&l_109[0],&l_109[4],(void*)0,&l_107,&l_109[4],&g_32,&l_109[4],&g_32},{&l_109[0],(void*)0,(void*)0,&l_109[1],(void*)0,(void*)0,&l_109[0],&l_109[4]},{&l_109[4],&l_109[0],(void*)0,&g_32,&l_109[4],(void*)0,&l_109[4],&l_109[4]}},{{&l_107,&l_107,&l_109[4],&l_109[3],&l_109[4],&l_109[0],&l_109[2],&l_109[1]},{&l_109[4],(void*)0,&g_32,&l_109[4],(void*)0,&l_109[4],&l_109[4],(void*)0},{&l_109[0],&l_109[4],&l_109[4],(void*)0,&l_109[4],&g_32,&g_32,&l_109[4]},{&l_109[1],&l_109[4],(void*)0,(void*)0,&l_107,(void*)0,&g_32,&l_109[1]}},{{&l_107,&l_109[4],(void*)0,&g_32,(void*)0,&l_109[4],&l_109[4],(void*)0},{&l_109[4],&l_109[4],(void*)0,(void*)0,&l_109[1],(void*)0,&l_107,&g_32},{&g_32,&l_107,&g_32,&l_109[4],&l_109[2],&l_109[4],&l_109[4],&l_109[4]},{(void*)0,&l_109[4],(void*)0,&l_109[0],(void*)0,&g_32,&l_107,&l_109[4]}},{{&l_107,&l_109[0],&l_109[4],&l_107,&l_107,(void*)0,&l_109[1],&l_109[0]},{(void*)0,&l_107,&g_32,&l_109[1],(void*)0,&l_109[3],&l_109[3],(void*)0},{&l_109[1],&g_32,&g_32,&l_109[1],(void*)0,&l_109[4],&l_109[4],&g_32},{&g_32,&l_109[4],&g_32,&g_32,&l_107,&g_32,(void*)0,&l_109[4]}},{{&l_109[3],&l_109[4],(void*)0,&g_32,&l_109[4],&l_109[4],&l_109[4],&l_109[2]},{&l_109[4],&g_32,&g_32,&l_109[1],&l_109[0],&l_109[3],&g_32,&l_109[4]},{&l_109[3],&l_107,(void*)0,&l_109[4],(void*)0,(void*)0,&l_109[4],&l_109[4]},{&l_109[4],&l_109[0],&l_109[4],&g_32,&l_109[1],&g_32,(void*)0,&l_107}},{{&l_109[0],&l_109[4],&l_109[1],&g_32,(void*)0,&l_109[4],&l_107,&l_109[4]},{&l_109[3],&l_107,&l_109[4],(void*)0,&g_32,(void*)0,(void*)0,&g_32},{(void*)0,&l_109[4],&l_109[4],&l_109[3],&l_109[4],&l_109[4],&g_32,&l_109[4]},{&l_107,&l_109[4],(void*)0,&l_109[4],&l_109[4],(void*)0,&l_109[2],&l_109[4]}}};
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_109[i] = &g_108[4];
    --l_110[0][3][2];
    (*l_107) |= ((!(g_46 <= ((void*)0 != l_114))) < 0x9089L);
    p_97 = &p_95;
    return (*g_32);
}


