on suppose défini un type Triangle_t

soit le type List_t

* avoir le nb d'elt d'une liste
int GetNbOfElt(List_t * list);

* obtenir le 1er elt

Elt_t * GetFirstElt(List_t * list);


* obtenir le dernier elt

Elt_t * GetLastElt(List_t * list);


soit le type Elt_t
Que contient ce type?
information sur:
    * elt suivant
    * elt précédent
    * objet qui nous intéresse (triangle)
    * la liste à laquelle l'élt appartient

Que demander à un elt?
* obtenir son objet (triangle)

Triangle_t * GetEltTriangle(Elt_t * elt);

* obtenir son voisin précédent dans la liste

Elt_t * GetPreviousElt(Elt_t * elt);


* obtenir son voisin suivant dans la liste

Elt_t * GetNextElt(Elt_t * elt);


* la liste à laquelle il appartient

List_t * GetListOfElt(Elt_t * elt);











