#ifndef TRIANGLETYPE_H_
#define TRIANGLETYPE_H_

typedef struct Point_t Point_t;
struct Point_t {
    double x;
    double y;
};

typedef struct Triangle_t Triangle_t;
struct Triangle_t {
    Point_t vertex[3];
};


#endif // TRIANGLETYPE_H_
