#ifndef LISTTYPES_H_
#define LISTTYPES_H_

#include "eltTypes.h"

typedef struct List_t List_t;
struct List_t {
    int nbElt;
    Elt_t* firstElt;
    Elt_t* lastElt;
};


#endif // LISTTYPES_H_
