#ifndef LIST_H_
#define LIST_H_

int GetNbOfElt(List_t * list); // avoir le nb d'elt d'une liste
Elt_t * GetFirstElt(List_t * list); // obtenir le 1er elt
Elt_t * GetLastElt(List_t * list); // obtenir le dernier elt


#endif // LIST_H_
