#ifndef ELT_H_
#define ELT_H_

Triangle_t * GetEltTriangle(Elt_t * elt);
Elt_t * GetPreviousElt(Elt_t * elt);
Elt_t * GetNextElt(Elt_t * elt);
List_t * GetListOfElt(Elt_t * elt);

#endif // ELT_H_
