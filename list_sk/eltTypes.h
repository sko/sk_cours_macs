#ifndef ELTTYPE_H_
#define ELTTYPE_H_

#include "listTypes.h"
#include "triangleTypes.h"

typedef struct List_t List_t;

typedef struct Elt_t Elt_t;
struct Elt_t {
    Triangle_t* triangle;
    List_t* list;
    Elt_t* previousElt;
    Elt_t* nextElt;
};


#endif // ELTTYPE_H_
