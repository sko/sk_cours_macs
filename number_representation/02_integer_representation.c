/*
 * Representation of integers
 * Thanks to gdb: monitor the value of the variables
 * us: displaying two octets starting from address &us
 * c:  displaying one octet starting from address &c
 * 
 */

int main(void) {
	unsigned short us;
	unsigned char c;

	c = 0;
	us = 0;
	us = 3;
	us = 255;
	us = 258;
	c = 255;

	us = us + 1;
	c = c + 1;
	c = c + 2;

	return 0;
}
