#include<stdio.h>

int main(void) {
	char c = 0;
	short s = 0;

	/* test 1 */
	s = 5;
	c = s;

	printf("--- test 1 \n");
	printf("s = %d\n", s);
	printf("c = %d\n", c);

	/* test 2 */
	s = 129;
	c = s;

	printf("--- test 2 \n");
	printf("s = %d\n", s);
	printf("c = %d\n", c);

	/* test 3 */
	s = 258;
	c = s;

	printf("--- test 3 \n");
	printf("s = %d\n", s);
	printf("c = %d\n", c);

	/* test 4 */
	c = -1;
	s = c;

	printf("--- test 4 \n");
	printf("s = %d\n", s);
	printf("c = %d\n", c);


	c = c + 1;
	s = s + 1;

	return 0;
}
