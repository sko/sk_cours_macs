#include<stdio.h>
#include "types_lorenz.h"
#include "routines_lorenz.h"

#define TODOint 1
#define TODOdouble 1.0



/* ------------------------------- */

double Get_x(State_t *S){
    return S->_x;
}

double Get_y(State_t *S){
    return S->_y;
}

double Get_z(State_t *S){
    return S->_z;
}

double Get_t(State_t *S){
    return S->_t;
}

void Set_x(State_t * S, double val){
    S->_x = val;
}

void Set_y(State_t * S, double val){
    S->_y = val;
}

void Set_z(State_t * S, double val){
    S->_z = val;
}

void Set_t(State_t * S, double val){
    S->_t = val;
}

int IsNotOver(State_t * S){
    const double tMax = 100.0;
    double t = Get_t(S);
    return (t<tMax);
}

void Fct_lorenz(
                double * xx, double * yy, double * zz, double *tt,
                double x, double y, double z, double t
                ){
    const double a = 10.0;
    const double b = 28.0;
    const double c = 8.0/3.0;
    const double dt = 0.01;
    *xx = x + dt*a*(y-x);
    *yy = y + dt*(x*(b-z) - y);
    *zz = z + dt*(x*y - c*z);
    *tt = t + dt;
}

void UpdateState(State_t * S){
/*    const double a = 10.0;
    const double b = 28.0;
    const double c = 8.0/3.0;
    const double dt = 0.01;
    */
    double x = Get_x(S);
    double y = Get_y(S);
    double z = Get_z(S);
    double t = Get_t(S);
    double newx = 0.0;
    double newy = 0.0;
    double newz = 0.0;
    double newt = 0.0;

/*
    newx = x + dt*a*(y-x);
    newy = y + dt*(x*(b-z) - y);
    newz = z + dt*(x*z - c*z);
    newt = t + dt;
*/
    Fct_lorenz(&newx, &newy, &newz, &newt,x, y, z, t);

    Set_x(S, newx);
    Set_y(S, newy);
    Set_z(S, newz);
    Set_t(S, newt);
}

void PrintState(State_t * S){
    double x = Get_x(S);
    double y = Get_y(S);
    double z = Get_z(S);
    double t = Get_t(S);

    printf("%lf %lf %lf %lf\n",t,x,y,z);

}

void Initialize(State_t * S, double x0, double y0,
                double z0, double t0 ){
    Set_x(S,x0);
    Set_y(S,y0);
    Set_z(S,z0);
    Set_t(S,t0);
}







