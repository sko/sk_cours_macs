#ifndef TYPES_LORENZ_H
#define TYPES_LORENZ_H 1

typedef struct State_t  State_t;
struct State_t{
    double _x;
    double _y;
    double _z;
    double _t;
};


#endif // TYPES_LORENZ_H
