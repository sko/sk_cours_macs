#include<stdlib.h>
#include "routines_lorenz.h"
#include "types_lorenz.h"

int main(void){
    const double x0 = 0.1;
    const double y0 = 0.0;
    const double z0 = 0.0;
    const double t0 = 0.0;
    State_t S;
    Initialize(&S, x0, y0, z0, t0 );

    while(IsNotOver(&S)){
        UpdateState(&S);
        PrintState(&S);
    }


    return EXIT_SUCCESS;
}







