#include "routines_lorenz.h"
#include <stdio.h>

int main(void){
double x=0.0;
double y=0.0;
double z=0.0;
double t=0.0;
double newx=0.0;
double newy=0.0;
double newz=0.0;
double newt=0.0;

Fct_lorenz(&newx, &newy, &newz, &newt, x, y, z, t);

printf("x = %lf\ny = %lf\nz = %lf\nt = %lf\n",x,y,z,t);
printf("newx = %lf\nnewy = %lf\nnewz = %lf\nnewt = %lf\n",newx,newy,newz,newt);

x = 1.0;
y = 1.0;
z = 1.0;
t = 1.0;

Fct_lorenz(&newx, &newy, &newz, &newt, x, y, z, t);

printf("x = %lf\ny = %lf\nz = %lf\nt = %lf\n",x,y,z,t);
printf("newx = %lf\nnewy = %lf\nnewz = %lf\nnewt = %lf\n",newx,newy,newz,newt);

x = 1.0;
y = 2.0;
z = 3.0;
t = 4.0;

Fct_lorenz(&newx, &newy, &newz, &newt, x, y, z, t);

printf("x = %lf\ny = %lf\nz = %lf\nt = %lf\n",x,y,z,t);
printf("newx = %lf\nnewy = %lf\nnewz = %lf\nnewt = %lf\n",newx,newy,newz,newt);


    return 0;
}
