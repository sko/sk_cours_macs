#ifndef ROUTINES_LORENZ
#define ROUTINES_LORENZ 1

#include "types_lorenz.h"

int IsNotOver(State_t * S);
double Get_x(State_t *S);
double Get_y(State_t *S);
double Get_z(State_t *S);
double Get_t(State_t *S);
void UpdateState(State_t * S);
void PrintState(State_t * S);
void Set_x(State_t * S, double val);
void Set_y(State_t * S, double val);
void Set_z(State_t * S, double val);
void Set_t(State_t * S, double val);
void Fct_lorenz(double * xx, double * yy, double * zz, double *tt,
                double x, double y, double z, double t);
void Initialize(State_t * S, double x0, double y0,
                double z0, double t0 );










#endif // ROUTINES_LORENZ
