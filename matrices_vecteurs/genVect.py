import numpy as np

nbElt = 120

A = np.random.rand(nbElt)*10

fileName = "vect.dat"
np.savetxt(fileName, A, "%10.20g", "\n", header="%d" %  nbElt)    
